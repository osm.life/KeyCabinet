/********************************************************************************
** Form generated from reading UI file 'setting3gnet.ui'
**
** Created: Wed Aug 5 16:21:48 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTING3GNET_H
#define UI_SETTING3GNET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_setting3gnet
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QComboBox *comboBox;
    QPushButton *cancelBtn;
    QPushButton *okBtn;
    QLabel *label;

    void setupUi(QDialog *setting3gnet)
    {
        if (setting3gnet->objectName().isEmpty())
            setting3gnet->setObjectName(QString::fromUtf8("setting3gnet"));
        setting3gnet->setEnabled(true);
        setting3gnet->resize(400, 200);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(setting3gnet->sizePolicy().hasHeightForWidth());
        setting3gnet->setSizePolicy(sizePolicy);
        setting3gnet->setMinimumSize(QSize(400, 200));
        setting3gnet->setMaximumSize(QSize(400, 200));
        QFont font;
        font.setPointSize(16);
        setting3gnet->setFont(font);
        verticalLayout = new QVBoxLayout(setting3gnet);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        comboBox = new QComboBox(setting3gnet);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setMinimumSize(QSize(0, 50));

        gridLayout->addWidget(comboBox, 1, 0, 1, 2);

        cancelBtn = new QPushButton(setting3gnet);
        cancelBtn->setObjectName(QString::fromUtf8("cancelBtn"));
        cancelBtn->setMinimumSize(QSize(0, 60));

        gridLayout->addWidget(cancelBtn, 2, 0, 1, 1);

        okBtn = new QPushButton(setting3gnet);
        okBtn->setObjectName(QString::fromUtf8("okBtn"));
        okBtn->setMinimumSize(QSize(0, 60));

        gridLayout->addWidget(okBtn, 2, 1, 1, 1);

        label = new QLabel(setting3gnet);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 2);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(setting3gnet);

        QMetaObject::connectSlotsByName(setting3gnet);
    } // setupUi

    void retranslateUi(QDialog *setting3gnet)
    {
        setting3gnet->setWindowTitle(QApplication::translate("setting3gnet", "Dialog", 0, QApplication::UnicodeUTF8));
        cancelBtn->setText(QApplication::translate("setting3gnet", "\345\217\226\346\266\210", 0, QApplication::UnicodeUTF8));
        okBtn->setText(QApplication::translate("setting3gnet", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("setting3gnet", "3G\344\270\212\347\275\221\347\253\257\345\217\243\351\200\211\346\213\251", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class setting3gnet: public Ui_setting3gnet {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTING3GNET_H
