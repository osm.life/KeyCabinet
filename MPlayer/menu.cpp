#include "menu.h"
#include "ui_menu.h"
#include<QProcess>


menu::menu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menu)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    setWindowTitle("管理界面");
    x_0=0;
    y_0=0;
    x_1=0;
    y_1=0;
    timeLong=0;

    gooutFlag=300;

    maxDrawNO=conf.read("Cabinet","cabinetNumber").toInt();
    QString localIp,localPort;
    localIp=conf.read("Server","serverIp").toString();
    localPort=conf.read("Server","serverPort").toString();
    ui->tipIpinfor->setText("当前服务器ip:"+localIp+"   端口:"+localPort);

    passwordFlag=false;
    connect(ui->listWidget,SIGNAL(clicked(QModelIndex)),this,SLOT(choose(QModelIndex)));
    connect(ui->listWidget,SIGNAL(activated(QModelIndex)),this,SLOT(choose(QModelIndex)));
    show_cabinet(0,ui->stackedWidget->widget(0),24);
    show_cabinet(1,ui->stackedWidget->widget(1),22);
    show_cabinet(2,ui->stackedWidget->widget(2),20);
    ui->stackedWidget->setCurrentIndex(0);

    connect(ui->serverPort,SIGNAL(textChanged(QString)),this,SLOT(lineEidetShowKeyboard(QString)));
    timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(showTime()));
    timer->start(1000);

    ui->serverPort->installEventFilter(this);

    connect(this,SIGNAL(sendlockNum(int)),&openlock,SLOT(getlockNo(int)));
    connect(this,SIGNAL(sendcloseNum(int)),&openlock,SLOT(getcloseNo(int)));


    connect(&openlock,SIGNAL(senddata(int,QString)),this,SLOT(getlockStatus(int,QString)));
    connect(&openlock,SIGNAL(sendManagerOp(QString,QString,QString)),this,SLOT(getManagerOp(QString,QString,QString)));
    connect(&openlock,SIGNAL(sendcloseData(int,QString)),this,SLOT(getCloseStatus(int,QString)));
    connect(this,SIGNAL(sendWarnCmd(int)),&openlock,SLOT(getWarnCmd(int)));
    connect(allopen,SIGNAL(clicked()),this,SLOT(openlockThread()));
    connect(&cabinetstatus,SIGNAL(sendDatas(int,QString)),this,SLOT(CabinetStatusShow(int,QString)));

    initshow();
    openType=true;
    lock=new showWriteLock();
    connect(this,SIGNAL(sendWriteNum(int,int)),lock,SLOT(getLockNo(int,int)));

    key=new changeKey;
    connect(this,SIGNAL(sendChangeKey(int)),key,SLOT(getLockNo(int)));
    connect(key,SIGNAL(sendManagerOp(QString,QString,QString)),this,SLOT(getManagerOp(QString,QString,QString)));

    connect(ui->networkMode,SIGNAL(activated(int)),this,SLOT(netsetting(int)));

    qDebug()<<database.openDatabase(1);

    QRegExp rx("((2[0-4]//d|25[0-5]|[01]?//d//d?)//.){3}(2[0-4]//d|25[0-5]|[01]?//d//d?)");
    QRegExpValidator v(rx, 0);
    ui->ipEdit->setValidator(&v);
    ui->ipEdit->setInputMask("000.000.000.000;_");//只要加上;0保证有默认值即可使得正则和mask同时生效。

    QString strStly="QScrollBar::vertical{background:skyblue;border:0px; width:32px; margin:-5px,0px,0px,0px;padding-top:33px;Padding-bottom:33px; background-color:hsva(255,255,255,0%);}";
    ui->listWidget->setStyleSheet(strStly);
}
void menu::showEvent(QShowEvent *)
{
    conf.writetemp("userlogin","flag","1");
    qDebug()<<"进入登入";
}

menu::~menu()
{
    cabinetstatus.stopThread();
    qDebug()<<"正在释放内存空间";
    delete ui;
    for(int i=0;i<button0.size();i++)
    {
    delete button0.at(i);
    delete button1.at(i);
    delete button2.at(i);
    }
    delete allopen;
    delete  tip;
    delete lock;
    delete key;
}

void menu::initMenu(int role)//
{
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/over.png"), tr("无条件开")));//1
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/changeKey.png"), tr("更换钥匙")));//2
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/status.png"), tr("钥匙查看")));//3
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/calendar.png"), tr("日期时间")));//4
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/cabinet.png"), tr("柜号设置")));//5

    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/audio.png"), tr("语音开关")));//6
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/server.png"), tr("设服务器")));//7
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/phone.png"), tr("手机号码")));//8
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/password.png"), tr("管理密码")));//9
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/timecode.png"), tr("条码时间")));//10

    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/8.png"), tr("抽屉自关")));//11
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/network.png"), tr("网络切换")));//12
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/battery.png"), tr("电源其他")));//13

    /*厂家设置*/
    if(role>0)
    {
    passwordFlag=true;
    ui->tipuserpassword->setText("请输入管理员密码");
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/8.png"), tr("抽屉数量")));//14
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/8.png"), tr("锁号配置")));//15
    }
    ui->listWidget->addItem(new QListWidgetItem(QIcon(":/images/logout.png"), tr("退出管理")));//16

    ui->listWidget->setIconSize(QSize(50,50));

}


void menu::mousePressEvent(QMouseEvent *e)
{
    gooutFlag=300;
    qDebug()<<"gogoutFalg:"<<gooutFlag;
    x_0=e->pos().x();
    y_0=e->pos().y();

}
void menu::mouseReleaseEvent(QMouseEvent *e)
{
    x_1=e->pos().x();
    y_1=e->pos().y();

    int x=x_1-x_0;
    int y=y_1-y_0;

    if(x>50)
    {
    ui->listWidget->setMaximumWidth(500);
    }
    if(x<(-50))
    {
    ui->listWidget->setMaximumWidth(60);
    }

}
void menu::choose(QModelIndex index)//点击左边菜单栏
{
    qDebug()<<"index:"<<index.row();
    currentFunction=index.row();
    qDebug()<<"currentFunction:"<<currentFunction;
    if(index.data().toString()=="退出管理")
    {
    quitManager();
    }
    else
    {
        if(index.row()==2)
        {
           if(!cabinetstatus.isRunning())
          {
             cabinetstatus.start();
             cabinetstatus.StartRunThread();
             }
              else
            cabinetstatus.StartRunThread();
        }
        else
        {
             cabinetstatus.PauseThread();
             if(currentFunction==6)//设服务器
             {
             ui->ipEdit->setFocus();
             }
             if(currentFunction==7)
             {
                ui->phone->setFocus();
             }
             if(currentFunction==8)//设密码
             {
                ui->newcode2->setFocus();
             }
        }
    ui->stackedWidget->setCurrentIndex(index.row());
    }
}
void menu::quitManager()
{
    this->close();
}
void menu::closeEvent(QCloseEvent *e)
{
    cabinetstatus.stopThread();
    if(gooutFlag<=-1)
    {
     gooutFlag=300;
     conf.writetemp("userlogin","flag","0");
     e->accept();
    }
    else
    {
        myMessageBox *box=new myMessageBox;
        if(box->exec())
         {
           e->ignore();
         }
         else
         {
            conf.writetemp("userlogin","flag","0");
            qDebug()<<"退出登入";
            timer->stop();
            e->accept();
         }
    }
}

void menu::show_cabinet(int index,QWidget *wiget,int number)
{

   number= conf.read("Cabinet","cabinetNumber").toInt();
   qDebug()<<"number show:  "<<number;
        QGridLayout *layout=new QGridLayout;

        layout->setColumnMinimumWidth(4,10);
        int size=80;//图标大小
        QString infor;
        if(index==0)//无条件开琐
        {
            allopen=new QPushButton;
            tip=new QLabel;
            tip->setText("抽屉打开状态");
            allopen->setText("开所有抽屉");
            allopen->setMinimumWidth(200);
            allopen->setMaximumWidth(200);
            QHBoxLayout *hlayout=new QHBoxLayout;
            hlayout->addWidget(tip);
            hlayout->addWidget(allopen);
            QVBoxLayout *vlayout=new QVBoxLayout;
            vlayout->addLayout(hlayout);
            vlayout->addLayout(layout);
            wiget->setLayout(vlayout);

            for(int i=0;i<number;i++)
            {
              //  qDebug()<<"built button i:"<<i;
                button0.push_back(new myButton());
                button0.at(i)->setWindowName(QString::number(i+1));
                button0.at(i)->setMyPicture(":/images/draw.png",size);
                infor=QString::number(i+1)+" 号抽屉";
                if(i<9)
                {
                infor="0"+QString::number(i+1)+" 号抽屉";
                }
                button0.at(i)->setText(infor);
                layout->addWidget(button0.at(i));
                connect(button0.at(i),SIGNAL(sendNum(QString)),this,SLOT(sendlock_slot(QString)));

            }

        }
        if(index==1)//更换钥匙
        {
            wiget->setLayout(layout);
            for(int i=0;i<number;i++)
            {
                button1.push_back(new myButton());
                button1.at(i)->setWindowName(QString::number(i+1));
                button1.at(i)->setMyPicture(":/images/draw.png",size);
                infor=QString::number(i+1)+" 号抽屉";
                if(i<9)
                {
                infor="0"+QString::number(i+1)+" 号抽屉";
                }
                button1.at(i)->setText(infor);
                layout->addWidget(button1.at(i));
                connect(button1.at(i),SIGNAL(sendNum(QString)),this,SLOT(changekey_slot(QString)));

            }
        }
        if(index==2)//状态查看
        {
            wiget->setLayout(layout);
            for(int i=0;i<number;i++)
            {
                button2.push_back(new myButton());
                button2.at(i)->setWindowName(QString::number(i+1));
                button2.at(i)->setMyPicture(":/images/key_y.png",size);
                infor=QString::number(i+1)+" 号抽屉";
                if(i<9)
                {
                infor="0"+QString::number(i+1)+" 号抽屉";
                }
                button2.at(i)->setText(infor);
                layout->addWidget(button2.at(i));

            }
        }

        /********************调整控件的排列**************************/
        if((number%25)!=0)
        {
            QLabel *mylabel=new QLabel;
            mylabel->resize(QSize(size,size));//大小与toolbutton相同
            int x=number/25;
             for(int z=0;z<=(number%25);z++)
              {
              layout->addWidget(mylabel);
              }
        }
}

void menu::showTime()
{
    timeLong++;
    if(gooutFlag>0)
    {
    gooutFlag--;
    }
    else if(gooutFlag==0)
    {
    gooutFlag--;
    cabinetstatus.PauseThread();
    this->close();
    }
    else
    {

    }
    QString timelabel=QDateTime::currentDateTime().toString("yyyy年MM月dd日 hh时mm分ss秒");
    ui->timeLabel->setText(timelabel);
}

bool menu::eventFilter(QObject *watched, QEvent *event)
{
     if (watched==ui->serverPort)         //首先判断控件(这里指 lineEdit1)
     {
          if (event->type()==QEvent::FocusIn)     //然后再判断控件的具体事件 (这里指获得焦点事件)
          {
              QPalette p=QPalette();
              p.setColor(QPalette::Base,Qt::yellow);
              ui->serverPort->setPalette(p);
          }
          else if (event->type()==QEvent::FocusOut)    // 这里指 lineEdit1 控件的失去焦点事件
          {
              QPalette p=QPalette();
              p.setColor(QPalette::Base,Qt::white);
              ui->serverPort->setPalette(p);
           }
     }

 return QWidget::eventFilter(watched,event);     // 最后将事件交给上层对话框
}
void menu::initshow()
{
    for(int i=1;i<=255;i++)
    {
           ui->time_code->addItem(QString::number(i));
    }
    for(int i=1;i<61;i++)
    {
        ui->cabinet_autoClose->addItem(QString::number(i));
    }
    ui->time_code->setCurrentIndex(4);
    ui->cabinet_autoClose->setCurrentIndex(29);

    QStringList networkmode;
    networkmode<<"有线网络"<<"4G网络"<<"wifi网络";
    ui->networkMode->addItems(networkmode);
    ui->networkMode->setItemIcon(0,QPixmap(":/images/net_wire.png"));
    ui->networkMode->setItemIcon(1,QPixmap(":/images/net_4g.png"));
    ui->networkMode->setItemIcon(2,QPixmap(":/images/net_wifi.png"));

    for(int i=1;i<31;i++)
    {
        ui->CabinetNumber->addItem(QString::number(i));
    }
    ui->CabinetNumber->setCurrentIndex(16);



    for(int i=0;i<10;i++)
    {
        ui->cabinetNo_1->addItem(QString::number(i));
         ui->cabinetNo_2->addItem(QString::number(i));
          ui->cabinetNo_3->addItem(QString::number(i));
           ui->cabinetNo_4->addItem(QString::number(i));
    }

    ui->table__lock->setColumnCount(5);
    int locknumber=conf.read("Cabinet","cabinetNumber").toInt();
    qDebug()<<"locknumber count:"<<locknumber;
    QString tipdrawnum=QString::number(locknumber);
    ui->tipDrawNum->setText(tipdrawnum);
    QString tipvoice;

    ui->VoiceBtn->setIconSize(QSize(100,50));
    ui->VoiceBtn->setFlat(true);

    if(conf.read("Voice","voice").toInt()<1)
    {
    tipvoice="已开";
    ui->VoiceBtn->setIcon(QPixmap(":/images/voice_on.png"));
    }
    else
    {
    tipvoice="已关";
    ui->VoiceBtn->setIcon(QPixmap(":/images/voice_on.png"));
    }

    ui->VoiceBtn->setText(tipvoice);
    //int =conf.read("Cabinet","cabinetNumber").toInt();
     QString tipcabinetno="当前柜号："+conf.read("Cabinet","cabinetNo").toString();
    ui->tipCabinetNo->setText(tipcabinetno);
    QString TimeCabinetClose="当前设置时间:"+conf.read("TimeCabinetClose","timecabinetclose").toString()+"秒";
    ui->tipAntuclaseTime->setText(TimeCabinetClose);

    QString netmode;

    switch(conf.read("Network","networkmode").toInt())
    {
    case 0:
        netmode="有线网络";
        ui->networkMode->setCurrentIndex(0);
        break;
    case 1:
        netmode="4G网络";
        ui->networkMode->setCurrentIndex(1);
        break;
    case 2:
        netmode="wifi网络";
        ui->networkMode->setCurrentIndex(2);
        break;
    }
    ui->tipNetworkMode->setText("当前网络:"+netmode);

    QString tipPone="当前号码："+conf.read("Phone","phone").toString();
    ui->tipPhone->setText(tipPone);

    QString codeTime="当前设置："+conf.read("TimeCode","timecode").toString()+"分钟";
    ui->tipCodeTime->setText(codeTime);

    int rowcount=locknumber%5>0?locknumber/5+1:locknumber/5;
    ui->table__lock->setRowCount(rowcount+2);
    ui->table__lock->horizontalHeader()->setHidden(true);
    ui->table__lock->verticalHeader()->setHidden(true);

    ui->table_Board->setColumnCount(5);
    ui->table_Board->setRowCount(rowcount+2);
    ui->table_Board->horizontalHeader()->setHidden(true);
    ui->table_Board->verticalHeader()->setHidden(true);


    for(int i=0;i<locknumber+10;i++)
    {
        ui->table_Board->setItem(i/5,i%5,new QTableWidgetItem(QString::number(i+1)));
        ui->table__lock->setItem(i/5,i%5,new QTableWidgetItem(QString::number(i+1)));
    }
    for(int i=0;i<((locknumber+10)/5+1);i++)
    {
    ui->table_Board->setRowHeight(i,50);
    ui->table__lock->setRowHeight(i,50);
    }
    for(int i=0;i<5;i++)
    {
    ui->table_Board->setColumnWidth(i,70);
    ui->table__lock->setColumnWidth(i,70);
    }

    for(int i=0;i<=locknumber;i++)
    {
        StatusDraw<<"0";//默认全部是关闭的
    }

    int ok=conf.read("Player","picturePlayer").toInt();
    if(ok==0)//关
    {
      ui->picSettingBtn->setText("图片广告播放(已关)");
    }
    else
    {
      ui->picSettingBtn->setText("图片广告播放(已开)");
    }
    QString copyright=conf.read("copyright","copyright").toString();
    copyright.insert(2,".");
    copyright.insert(5,".");
    copyright.insert(8,".");
    copyright="当前软件本版:"+copyright;
    ui->tipsystem->setText(copyright);
}

void menu::on_cabinetNo_btn_clicked()//写入柜号
{
            QString cabinetno=ui->cabinetNo_1->currentText()+ui->cabinetNo_2->currentText()+ui->cabinetNo_3->currentText()+ui->cabinetNo_4->currentText();
            qDebug()<<"cabinet:"<<cabinetno;
            conf.write("Cabinet","cabinetNo",cabinetno);

            QString tipcabinetno="当前柜号："+cabinetno;
            ui->tipCabinetNo->setText(tipcabinetno);
            tipuser("柜体编号设置成功");
}

void menu::on_CabinetNumber_btn_clicked()//写入柜体抽屉数量
{
    QString cabinetNumber=ui->CabinetNumber->currentText();
    qDebug()<<"cabinetNumber:"<<cabinetNumber;
    conf.write("Cabinet","cabinetNumber",cabinetNumber);
    tipuser("柜体抽屉数设置成功");
}

void menu::on_VoiceBtn_currentIndexChanged(int index)//语音开关 0，表示打开，1，表示关闭
{

    conf.write("Voice","voice",index);
    qDebug()<<"index:"<<index;
}

void menu::on_serverIpOk_clicked()//服务器ip port设置
{
    QString ip=ui->ipEdit->text();

    qDebug()<<"serverip:"<<ui->ipEdit->text();
    //QMessageBox::information(0,"test",ui->ipEdit->text());
    QString ipstr1,ipstr2,ipstr3,ipstr4;
    ipstr1=QString::number(ip.mid(0,3).toInt());
    ipstr2=QString::number(ip.mid(4,3).toInt());
    ipstr3=QString::number(ip.mid(8,3).toInt());
    ipstr4=QString::number(ip.mid(12,3).toInt());
    qDebug()<<ipstr1<<"  "<<ipstr2<<"  "<<ipstr3<<" "<<ipstr4;
    ip=ipstr1+"."+ipstr2+"."+ipstr3+"."+ipstr4;
    qDebug()<<"new ip:"<<ip;


    conf.write("Server","serverIp",ip);
    tipuser("服务器ip地址和端口设置成功");
    maxDrawNO=conf.read("Cabinet","CabinetNumber").toInt();
     QString localIp,localport;
    localIp=conf.read("Server","serverIp").toString();
    localport=conf.read("Server","serverPort").toString();
    ui->tipIpinfor->setText("当前服务器ip:"+localIp+"   端口:"+localport);
    if(!ui->serverPort->text().isEmpty())
    {
    QString serverPort=ui->serverPort->text();
    conf.write("Server","serverPort",serverPort);
    ui->tipIpinfor->setText("当前服务器ip:"+localIp+"   端口:"+ui->serverPort->text());
    }
}

void menu::on_phone_btn_clicked() //电话号码设置
{
    QString phone=ui->phone->text();
    conf.write("Phone","phone",phone);
    qDebug()<<"phone"<<phone;
    QString tipPone="当前手机："+phone;
    ui->phone->clear();
    ui->tipPhone->setText(tipPone);
    tipuser("接收信息手机号码更改成功");
}

void menu::on_newcode_btn_clicked()//密码设置
{
    if(passwordFlag)
    {
    QString code=ui->newcode2->text();
    if(code.isEmpty())
    {
         tipuser("秘密不能为空！");
    }
    else
    {
    passwordFlag=false;
    conf.write("Password","password",code);
    tipuser("管理员密码已经更改成功");
    ui->newcode2->clear();
    ui->tipuserpassword->setText("请输入旧密码");
    }
    }
    else
    {
        QString old=conf.read("Password","password").toString();
        QString newcode=ui->newcode2->text();
        if(old==newcode)
        {
            passwordFlag=true;
            ui->tipuserpassword->setText("请输入新密码");
        }
        else
        {
        tipuser("密码错误！请输入正确密码");
        }

    }
    ui->newcode2->clear();
}

void menu::on_timecode_btn_clicked()//条码有效时间
{
    QString timecode=ui->time_code->currentText();
    conf.write("TimeCode","timecode",timecode);

    QString codeTime="当前设置："+timecode+"分钟";
    ui->tipCodeTime->setText(codeTime);
    tipuser("条码有效时间设置成功");
}

void menu::on_CabinetAutoTime_btn_clicked()
{
    QString cabintautoTime=ui->cabinet_autoClose->currentText();
    conf.write("TimeCabinetClose","timecabinetclose",cabintautoTime);
    QString TimeCabinetClose="当前设置时间:"+cabintautoTime;
    ui->tipAntuclaseTime->setText(TimeCabinetClose);
    tipuser("抽屉自动关闭时间已经更改成功");
}

void menu::on_networdMode_btn_clicked()//上网模式设置 0，表是有线，1表示GPRS , 2 表示WIFI
{
    QString netmode=QString::number(ui->networkMode->currentIndex());
    conf.write("Network","networkmode",netmode);
    switch(ui->networkMode->currentIndex())
    {
    case 0:
        netmode="有线网络";
        break;
    case 1:
        netmode="4G网络";
        break;
    case 2:
        netmode="wifi网络";
        break;
    }
    ui->tipNetworkMode->setText("当前网络:"+netmode);
    tipuser("上网模式已经更改成功");
}
void menu::tipuser(QString str)
{
QFont font;
font.setPointSize(18);
QMessageBox box;
box.setFont(font);
QTimer::singleShot(3000,&box,SLOT(close()));
box.setWindowFlags(Qt::FramelessWindowHint);
box.addButton(new QPushButton("确认"),QMessageBox::AcceptRole);
box.setText(str);
box.exec();
}

void menu::getlockStatus(int index, QString infor)//获取线程中开锁的信息
{
//    for(int i=0;i<StatusDraw.size();i++)
//    {
//    qDebug()<<"打开状态："<<StatusDraw.at(i);
//    }
    gooutFlag=300;
    QString path;
    int number=maxDrawNO;
    if(infor=="99")
    {
        path=":/images/draw.png";
        tip->setText("正在打开"+QString::number(index)+"号抽屉");
    }

    switch(infor.toInt())
    {
    case 0:
        path=":/images/draw_e.png";
        tip->setText(QString::number(index)+"号抽屉响应超时");
        break;
    case 1:
        path=":/images/draw_o.png";
        tip->setText(QString::number(index)+"号抽屉已经打开");
        StatusDraw.replace(index,"1");
        break;
    case 2:
        path=":/images/draw_o.png";
        tip->setText(QString::number(index)+"号抽屉已经打开");
        StatusDraw.replace(index,"1");
        break;
    case 4:
        path=":/images/draw_e.png";
        tip->setText(QString::number(index)+"号抽屉故障，无法打开");
        break;
    default:
        break;
    }
    if(index>=1)
    {
        button0.at(index-1)->setIcon(QIcon(QPixmap(path)));
    }

    if(index==number)
    {
       allopen->setEnabled(true);
       tip->setText("全部抽屉执行已经完成");
    }

}

void menu::getCloseStatus(int index, QString infor)//获取线程中关锁的信息
{
    gooutFlag=300;
    QString path;
    if(infor=="88")
    {
        path=":/images/draw.png";
        tip->setText("正在关闭"+QString::number(index)+"号抽屉");
    }

    switch(infor.toInt())
    {
    case 0:
        path=":/images/draw_e.png";
         tip->setText(QString::number(index)+"号抽屉响应超时");
        break;
    case 1:
        path=":/images/draw.png";
         tip->setText(QString::number(index)+"号抽屉已经关闭");
        StatusDraw.replace(index,"0");
        break;
    case 2:
        path=":/images/draw.png";
         tip->setText(QString::number(index)+"号抽屉已经关闭");
         StatusDraw.replace(index,"0");
        break;
    case 4:
        path=":/images/draw_e.png";
         tip->setText(QString::number(index)+"号抽屉故障，无法关闭");

        break;
    default:
        break;
    }
    button0.at(index-1)->setIcon(QIcon(QPixmap(path)));
    if(index==maxDrawNO)
    {
    allopen->setEnabled(true);
    }

}
void menu::openlockThread()//全开抽屉
{
    gooutFlag=300;
    allopen->setEnabled(false);
    tip->setText("正在打开1号抽屉");
    openlock.startOpenAll(openType);
    if(openType)
    {
    openType=false;
    allopen->setText("关闭所有抽屉");
    }
    else
    {
    openType=true;
    allopen->setText("打开所有抽屉");
    }


}
void menu::sendlock_slot(QString number)//单开抽屉
{
    gooutFlag=300;
    allopen->setEnabled(true);
    for(int i=0;i<StatusDraw.size();i++)
    {
    qDebug()<<"发送信号打开状态："<<StatusDraw.at(i);
    }

    if(!openlock.isRunning())
    {
       openlock.start();
    }
    if(StatusDraw.at(number.toInt())=="0")
    {
    openlock.startSingle();
    emit sendlockNum(number.toInt());
    tip->setText("打开第"+number+"号抽屉");
    }
    else
    {
        openlock.startSingle();
        emit sendcloseNum(number.toInt());
        tip->setText("关闭第"+number+"号抽屉");
    }

}
void menu::CabinetStatusShow(int index, QString infor)//显示抽屉状态函数，接收线程中返回的状态值
{

    QString path;
    switch(infor.toInt())
    {
    case 0://没响应
        path=":/images/key_n.png";
        break;
    case 1://钥匙在位
        path=":/images/key_y.png";

        break;
    case 2://钥匙错误
        path=":/images/key_e.png";

        break;
    case 3://无钥匙在
        path=":/images/key_n.png";

        break;
    default:
        break;
    }
    button2.at(index-1)->setIcon(QIcon(QPixmap(path)));

}

void menu::keyPressEvent(QKeyEvent *k)
{
      gooutFlag=300;
    if(k->key()==Qt::Key_Escape)
    {
    timeKeyCancel1=timeLong;
    }

}
void menu::keyReleaseEvent(QKeyEvent *e)
{
    QString text=e->text();
    qDebug()<<"k.text"<<text;
    keyOption(e);



}


void menu::on_table__lock_itemClicked(QTableWidgetItem *item)
{
  item->setIcon(QIcon(QPixmap(":/images/draw.png")));
  qDebug()<<"number:"<<item->row()*5+item->column()+1;
  emit sendWriteNum(1,item->row()*5+item->column()+1);
    lock->show();

}

void menu::on_table_Board_itemClicked(QTableWidgetItem *item)
{
  item->setIcon(QIcon(QPixmap(":/images/key_y.png")));
    emit sendWriteNum(2,item->row()*5+item->column()+1);
     qDebug()<<"number:"<<item->row()*5+item->column()+1;
    lock->show();
}


void menu::changekey_slot(QString num)
{
    key->show();
    emit sendChangeKey(num.toInt());
}


void menu::netsetting(int index)
{
    qDebug()<<"index:"<<index;
    if(index==1)
    {
    }
    if(index==2)//wifi上网设置
    {
    wifisetting *wifi=new wifisetting;
    wifi->show();
    }
}

void menu::superUser(bool role)
{

    if(role)
    {
      initMenu(3);
       qDebug()<<"super user";
    }
    else
    {
         qDebug()<<"commond user";
     initMenu(0);
    }
}
void menu::keyUp()
{
    qDebug()<<"上";
    ui->listWidget->setCurrentRow(ui->listWidget->currentRow()-1);
}
void menu::keyDown()
{
    qDebug()<<"下";
    ui->listWidget->setCurrentRow(ui->listWidget->currentRow()+1);
}
void menu::keyLeft()
{
    qDebug()<<"左";
}
void menu::keyRight()
{
    qDebug()<<"右";
}
void menu::getCurrentCursor(const QObjectList& q)//获取光标所在的控件
{
    for(int i=0;i<q.length();i++)
    {

    if(!q.at(i)->children().empty())
    {
    getCurrentCursor(q.at(i)->children());
    }
    else
    {

         QObject* o = q.at(i);
           qDebug()<<"QPushButton:"<<o->objectName();
          if (o->inherits("QPushButton"))
          {
          QPushButton* b = qobject_cast<QPushButton*>(o);
          //b->setText("<>");
          }

    //end if
    } //end for
    } //end function
}
void menu::keyOption(QKeyEvent *e)
{
    QString objname=this->focusWidget()->objectName();
    QString clasename=this->focusWidget()->metaObject()->className();
    if(clasename=="QListWidget")
    {
        if(e->key()==Qt::Key_Escape)
        {
        timeKeyCancel2=timeLong;
        int timeDestant=timeKeyCancel2-timeKeyCancel1;
        if(timeDestant>=1)
        {
            qDebug()<<"currnet:"<<currentFunction;
            if(Role>0)
            {
            ui->listWidget->setCurrentRow(15);
            }
            else
                 ui->listWidget->setCurrentRow(13);

        }
        }

        if(e->text()=="2")
        {
        keyUp();//上
        }
        if(e->text()=="8")
        {
        keyDown();//下
        }
        if(e->text()=="4")
        {
        keyLeft();//左
        }
        if(e->text()=="6")
        {
        keyRight();//右
        }


    }
    if((clasename=="QLineEdit")||(clasename=="QComboBox"))
    {

        if(clasename=="QLineEdit")
        {
            QLineEdit *line= this->findChild<QLineEdit *>(objname);
        if(!line->text().isEmpty())
        {
            if(e->key()==Qt::Key_Escape)
            {
            line->backspace();
            }
        }

        }

        if(clasename=="QComboBox")
        {
            qDebug()<<"活动：QComboBox"<<"  "<<objname;
           QComboBox *line= this->findChild<QComboBox *>(objname);
           if(line->isEditable())//可以编辑
           {
               if(e->key()==Qt::Key_Escape)
               {
               //line->backspace();
               line->itemText(line->currentIndex());
               }
           }
           else//不可以编辑
           {
           if(e->text()=="2")
           {
           line->setCurrentIndex(line->currentIndex()-1);//上
           }
           if(e->text()=="8")
           {
           line->setCurrentIndex(line->currentIndex()+1);//上
           }
           }

        }

    }
    if((e->key()==Qt::Key_Return)||(e->key()==Qt::Key_Enter))//确认键盘
    {
        qDebug()<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        switch(currentFunction)
        {
        case 0:
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
        case 5:
            break;
        case 6://设服务器
            qDebug()<<"#####设服务器######";
            if(ui->ipEdit->isActiveWindow())
            {
            ui->serverPort->setFocus();
            }
            if(ui->serverPort->text().size()>=4)
            {
            ui->serverIpOk->click();
            //ui->listWidget->setCurrentRow(currentFunction);

            }
            break;
        case 7://设置手机号码
            if(ui->phone->text().size()>5)
            {
            ui->phone_btn->click();
            }
            break;
        case 8:
            if(ui->newcode2->text().size()>0)
            {
            ui->newcode_btn->click();
            }
            break;
        case 9:
            break;
        case 10:
            break;
        case 11:
            break;
        case 12:
            break;
        case 13:
            break;
        case 14:
            break;
        }

    }
}


void menu::on_reboot_clicked()
{
    QMessageBox box;
    box.setWindowFlags(Qt::FramelessWindowHint);
    box.setText("是否要重启系统！");
    box.addButton(new QPushButton("取消"),QMessageBox::RejectRole);
    box.addButton(new QPushButton("确认"),QMessageBox::AcceptRole);
    if(box.exec()==1)
    {
          QProcess::execute("reboot");
    }
    else
    {
        qDebug()<<"cancel!";
    }
}

void menu::on_shutdown_clicked()
{
    cabinetstatus.PauseThread();
    QMessageBox box;
    box.setWindowFlags(Qt::FramelessWindowHint);
    box.setText("是否要关闭系统！");
    box.addButton(new QPushButton("取消"),QMessageBox::RejectRole);
    box.addButton(new QPushButton("确认"),QMessageBox::AcceptRole);
    if(box.exec()==1)
    {

     QProcess::execute("shutdown -h now");
    }
    else
    {
        qDebug()<<"cancel is press!";
    }
}

void menu::on_initBtn_clicked()
{
    QMessageBox box;
    box.setWindowFlags(Qt::FramelessWindowHint);
    box.setText("是否要初始化记忆库！");
    box.addButton(new QPushButton("取消"),QMessageBox::RejectRole);
    box.addButton(new QPushButton("确认"),QMessageBox::AcceptRole);
    if(box.exec()==1)
    {
     QProcess::execute("rm /usr/local/mystart/database.db");
    }
    else
    {
        qDebug()<<"cancel is press!";
    }
}
void menu::getManagerOp(QString drawno,QString keyid,QString flag)
{  
     qDebug()<<"menu get change:";
      database.wirteDateManagerOp(drawno,keyid,flag);//更换钥匙
}

void menu::on_updatavoice_clicked()
{
    cabinetstatus.PauseThread();
    QMessageBox box;
    box.setWindowFlags(Qt::FramelessWindowHint);
    box.setText("更新语音！");
    box.addButton(new QPushButton("取消"),QMessageBox::RejectRole);
    box.addButton(new QPushButton("确认"),QMessageBox::AcceptRole);
    if(box.exec()==1)
    {
        QString cmd="cp /udisk/voice /usr/local/mystart/ -rf";
        QProcess::execute(cmd);
        cmd="cp /udisk/1/voice /usr/local/mystart/ -rf";
        QProcess::execute(cmd);
    }
    else
    {
        qDebug()<<"cancel is press!";
    }
}

void menu::on_warnBtn_clicked()
{

    emit sendWarnCmd(maxDrawNO+103);
    emit sendWarnCmd(maxDrawNO+103);
}
void menu::on_picSettingBtn_clicked()
{
   int ok=conf.read("Player","picturePlayer").toInt();
   if(ok==0)//关
   {
       conf.write("Player","picturePlayer","1");
       ui->picSettingBtn->setText("图片广告播放(已开)");
   }
   else
   {
       conf.write("Player","picturePlayer","0");
       ui->picSettingBtn->setText("图片广告播放(已关)");
   }
}

void menu::on_pictureSureBtn_clicked()
{
    QString picplayTimer=ui->pictureTimeEdit->text();
    conf.write("Player","picturePlayerTime",picplayTimer);
    ui->pictureTimeEdit->clear();
}


void menu::setDate(QString timelistr)
{
        QProcess *date=new QProcess(this);
        QProcess *sync=new QProcess(this);
        QStringList cmdList;
        QString timelist=timelistr;
        timelist.insert(4,"-");
        timelist.insert(7,"-");
        timelist.insert(10," ");
        timelist.insert(13,":");
        timelist.insert(16,":");

        qDebug()<<timelist;
        cmdList<<"-s"<<timelist;
        date->start("date",cmdList);
        cmdList.clear();
        cmdList<<"-w";
        sync->start("hwclock",cmdList);

        if(date->waitForFinished(1000))
        {
            qDebug()<<"成功1";
        }
        if(sync->waitForFinished(1000))
        {
            qDebug()<<"成功2";
        }
        delete date;
        delete sync;
        timer->start(1000);
}
void menu::on_timeOk_btn_clicked()
{
   timer->stop();
   QString timestr=ui->dateTimeEdit->dateTime().toString("yyyyMMddhhmmss");
   qDebug()<<"时间："<<timestr;
   setDate(timestr);

}


void menu::on_VoiceBtn_clicked()
{
    QString tipvoice;
    if(conf.read("Voice","voice").toInt()==1)
    {
        conf.write("Voice","voice","0");
        tipvoice="已开";
        ui->VoiceBtn->setIcon(QPixmap(":/images/voice_on.png"));
    }
    else
    {
    conf.write("Voice","voice","1");
    tipvoice="已关";
    ui->VoiceBtn->setIcon(QPixmap(":/images/voice_off.png"));
    }
    ui->VoiceBtn->setText(tipvoice);
}

void menu::on_clearlogBtn_clicked()
{
   QProcess::execute("rm -rf log");
   ui->clearlogBtn->setText("日志已清除");
   QString mkdir="mkdir log";
   system(mkdir.toAscii());
}

void menu::on_copyLogToBtn_clicked()
{
    ui->copyLogToBtn->setText("日志已导入");
    QProcess::execute("cp -rp log /udisk/");
    QProcess::execute("cp -rp log /udisk/1/");
    tipuser("日志数据已经成功导到U盘，请取出U盘！");
}
