/****************************************************************************
** Meta object code from reading C++ file 'networktest.h'
**
** Created: Fri Nov 13 09:19:26 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "networktest.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'networktest.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_networkTest[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   12,   13,   12, 0x0a,
      32,   12,   27,   12, 0x0a,
      42,   12,   27,   12, 0x0a,
      54,   12,   27,   12, 0x0a,
      65,   12,   27,   12, 0x0a,
      76,   12,   27,   12, 0x0a,
      86,   12,   27,   12, 0x0a,
      98,   12,   13,   12, 0x0a,
     110,   12,   12,   12, 0x0a,
     133,   12,   12,   12, 0x0a,
     144,   12,   27,   12, 0x0a,
     154,   12,   27,   12, 0x0a,
     163,   12,   27,   12, 0x0a,
     182,   12,   27,   12, 0x0a,
     194,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_networkTest[] = {
    "networkTest\0\0int\0stepone()\0bool\0"
    "steptwo()\0stepthree()\0stepfour()\0"
    "stepfive()\0stepsix()\0stepseven()\0"
    "netSignal()\0getnetworkSignalSize()\0"
    "getSinal()\0getAddr()\0getDns()\0"
    "getNetworkStatus()\0rebootDev()\0"
    "updateDev()\0"
};

void networkTest::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        networkTest *_t = static_cast<networkTest *>(_o);
        switch (_id) {
        case 0: { int _r = _t->stepone();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 1: { bool _r = _t->steptwo();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: { bool _r = _t->stepthree();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: { bool _r = _t->stepfour();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: { bool _r = _t->stepfive();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = _t->stepsix();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->stepseven();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { int _r = _t->netSignal();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 8: _t->getnetworkSignalSize(); break;
        case 9: _t->getSinal(); break;
        case 10: { bool _r = _t->getAddr();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: { bool _r = _t->getDns();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->getNetworkStatus();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: { bool _r = _t->rebootDev();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 14: _t->updateDev(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData networkTest::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject networkTest::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_networkTest,
      qt_meta_data_networkTest, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &networkTest::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *networkTest::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *networkTest::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_networkTest))
        return static_cast<void*>(const_cast< networkTest*>(this));
    return QDialog::qt_metacast(_clname);
}

int networkTest::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
