/********************************************************************************
** Form generated from reading UI file 'showcode.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWCODE_H
#define UI_SHOWCODE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_showCode
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;

    void setupUi(QDialog *showCode)
    {
        if (showCode->objectName().isEmpty())
            showCode->setObjectName(QString::fromUtf8("showCode"));
        showCode->resize(635, 456);
        QFont font;
        font.setPointSize(16);
        showCode->setFont(font);
        verticalLayout = new QVBoxLayout(showCode);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(showCode);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setPointSize(20);
        label->setFont(font1);
        label->setCursor(QCursor(Qt::BlankCursor));
        label->setFrameShadow(QFrame::Raised);
        label->setLineWidth(0);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);


        retranslateUi(showCode);

        QMetaObject::connectSlotsByName(showCode);
    } // setupUi

    void retranslateUi(QDialog *showCode)
    {
        showCode->setWindowTitle(QApplication::translate("showCode", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class showCode: public Ui_showCode {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWCODE_H
