#ifndef LOGINMANAGER_H
#define LOGINMANAGER_H

#include <QDialog>
#include<QKeyEvent>
#include"menu.h"
#include<QShowEvent>
namespace Ui {
    class loginManager;
}

class loginManager : public QDialog
{
    Q_OBJECT

public:
    explicit loginManager(QWidget *parent = 0);
    void keyReleaseEvent(QKeyEvent *);
    void keyPressEvent(QKeyEvent *);
    void showEvent(QShowEvent *);
    ~loginManager();

private:
    Ui::loginManager *ui;
    int limitTime;//自动退出时间
    bool superUserflag;
    menu *linMenu;

signals:
    void sendUserLogin();
      void sendManagerOp(int,QString);//发送管理员操作

private slots:
     void showtime();
     void on_ok_btn_clicked();
     bool checkLogin(QString);//检测是否是管理员密码
     void on_password_textEdited(const QString &arg1);
     void remoteLogin();
     void getManagerOp(int,QString);//管理员操作

};

#endif // LOGINMANAGER_H
