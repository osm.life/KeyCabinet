#include <QtGui/QApplication>
#include<QTextCodec>
#include "menu.cpp"
#include<QSplashScreen>
#include"mainwindow.h"
#include"wiftthread.h"
int main(int argc, char *argv[])
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("utf-8"));
    QApplication a(argc, argv);

    MainWindow main;
    main.showMaximized();

    return a.exec();
}
