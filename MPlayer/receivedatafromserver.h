#ifndef RECEIVEDATAFROMSERVER_H
#define RECEIVEDATAFROMSERVER_H

#include <QThread>
#include<QtNetwork/QTcpSocket>
#include<QtNetwork/QTcpServer>
#include<QQueue>
#include"gunconman.h"
#include"drawandkeystatus.h"
#include"config.h"
#include"command.h"
#include"mydatabase.h"
#include"receivenetworkthread.h"
//#include"watchdog.h"
#include"syslog.h"

class codeStore  //条码结构体
{
public:
    int timeLimit;//条码有效时间
    QString command;//网络接收下的命令
    QString code;//条码或密码
    QString cardid;
};

class receiveDataFromServer : public QThread
{
    Q_OBJECT
public:
    explicit receiveDataFromServer(QObject *parent = 0);
    char ConvertHexChar(char ch);
    QByteArray GetHexValue(QString str);
    void setThread(int);
    QString readCardId(int);
    QString openDraw(int);
    QString closeDraw(int);//关闭抽屉
    QString writeCardId(int,QString);

    bool getLinkServerStatus();

    void updateStatus(int,int,int);

    void updateManagerChange();

    void setDate(QString);

signals:
    void sigalOpendoor(char *);
    void sigalRelink();//数据重新连接网络
    void sigalhasdata();//有数据信号
    void sendcodestatus(int,QString);//确认二维码

    void sendDrawNumber(int );//发送给线程
    void sendTimeTipUser(int);//抽屉自动关闭时间
    void sendCmdStatus(QString,QString);//命令和状态
    void sendReadKeyStatues();//

    void sendSoundPlay(int);
    void sendUpdateFileName(QString);//发送下载连接

    void sendDatatoServer(QString);
    void signalautoCloseTimer(int);

    void signalthread(int);

    void signaldata(int);

private:

    int  waitGoonTime;
    int index1;

    bool networkflag;//网络连接标志
    QStringList queue;//网络接收命令队列
    QQueue<codeStore> mycodestore;//接收网络发过来的二维码;
    QList<QString>cmdNumber;//存一百条流水号
   // int  timeoutflag;//命名执行超时

    int numberFlag;//流水号
    int codetime;//从配置文件中读取出来条码有效时间

    QString CabinetNo;//柜号
    int maxDrawNo;//最大抽屉号
    gunconman guncmd;
    command cmd;
    QString cmdHead;
    QString cmdCurrentRun;//当前正在执行的命令；

    int antocloseTime;//自动关闭抽屉时间
    int closeTimeFlag;
    int drawnumber;//打开了的抽屉号
    DrawAndKeyStatus *drawKeyThread;
    QString drawStatus;//抽屉状态
    QString keyStatus;//钥匙状态

    int timePacktoserver;
    int timeofGetData;//获取串口数据等待次数
    bool IsCmdRuning;

    mydatabase datebase;
    newqserialport port;
    receiveNetworkThread *networkTread;

    QString returnbyidnumber;//刷流水号
   // watchdog *dog;


    QStringList overOpenCmdlist;

    QStringList cmdlistServer;//收到服务器命令

    QString networkmode;

    int useruifalg;
    int timecountNodataflag;//定时时间

private slots:

  bool rebackData(QString);//返回数据给服务器
  void idelData();
  void takeCode(int,QString);//用户扫描二维码处理
  void timeTimer();//定时器函数
  QString getNumberflag();//获取流水号
  void backcodetoServer(int,QString);//返回二维码给服务器 arg1,1为二维码形式，2为钥匙id形式;  arg2,1为二维码，2为钥匙id;

  void autoCloseTimer();
  void autoCloseTimer(int);


  void getReadKeyStatues(QString);

  void updateKeyStatus(QString);//更新钥匙
  void updateDrawStatus(QString);//抽屉状态
  void updatecodeStatus(QString);//刷卡区扫码获取钥匙ID
  void updateWarningStatus(QString);//震动报警

  bool packToServer();//心跳包

  void updateManagerChange(int,QString);//管理员更换钥匙上传

  void readMessage(QString);  //接收数据

  void getNetworkFlag(bool);

  void runNetworkcmd();

  void threadRunset(int);

  QString getnetworkTime();//获取网络时间

  void senddatasignal(int);








};


#endif // RECEIVEDATAFROMSERVER_H
