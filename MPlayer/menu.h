#ifndef MENU_H
#define MENU_H

#include <QDialog>
#include<QMouseEvent>
#include<QModelIndex>
#include<QEvent>
#include"mybutton.h"
#include"config.h"
#include"openlock.h"
#include<QLabel>
#include"cabinetstatus.h"
#include"QTableWidgetItem"
#include"showwritelock.h"
#include<QKeyEvent>
#include"changekey.h"
#include<QStringList>
#include<QDebug>
#include<QMessageBox>
#include<QToolButton>
#include<QPushButton>
#include<QTimer>
#include"openlock.h"
#include<QSplitter>
#include"wifisetting.h"
#include"mymessagebox.h"
#include"mydatabase.h"
namespace Ui {
    class menu;
}

class menu : public QDialog
{
    Q_OBJECT

public:
    explicit menu(QWidget *parent = 0);
    void initMenu(int );//初始化菜单
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);

    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    void setDate(QString);
    void superUser(bool);

    void show_cabinet(int index,QWidget *wiget,int number );//显示的位置，与个数

    void keyUp();
    void keyDown();
    void keyLeft();
    void keyRight();

    void getCurrentCursor(const QObjectList& q);//获取光标所在的控件

    void keyOption(QKeyEvent *e);//


    ~menu();

private:
    Ui::menu *ui;
    QVector<myButton *> button0;//无条件开锁
    QVector<myButton *> button1;//更换钥匙
    QVector<myButton *> button2;//状态查看
    config conf;
     Openlock openlock;
     QPushButton *allopen;
     QLabel *tip;
    // int openType;//0，表示单开，1，表示全开
     bool CabinetFlag;
    CabinetStatus cabinetstatus;
    showWriteLock *lock;
    changeKey *key;
    int x_0,x_1,y_0,y_1;

    QStringList StatusDraw;

    int timeKeyCancel1;
    int timeKeyCancel2;
    int timeLong;

    int maxDrawNO;

    int currentFunction;//功能序列
    int Role;//权限


    mydatabase database;
    bool passwordFlag;

    bool openType;

    int gooutFlag;

    QTimer *timer;








private slots:
    void choose(QModelIndex);//选择界面显示
    void quitManager();//退出管理
    void openlockThread();//无条件全开琐
    void getlockStatus(int,QString);//获取线程中返回的状态,无条件开抽屉
    void getCloseStatus(int,QString);//获取线程中返回的状态,关闭抽屉

    void CabinetStatusShow(int,QString);//获取线程中返回的状态，抽屉状态

    void showTime();//一秒执行一次
    bool eventFilter(QObject *,QEvent *);    //事件过滤器
    void initshow();
    void on_cabinetNo_btn_clicked();
    void on_CabinetNumber_btn_clicked();
    void on_VoiceBtn_currentIndexChanged(int index);
    void on_serverIpOk_clicked();
    void on_phone_btn_clicked();
    void on_newcode_btn_clicked();
    void on_timecode_btn_clicked();
    void on_CabinetAutoTime_btn_clicked();
    void on_networdMode_btn_clicked();
    void tipuser(QString);
    void sendlock_slot(QString);
    void changekey_slot(QString);

    void on_table__lock_itemClicked(QTableWidgetItem *item);
    void on_table_Board_itemClicked(QTableWidgetItem *item);

    void netsetting(int);

    void on_reboot_clicked();

    void on_shutdown_clicked();

    void on_initBtn_clicked();
    void getManagerOp(QString,QString,QString);//管理员操作记录

    void on_updatavoice_clicked();

    void on_warnBtn_clicked();

    void on_picSettingBtn_clicked();

    void on_pictureSureBtn_clicked();

    void on_timeOk_btn_clicked();



    void on_VoiceBtn_clicked();

    void on_clearlogBtn_clicked();

    void on_copyLogToBtn_clicked();

signals:
    void sendlockNum(int );//发送锁号给线程
    void sendcloseNum(int );//发送锁号给线程

    void sendWriteNum(int,int);//发送锁号给确认按钮,arg1是写锁号，还是版号标记
    void sendChangeKey(int);//发送更换钥匙信号

    void sendWarnCmd(int);


   // void sendManagerOp(int,QString);//管理员操作记录

};

#endif // MENU_H
