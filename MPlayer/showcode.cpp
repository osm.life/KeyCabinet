#include "showcode.h"
#include "ui_showcode.h"
#include<QTimer>
#include<QDebug>
showCode::showCode(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::showCode)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    QTimer *timerSecond=new QTimer(this);
    connect(timerSecond,SIGNAL(timeout()),this,SLOT(oneSecondTimer()));
    timerSecond->start(1000);
    showTimelong=0;
}

showCode::~showCode()
{
    delete ui;
}
void showCode::getInfor(QString code)
{
    this->show();
    ui->label->setText(code);
}
void showCode::clearTipinfor()
{
    ui->label->clear();
}
void showCode::showEvent(QShowEvent *)
{
    showTimelong=9;
}
void showCode::oneSecondTimer()
{
    if(showTimelong>0)
    {
    showTimelong--;
   // qDebug()<<"showTimelong:"<<showTimelong;
    }
    else
    {
        if(showTimelong==0)
        {
       // qDebug()<<"close showTimelong:"<<showTimelong;
        showTimelong--;
        this->close();
        }
    }
}
void showCode::mousePressEvent(QMouseEvent *)
{
    showTimelong=-1;
    this->close();
}
