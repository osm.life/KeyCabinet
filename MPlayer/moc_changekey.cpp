/****************************************************************************
** Meta object code from reading C++ file 'changekey.h'
**
** Created: Fri Nov 13 09:19:14 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "changekey.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'changekey.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_changeKey[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   11,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      53,   10,   10,   10, 0x08,
      68,   10,   10,   10, 0x08,
      91,   10,   10,   10, 0x08,
     116,   10,  108,   10, 0x08,
     134,   10,  108,   10, 0x08,
     152,   10,   10,   10, 0x08,
     171,   10,  108,   10, 0x08,
     187,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_changeKey[] = {
    "changeKey\0\0,,\0sendManagerOp(QString,QString,QString)\0"
    "getLockNo(int)\0on_cancelBtn_clicked()\0"
    "openCabinet(int)\0QString\0closeCabinet(int)\0"
    "getinfor(QString)\0on_okBtn_clicked()\0"
    "readCardId(int)\0on_closeBtn_clicked()\0"
};

void changeKey::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        changeKey *_t = static_cast<changeKey *>(_o);
        switch (_id) {
        case 0: _t->sendManagerOp((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 1: _t->getLockNo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_cancelBtn_clicked(); break;
        case 3: _t->openCabinet((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: { QString _r = _t->closeCabinet((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 5: { QString _r = _t->getinfor((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 6: _t->on_okBtn_clicked(); break;
        case 7: { QString _r = _t->readCardId((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 8: _t->on_closeBtn_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData changeKey::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject changeKey::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_changeKey,
      qt_meta_data_changeKey, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &changeKey::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *changeKey::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *changeKey::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_changeKey))
        return static_cast<void*>(const_cast< changeKey*>(this));
    return QDialog::qt_metacast(_clname);
}

int changeKey::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void changeKey::sendManagerOp(QString _t1, QString _t2, QString _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
