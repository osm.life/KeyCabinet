#ifndef LOGINUSER_H
#define LOGINUSER_H

#include <QDialog>
#include<QKeyEvent>
#include<QShowEvent>
namespace Ui {
    class loginUser;
}

class loginUser : public QDialog
{
    Q_OBJECT

public:
    explicit loginUser(QWidget *parent = 0);
    void keyReleaseEvent(QKeyEvent *);
    void keyPressEvent(QKeyEvent *);
    bool eventFilter(QObject *obj, QEvent *event);
    void showEvent(QShowEvent *);
    ~loginUser();

private:
    Ui::loginUser *ui;
    int limitTime;//自动退出时间

private slots:
     void showtime();
     void on_password_textEdited(const QString &arg1);
     void getinfor(QString);
     void on_ok_btn_clicked();

     void on_cancel_btn_clicked();

     void on_num1_clicked();
      void on_num2_clicked();
       void on_num3_clicked();
        void on_num4_clicked();
         void on_num5_clicked();
          void on_num6_clicked();
           void on_num7_clicked();
            void on_num8_clicked();
             void on_num9_clicked();
              void on_num0_clicked();


signals:
     void sendCode(QString );//发送二维码
};

#endif // LOGINUSER_H
