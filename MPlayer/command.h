#ifndef COMMAND_H
#define COMMAND_H

#include <QObject>
#include"gunconman.h"
#include<QRegExp>
class command : public QObject
{
    Q_OBJECT
public:
    explicit command(QObject *parent = 0);
    bool check_command(QString comand);//验证接收命令是否正确
    unsigned char QStringToUnchar(QString sendstring);

    QString SendResultCamand();
    QString rebackInfor(QString cd,QString status);//返回信息
    QString rebackInfor(QString cd);//把字符串计算校验和返回加入字符串后的数据信息
    gunconman guncmd;
    QString codeType(QString str);//查看命令是二位码
    int getCabinetNo(QString);//获取抽屉号

    QString getCmdHead(QString);//获取命令头；
    QString getCmdNumber(QString);//获取流水号
    QString getCmdFlag(QString cmd);//返回命令状态码
    QString getCardId(QString cmd);//返回车钥匙id

    QString getpassword(QString);

    bool check_cmdSun(QString);


signals:

public slots:
private:
    QString gcabinetNo;

};

#endif // COMMAND_H
