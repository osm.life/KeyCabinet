/****************************************************************************
** Meta object code from reading C++ file 'menu.h'
**
** Created: Fri Nov 13 09:19:06 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "menu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'menu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_menu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      41,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
       6,    5,    5,    5, 0x05,
      23,    5,    5,    5, 0x05,
      43,   41,    5,    5, 0x05,
      65,    5,    5,    5, 0x05,
      84,    5,    5,    5, 0x05,

 // slots: signature, parameters, type, tag, flags
     101,    5,    5,    5, 0x08,
     121,    5,    5,    5, 0x08,
     135,    5,    5,    5, 0x08,
     152,   41,    5,    5, 0x08,
     179,   41,    5,    5, 0x08,
     207,   41,    5,    5, 0x08,
     238,    5,    5,    5, 0x08,
     254,   41,  249,    5, 0x08,
     284,    5,    5,    5, 0x08,
     295,    5,    5,    5, 0x08,
     322,    5,    5,    5, 0x08,
     359,  353,    5,    5, 0x08,
     396,    5,    5,    5, 0x08,
     420,    5,    5,    5, 0x08,
     443,    5,    5,    5, 0x08,
     468,    5,    5,    5, 0x08,
     494,    5,    5,    5, 0x08,
     527,    5,    5,    5, 0x08,
     556,    5,    5,    5, 0x08,
     573,    5,    5,    5, 0x08,
     596,    5,    5,    5, 0x08,
     625,  620,    5,    5, 0x08,
     671,  620,    5,    5, 0x08,
     717,    5,    5,    5, 0x08,
     733,    5,    5,    5, 0x08,
     753,    5,    5,    5, 0x08,
     775,    5,    5,    5, 0x08,
     799,  796,    5,    5, 0x08,
     837,    5,    5,    5, 0x08,
     862,    5,    5,    5, 0x08,
     883,    5,    5,    5, 0x08,
     910,    5,    5,    5, 0x08,
     938,    5,    5,    5, 0x08,
     962,    5,    5,    5, 0x08,
     984,    5,    5,    5, 0x08,
    1009,    5,    5,    5, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_menu[] = {
    "menu\0\0sendlockNum(int)\0sendcloseNum(int)\0"
    ",\0sendWriteNum(int,int)\0sendChangeKey(int)\0"
    "sendWarnCmd(int)\0choose(QModelIndex)\0"
    "quitManager()\0openlockThread()\0"
    "getlockStatus(int,QString)\0"
    "getCloseStatus(int,QString)\0"
    "CabinetStatusShow(int,QString)\0"
    "showTime()\0bool\0eventFilter(QObject*,QEvent*)\0"
    "initshow()\0on_cabinetNo_btn_clicked()\0"
    "on_CabinetNumber_btn_clicked()\0index\0"
    "on_VoiceBtn_currentIndexChanged(int)\0"
    "on_serverIpOk_clicked()\0on_phone_btn_clicked()\0"
    "on_newcode_btn_clicked()\0"
    "on_timecode_btn_clicked()\0"
    "on_CabinetAutoTime_btn_clicked()\0"
    "on_networdMode_btn_clicked()\0"
    "tipuser(QString)\0sendlock_slot(QString)\0"
    "changekey_slot(QString)\0item\0"
    "on_table__lock_itemClicked(QTableWidgetItem*)\0"
    "on_table_Board_itemClicked(QTableWidgetItem*)\0"
    "netsetting(int)\0on_reboot_clicked()\0"
    "on_shutdown_clicked()\0on_initBtn_clicked()\0"
    ",,\0getManagerOp(QString,QString,QString)\0"
    "on_updatavoice_clicked()\0on_warnBtn_clicked()\0"
    "on_picSettingBtn_clicked()\0"
    "on_pictureSureBtn_clicked()\0"
    "on_timeOk_btn_clicked()\0on_VoiceBtn_clicked()\0"
    "on_clearlogBtn_clicked()\0"
    "on_copyLogToBtn_clicked()\0"
};

void menu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        menu *_t = static_cast<menu *>(_o);
        switch (_id) {
        case 0: _t->sendlockNum((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->sendcloseNum((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->sendWriteNum((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->sendChangeKey((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->sendWarnCmd((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->choose((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 6: _t->quitManager(); break;
        case 7: _t->openlockThread(); break;
        case 8: _t->getlockStatus((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 9: _t->getCloseStatus((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 10: _t->CabinetStatusShow((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 11: _t->showTime(); break;
        case 12: { bool _r = _t->eventFilter((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QEvent*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: _t->initshow(); break;
        case 14: _t->on_cabinetNo_btn_clicked(); break;
        case 15: _t->on_CabinetNumber_btn_clicked(); break;
        case 16: _t->on_VoiceBtn_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->on_serverIpOk_clicked(); break;
        case 18: _t->on_phone_btn_clicked(); break;
        case 19: _t->on_newcode_btn_clicked(); break;
        case 20: _t->on_timecode_btn_clicked(); break;
        case 21: _t->on_CabinetAutoTime_btn_clicked(); break;
        case 22: _t->on_networdMode_btn_clicked(); break;
        case 23: _t->tipuser((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 24: _t->sendlock_slot((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 25: _t->changekey_slot((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 26: _t->on_table__lock_itemClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 27: _t->on_table_Board_itemClicked((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 28: _t->netsetting((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->on_reboot_clicked(); break;
        case 30: _t->on_shutdown_clicked(); break;
        case 31: _t->on_initBtn_clicked(); break;
        case 32: _t->getManagerOp((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 33: _t->on_updatavoice_clicked(); break;
        case 34: _t->on_warnBtn_clicked(); break;
        case 35: _t->on_picSettingBtn_clicked(); break;
        case 36: _t->on_pictureSureBtn_clicked(); break;
        case 37: _t->on_timeOk_btn_clicked(); break;
        case 38: _t->on_VoiceBtn_clicked(); break;
        case 39: _t->on_clearlogBtn_clicked(); break;
        case 40: _t->on_copyLogToBtn_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData menu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject menu::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_menu,
      qt_meta_data_menu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &menu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *menu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *menu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_menu))
        return static_cast<void*>(const_cast< menu*>(this));
    return QDialog::qt_metacast(_clname);
}

int menu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 41)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 41;
    }
    return _id;
}

// SIGNAL 0
void menu::sendlockNum(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void menu::sendcloseNum(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void menu::sendWriteNum(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void menu::sendChangeKey(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void menu::sendWarnCmd(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
