#ifndef SYSLOG_H
#define SYSLOG_H

#include <QObject>
#include<QDateTime>
class syslog : public QObject
{
    Q_OBJECT
public:
    explicit syslog(QObject *parent = 0);
    void writelog(QString ,QString);//写日志
    void clearlog(QString dateD);//按日期
    void clearlog(QString dateM,QString dateD);//按日期,month,day
    void updatelog(QString, QString);


signals:
private:
    QString currentTime;
    QString logdata;
    QString currentStr;

public slots:

};

#endif // SYSLOG_H
