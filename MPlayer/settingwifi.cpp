#include "settingwifi.h"
#include "ui_settingwifi.h"
#include<QDebug>
#include"config.h"
#include<QTimer>
#include"wiftthread.h"
settingWifi::settingWifi(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settingWifi)
{
    ui->setupUi(this);
   // ui->picture->setPixmap(QPixmap("/home/luo/Pictures/wifi.png"));
  //  QPixmap icon=QPixmap("/home/luo/Pictures/wifi.png");
   // ui->picture->setIcon(QIcon(icon));
  ui->picture->setIconSize(QSize(80,80));
   // ui->password->setAutoFillBackground(true);
    this->setWindowFlags(Qt::FramelessWindowHint);
  ui->checkBox->setChecked(false);

}

settingWifi::~settingWifi()
{
    delete ui;
}

void settingWifi::on_linkBtn_clicked()
{
    config conf;
    QString essidname=essId;
    QString password=ui->password->text();
    conf.write("Network","wifiname",essId);
    conf.write("Network","wifipassword",password);
    QString arg="start-wifi wpa "+essidname+"  "+password+" >./tmpsult";
    system(arg.toAscii());
    ui->linkBtn->setText("已经保存");
    QTimer::singleShot(2000,this,SLOT(close()));

}
void settingWifi::getEssidName(QString esseid)
{
    qDebug()<<esseid;
    essId=esseid;
    ui->tipuser->setText(esseid);
    config conf;
   QString wifiname= conf.read("Network","wifiname").toString();
   QString wifipassword= conf.read("Network","wifipassword").toString();
   if(wifiname==esseid)
   {
    ui->password->setText(wifipassword);
   ui->picture->setIcon(QPixmap(":images/wifi_ok.png"));
   }
   else
   {
    ui->picture->setIcon(QPixmap(":images/wifi_no.png"));
   }
   qDebug()<<"wifiname:"<<wifiname<<"    getwifi:"<<esseid;
   qDebug()<<"wifipassword:"<<wifipassword;

}

void settingWifi::on_cancelBtn_clicked()
{
    ui->tipuser->clear();
    ui->password->clear();
    this->close();
}

void settingWifi::on_checkBox_clicked(bool checked)
{
    if(checked)
    {
    ui->password->setEchoMode(QLineEdit::Normal);
    }
    else
    {
    ui->password->setEchoMode(QLineEdit::Password);
    }
}

void settingWifi::on_forgetBtn_clicked()
{
    config conf;
    conf.write("Network","wifiname","");
    conf.write("Network","wifipassword","");
    ui->picture->setIcon(QPixmap(":images/wifi_no.png"));
    ui->password->clear();
}
