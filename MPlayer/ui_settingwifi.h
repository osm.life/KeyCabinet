/********************************************************************************
** Form generated from reading UI file 'settingwifi.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGWIFI_H
#define UI_SETTINGWIFI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_settingWifi
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *picture;
    QLabel *tipuser;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QLineEdit *password;
    QCheckBox *checkBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancelBtn;
    QPushButton *forgetBtn;
    QPushButton *linkBtn;

    void setupUi(QDialog *settingWifi)
    {
        if (settingWifi->objectName().isEmpty())
            settingWifi->setObjectName(QString::fromUtf8("settingWifi"));
        settingWifi->resize(400, 300);
        QFont font;
        font.setPointSize(15);
        settingWifi->setFont(font);
        verticalLayout_2 = new QVBoxLayout(settingWifi);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(settingWifi);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        picture = new QPushButton(frame);
        picture->setObjectName(QString::fromUtf8("picture"));
        picture->setMinimumSize(QSize(90, 90));
        picture->setMaximumSize(QSize(90, 90));
        picture->setFocusPolicy(Qt::NoFocus);

        horizontalLayout_2->addWidget(picture);

        tipuser = new QLabel(frame);
        tipuser->setObjectName(QString::fromUtf8("tipuser"));

        horizontalLayout_2->addWidget(tipuser);


        horizontalLayout_3->addLayout(horizontalLayout_2);


        verticalLayout->addWidget(frame);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(settingWifi);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_4->addWidget(label_4);

        password = new QLineEdit(settingWifi);
        password->setObjectName(QString::fromUtf8("password"));
        password->setEchoMode(QLineEdit::Password);

        horizontalLayout_4->addWidget(password);


        verticalLayout->addLayout(horizontalLayout_4);

        checkBox = new QCheckBox(settingWifi);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        verticalLayout->addWidget(checkBox);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        cancelBtn = new QPushButton(settingWifi);
        cancelBtn->setObjectName(QString::fromUtf8("cancelBtn"));

        horizontalLayout->addWidget(cancelBtn);

        forgetBtn = new QPushButton(settingWifi);
        forgetBtn->setObjectName(QString::fromUtf8("forgetBtn"));

        horizontalLayout->addWidget(forgetBtn);

        linkBtn = new QPushButton(settingWifi);
        linkBtn->setObjectName(QString::fromUtf8("linkBtn"));

        horizontalLayout->addWidget(linkBtn);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(settingWifi);

        QMetaObject::connectSlotsByName(settingWifi);
    } // setupUi

    void retranslateUi(QDialog *settingWifi)
    {
        settingWifi->setWindowTitle(QApplication::translate("settingWifi", "Dialog", 0, QApplication::UnicodeUTF8));
        picture->setText(QString());
        tipuser->setText(QString());
        label_4->setText(QApplication::translate("settingWifi", "\345\257\206\347\240\201", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("settingWifi", "\346\230\276\347\244\272\345\257\206\347\240\201", 0, QApplication::UnicodeUTF8));
        cancelBtn->setText(QApplication::translate("settingWifi", "\345\217\226\346\266\210", 0, QApplication::UnicodeUTF8));
        forgetBtn->setText(QApplication::translate("settingWifi", "\345\277\230\350\256\260", 0, QApplication::UnicodeUTF8));
        linkBtn->setText(QApplication::translate("settingWifi", "\350\277\236\346\216\245", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class settingWifi: public Ui_settingWifi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGWIFI_H
