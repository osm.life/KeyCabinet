#ifndef CABINETSTATUS_H
#define CABINETSTATUS_H

#include <QThread>
#include"gunconman.h"
#include"newqserialport.h"

class CabinetStatus : public QThread
{
    Q_OBJECT
public:
    explicit CabinetStatus(QObject *parent = 0);
    void run();
    void PauseThread();
    void StartRunThread();
    void stopThread();

signals:
    void sendDatas(int index,QString status);//锁号与对应的开锁信息

public slots:
    void getLockNum(int number);
    void sendCandtoSerial(int number);
private:
    bool stopflag;//控制线程执行
    int lockNumber;
    int timeoutflag;//超时变量
    newqserialport serial;
    gunconman comman;
    int number;//抽屉总数

};

#endif // CABINETSTATUS_H


