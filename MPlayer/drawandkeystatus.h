#ifndef DRAWANDKEYSTATUS_H
#define DRAWANDKEYSTATUS_H

#include <QThread>
#include<QDebug>
#include"newqserialport.h"
#include"config.h"
#include"gunconman.h"
#include<qmath.h>
#include<QMutexLocker>
#include"command.h"
class DrawAndKeyStatus : public QThread
{
    Q_OBJECT
public:
    explicit DrawAndKeyStatus(QObject *parent = 0);
    QString keystatus;//钥匙状态
    QString drawstatus;//抽屉状态
    QString code;//二维码
    void run();

    bool getStatus();

    void updateStatus(int,int,int);//更新状态

signals:
    void sendDrawstatus(QString );//发送抽屉状态
    void sendKeyStatus(QString );//发送钥匙状态
    void sendCode(QString);//发送刷卡区读取的状态
    void sendWarning(QString);//发送报警信息

public slots:
    bool readstatus(int);
    int rebackInfor(QString inforr);//返回锁号

    QString checkData(QString);//返回锁号和状态
    void pauseThread();
    void startThread();

private:
    bool stopflag;
   // int timeout;//抽屉自动关闭时间
    int DrawNumber;//抽屉号
    newqserialport port;
    gunconman cmd;
    int timeflag;//抽屉自动关闭时间
    int MaxKeyNo;//最大钥匙号
    int codeNumber;//刷卡区版号

    int numberStart;

    QMutex locker;

    command checkCmd;//命令操作相关




};

#endif // DRAWANDKEYSTATUS_H
