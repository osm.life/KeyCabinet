#ifndef HTTPWINDOW_H
#define HTTPWINDOW_H

#include <QWidget>
#include<QFile>
#include<QtNetwork/QNetworkAccessManager>
#include <QUrl>
#include<QObject>
#include<QNetworkRequest>
#include<QNetworkReply>
#include<QSslError>
#include<QDir>
class HttpWindow : public QWidget
{
    Q_OBJECT

public:
    explicit HttpWindow(QWidget *parent = 0);
    ~HttpWindow();
    void startRequest(QUrl url);
     void downloadFile(QString addres);
     int flag;
signals:
    void finishedownload();
    void downloadok(int);
    void downloadfail();
    void sendFlieRead(qint64,qint64);

private slots:
     void httpDownloadfail();
     void httpFinishedownload();
    void cancelDownload();
    void httpFinished();
    void httpReadyRead();
    void updateDataReadProgress(qint64 bytesRead, qint64 totalBytes);
    void sslErrors(QNetworkReply*,const QList<QSslError> &errors);

private:

    QUrl url;
    QNetworkAccessManager qnam;
    QNetworkReply *reply;
    QFile *file;
    int httpGetId;
    bool httpRequestAborted;
    void onConvert() ;
    QString rebootfile;
    QDir dir;
    QString filenameg;


};
 #endif
