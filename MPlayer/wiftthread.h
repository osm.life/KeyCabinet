#ifndef WIFTTHREAD_H
#define WIFTTHREAD_H

#include <QThread>
#include"config.h"
#include "initnetwork.h"
#include <QTimer>
#include"syslog.h"
class wiftThread : public QThread
{
    Q_OBJECT
public:
     explicit wiftThread(QObject *parent = 0);
     void run();

signals:
    void sendnetsignal(int);

public slots:
    void getNetSignal();
    void updateconf(QString);

private:
    config conf;
    int networkTypeFlag;
    initNetwork *network;
    int netSigalsize;
     QTimer netTime;

     bool netConnectFlag;//网络状态
     syslog log;

};

#endif // WIFTTHREAD_H
