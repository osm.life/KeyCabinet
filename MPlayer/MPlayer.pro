#-------------------------------------------------
#
# Project created by QtCreator 2015-06-15T10:55:35
#
#-------------------------------------------------
QT       += core gui network sql

TARGET = MPlayer
TEMPLATE = app


SOURCES += main.cpp \
    menu.cpp \
    mybutton.cpp \
    config.cpp \
    qextserialbase.cpp \
    newqserialport.cpp \
    posix_qextserialport.cpp \
    openlock.cpp \
    gunconman.cpp \
    cabinetstatus.cpp \
    loginuser.cpp \
    loginmanager.cpp \
    showcode.cpp \
    receivedatafromserver.cpp \
    command.cpp \
    showwritelock.cpp \
    changekey.cpp \
    settingwifi.cpp \
    wifisetting.cpp \
    initnetwork.cpp \
    showtime.cpp \
    mainwindow.cpp \
    advertisermenttext.cpp \
    drawandkeystatus.cpp \
    soundplayerthread.cpp \
    updatefilethread.cpp \
    httpwindow.cpp \
    mymessagebox.cpp \
    wiftthread.cpp \
    mydatabase.cpp \
    receivenetworkthread.cpp \
    ihttpdownloads.cpp \
    pictureplayer.cpp \
    watchdog.cpp \
    syslog.cpp \
    networktest.cpp \
    atcmd.cpp

HEADERS  += \
    menu.h \
    mybutton.h \
    config.h \
    qextserialbase.h \
    newqserialport.h \
    posix_qextserialport.h \
    openlock.h \
    gunconman.h \
    cabinetstatus.h \
    loginuser.h \
    loginmanager.h \
    showcode.h \
    receivedatafromserver.h \
    command.h \
    showwritelock.h \
    changekey.h \
    settingwifi.h \
    wifisetting.h \
    initnetwork.h \
    showtime.h \
    mainwindow.h \
    advertisermenttext.h \
    drawandkeystatus.h \
    soundplayerthread.h \
    updatefilethread.h \
    httpwindow.h \
    mymessagebox.h \
    wiftthread.h \
    mydatabase.h \
    receivenetworkthread.h \
    ihttpdownloads.h \
    pictureplayer.h \
    watchdog.h \
    syslog.h \
    networktest.h \
    atcmd.h

FORMS    +=\
    menu.ui \
    loginuser.ui \
    loginmanager.ui \
    showcode.ui \
    showwritelock.ui \
    changekey.ui \
    settingwifi.ui \
    wifisetting.ui \
    showtime.ui \
    mainwindow.ui \
    advertisermenttext.ui \
    ihttpdownloads.ui \
    pictureplayer.ui

RESOURCES += \
    images.qrc

OTHER_FILES += \
    文件说明.txt \
    voice/语音文字.txt \
    app.qss \
    apply.qss








































































































































































