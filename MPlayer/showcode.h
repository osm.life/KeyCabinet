#ifndef SHOWCODE_H
#define SHOWCODE_H

#include <QDialog>
#include<QShowEvent>

namespace Ui {
    class showCode;
}

class showCode : public QDialog
{
    Q_OBJECT

public:
    explicit showCode(QWidget *parent = 0);
    void clearTipinfor();

    void showEvent(QShowEvent *);
    void mousePressEvent(QMouseEvent *);

    ~showCode();

private:
    Ui::showCode *ui;
    int showTimelong;

private slots:
    void getInfor(QString);

    void oneSecondTimer();

};

#endif // SHOWCODE_H
