/****************************************************************************
** Meta object code from reading C++ file 'mplayer.h'
**
** Created: Mon Nov 9 10:05:11 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mplayer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mplayer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_mplayer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x0a,
      28,    8,    8,    8, 0x0a,
      41,    8,    8,    8, 0x0a,
      58,    8,    8,    8, 0x0a,
      71,    8,    8,    8, 0x0a,
      87,    8,    8,    8, 0x0a,
     104,    8,    8,    8, 0x0a,
     126,    8,    8,    8, 0x0a,
     144,    8,    8,    8, 0x0a,
     165,    8,    8,    8, 0x0a,
     187,    8,    8,    8, 0x0a,
     208,    8,    8,    8, 0x0a,
     221,    8,    8,    8, 0x0a,
     236,  234,    8,    8, 0x0a,
     266,    8,    8,    8, 0x0a,
     302,    8,    8,    8, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_mplayer[] = {
    "mplayer\0\0play_pause_slots()\0stop_slots()\0"
    "previous_slots()\0next_slots()\0"
    "seek_slots(int)\0get_time_slots()\0"
    "set_volume_slots(int)\0set_sound_slots()\0"
    "playerReward_slots()\0playerForward_slots()\0"
    "back_message_slots()\0playMovies()\0"
    "fullwindow()\0e\0mousePressEvent(QMouseEvent*)\0"
    "mouseDoubleClickEvent(QMouseEvent*)\0"
    "mouseMoveEvent(QMouseEvent*)\0"
};

void mplayer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        mplayer *_t = static_cast<mplayer *>(_o);
        switch (_id) {
        case 0: _t->play_pause_slots(); break;
        case 1: _t->stop_slots(); break;
        case 2: _t->previous_slots(); break;
        case 3: _t->next_slots(); break;
        case 4: _t->seek_slots((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->get_time_slots(); break;
        case 6: _t->set_volume_slots((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->set_sound_slots(); break;
        case 8: _t->playerReward_slots(); break;
        case 9: _t->playerForward_slots(); break;
        case 10: _t->back_message_slots(); break;
        case 11: _t->playMovies(); break;
        case 12: _t->fullwindow(); break;
        case 13: _t->mousePressEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 14: _t->mouseDoubleClickEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 15: _t->mouseMoveEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData mplayer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject mplayer::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_mplayer,
      qt_meta_data_mplayer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &mplayer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *mplayer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *mplayer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_mplayer))
        return static_cast<void*>(const_cast< mplayer*>(this));
    return QDialog::qt_metacast(_clname);
}

int mplayer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
