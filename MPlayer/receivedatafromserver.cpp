#include "receivedatafromserver.h"
#include"newqserialport.h"
#include<QProcess>
#include<QUdpSocket>
#include<QHostInfo>
#include<QTime>
receiveDataFromServer::receiveDataFromServer(QObject *parent) :
    QThread(parent)
{

     msleep(2000);
     index1=0;
     connect(this,SIGNAL(sigalhasdata()),this,SLOT(idelData()));
     QTimer *timelimit=new QTimer(this);
     connect(timelimit,SIGNAL(timeout()),this,SLOT(timeTimer()));
     timelimit->start(5000);

     numberFlag=40959;//16进制9fff;
     QFile file(QDir::currentPath()+"/atempnumber");
     QString str;
         if(file.open(QIODevice::ReadWrite | QIODevice::Text))
         {
           while(!file.atEnd()) {
           QByteArray line = file.readLine();
           QString strfile(line);
                 str=strfile;
                     break;
           }
             file.close();
         }
         if(str.toInt())
         qDebug()<<"########################################"<<str<<QString::number(str.toInt(0,16));
         if(str.toInt()>65534)
         {
         numberFlag=40959;
         }
         else if(str.toInt()<40959)
         {
         numberFlag=40959;
         }
         else
         {
         numberFlag=str.toInt()+1;
         }



     config conf;
     codetime=conf.read("TimeCode","timecode").toInt()*100;
     CabinetNo=conf.read("Cabinet","cabinetNo").toString();
     closeTimeFlag=conf.read("TimeCabinetClose","timecabinetclose").toInt();
     networkmode=conf.read("Network","networkmode").toString();
     connect(this,SIGNAL(signalautoCloseTimer(int)),this,SLOT(autoCloseTimer(int)));
     connect(this,SIGNAL(signalthread(int)),this,SLOT(threadRunset(int)));

     if(closeTimeFlag==0)
     {
     closeTimeFlag=20;
     }
     if(closeTimeFlag>60)
     {
      closeTimeFlag=30;
     }

     maxDrawNo=conf.read("Cabinet","cabinetNumber").toInt();

     conf.write("copyright","copyright","02111320");
     antocloseTime=0;
     QTimer *antoclose=new QTimer(this);
     connect(antoclose,SIGNAL(timeout()),this,SLOT(autoCloseTimer()));
     antoclose->start(1000);

     drawKeyThread=new DrawAndKeyStatus;
     connect(drawKeyThread,SIGNAL(sendKeyStatus(QString)),this,SLOT(updateKeyStatus(QString)));
     connect(drawKeyThread,SIGNAL(sendDrawstatus(QString)),this,SLOT(updateDrawStatus(QString)));
     connect(drawKeyThread,SIGNAL(sendCode(QString)),this,SLOT(updatecodeStatus(QString)));
     connect(drawKeyThread,SIGNAL(sendWarning(QString)),this,SLOT(updateWarningStatus(QString)));

     drawKeyThread->start();//探测钥匙柜抽屉和钥匙状态，以及刷卡区
     IsCmdRuning=false;

     timePacktoserver=0;//心跳包时间24*5=120；
     keyStatus="0000000000";
     drawStatus="0000000000";

     timeofGetData=0;

     if(datebase.openDatabase())
     {
     datebase.readData();
     }
     else
     {
     datebase.openDatabase();
     }
     port.openMycom(1,1);
     networkTread=new receiveNetworkThread;
     connect(this,SIGNAL(sendDatatoServer(QString)),networkTread,SLOT(getData(QString)));
     connect(networkTread,SIGNAL(sendData(QString)),this,SLOT(readMessage(QString)));
     connect(networkTread,SIGNAL(sendNetworkflag(bool)),this,SLOT(getNetworkFlag(bool)));
     connect(networkTread,SIGNAL(signaldata(int)),this,SLOT(senddatasignal(int)));
     networkTread->start();

     packToServer();//心跳包
     QString strHex="aa55a55a19e6"+CabinetNo+"00000000"+getNumberflag()+"000000000000000000000000000000000000000000000002aa";
     QString copyright=conf.read("copyright","copyright").toString();
     strHex.replace(42,8,copyright);
     rebackData(cmd.rebackInfor(strHex,"a2"));

     useruifalg=0;
     timecountNodataflag=0;

//     msleep(200);
//     QString networktimestr=getnetworkTime();
//     if(networktimestr.contains("0000000000"))
//     {}
//     else
//     {
//     setDate(networktimestr);
//     }
     //从网络获取时间，然后设置本地

}

void receiveDataFromServer::readMessage(QString strHex)
{
    qDebug()<<"start to running cmd>>>>>>>>>>>>>>>>>>>>>>"<<strHex;
    qDebug()<<"iscmdruning flag:"<<IsCmdRuning;
    cmdlistServer<<strHex;
}
void receiveDataFromServer::senddatasignal(int index)
{
    emit signaldata(index);
}

bool receiveDataFromServer::rebackData(QString data)
{
   emit sendDatatoServer(data);
   return true;
}
QByteArray receiveDataFromServer::GetHexValue(QString str) //转化为16进制发送
{
    QByteArray senddata;
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    senddata.resize(len/2);
    char lstr,hstr;
    for(int i=0; i<len; )
    {
        hstr=str[i].toAscii();
        if(hstr == ' ')
        {
            i++;
            continue;
        }
        i++;
        if(i >= len)
            break;
        lstr = str[i].toAscii();
        hexdata = ConvertHexChar(hstr);
        lowhexdata = ConvertHexChar(lstr);
        if((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata*16+lowhexdata;
        i++;
        senddata[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    senddata.resize(hexdatalen);
    return senddata;
}

char receiveDataFromServer::ConvertHexChar(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
        return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
        return ch-'a'+10;
    else return (-1);
}
void receiveDataFromServer::idelData()//接收命令后直接打开
{   
    QTime time;
    time.start();
    if(queue.size()>0)
    {
    IsCmdRuning=true;
    drawKeyThread->pauseThread();//暂停线程
    msleep(1500);
    QString strHex=queue.first();
    queue.removeFirst();//take a cmd from queanlist and remove it right now;
    cmdCurrentRun=strHex;
    cmdHead=cmd.getCmdHead(strHex);
    if(cmdHead=="01")//无条件打开抽屉
    {
        qDebug()<<"无条件开抽屉";
        drawnumber=cmd.getCabinetNo(strHex);
        QString openFlag=openDraw(drawnumber);
        if(openFlag=="01")
        {
        emit sendCmdStatus("01","01");
        antocloseTime=9;//重置自动关闭时间
        rebackData(cmd.rebackInfor(strHex,"a8"));
        }
        else
        {
        rebackData(cmd.rebackInfor(strHex,"e1"));
        IsCmdRuning=false;
        }
    }
    if(cmdHead=="02")//取钥匙
    {
        qDebug()<<"##############take keys###############";
    /**********************************************/
        drawnumber=cmd.getCabinetNo(strHex);
        int lockId=cmd.getCabinetNo(strHex)+100;
        QString cardid=readCardId(lockId);
        QString cardflag="00";
        if(cardid.size()<=10)
        {
         msleep(1000);
         cardid=readCardId(lockId);
        }
        if(cardid.size()>10)
        {
        cardflag=cardid.mid(10,2);
        cardid=cardid.mid(0,10);
        }
        else
        {
              msleep(1000);
              cardid=readCardId(lockId);
              cardflag="00";
              if(cardid.size()>10)
              {
               cardflag=cardid.mid(10,2);
               cardid=cardid.mid(0,10);
               }
        }

        QString serverCardId=cmd.getCardId(strHex);
        qDebug()<<"serverCarid:"<<serverCardId;
        qDebug()<<"cardid:"<<cardid;

        if(serverCardId==cardid)//服务器命令中的车id与抽屉的相同
        {
            datebase.deletData(QString::number(drawnumber));
            datebase.deletBorrow(drawnumber);//清除已有抽屉操作
            QString openFlag=openDraw(drawnumber);
            if(openFlag!="01")
            {
            openFlag=openDraw(drawnumber);
            }
            if(openFlag!="01")
            {
            openFlag=openDraw(drawnumber);
            }
            if(openFlag=="01")//抽屉打开成功
            {
               rebackData(cmd.rebackInfor(strHex,"a8"));
               antocloseTime=closeTimeFlag;//重置自动关闭时间
               emit sendCmdStatus("02","02");
               int ii=0;
               while(true)
               {
                   if(antocloseTime>1)//等待用户放入钥匙
                   {
                       ii++;
                       qDebug()<<ii;
                       if(ii>closeTimeFlag*4)
                       {
                       emit signalautoCloseTimer(1);
                       antocloseTime=-4;
                       break;
                       }
                      QString flag =readCardId(drawnumber+100);

                      if(flag.size()>10)
                      {
                          flag=flag.mid(10,2);
                      }
                      if(flag=="01")
                      {
                      }
                      if(flag=="02")
                      {

                      }
                      if(flag=="03")
                      {
                      msleep(1500);
                      antocloseTime=-5;//
                      emit sendTimeTipUser(0);
                      emit  signalautoCloseTimer(1);
                      break;
                      }
                   }
                   else
                       break;
               }
               }
            else
            {
             rebackData(cmd.rebackInfor(strHex,"e1"));
             IsCmdRuning=false;
            }
        }
        else
        {
            rebackData(cmd.rebackInfor(strHex,"e2"));
            IsCmdRuning=false;

        }

    }
    if(cmdHead=="03")//还钥匙
    {
       drawnumber=cmd.getCabinetNo(strHex);
       datebase.deletBorrow(drawnumber);//清除已有抽屉操作
       QString carid=cmd.getCardId(strHex);
       QString writeCardFlag=writeCardId(drawnumber,carid);
       if(writeCardFlag!="04")//one more try
       {
       msleep(200);
       writeCardFlag=writeCardId(drawnumber,carid);
       }

       if(writeCardFlag!="04")//one more try
       {
       msleep(300);
       writeCardFlag=writeCardId(drawnumber,carid);
       }
       if(writeCardFlag=="04")//写入钥匙id成功；
       {
         datebase.deletBorrow(drawnumber);
         datebase.deletData(QString::number(drawnumber));
         QString openFlag=openDraw(drawnumber);
          qDebug()<<"还钥匙openfalg："<<openFlag;
          if(openFlag!="01")
          {
          openFlag=openDraw(drawnumber);
          }
          if(openFlag!="01")
          {
          openFlag=openDraw(drawnumber);
          }

            if(openFlag=="01")//抽屉打开成功
            {
                 rebackData(cmd.rebackInfor(strHex,"a8"));
                 antocloseTime=closeTimeFlag;//重置自动关闭时间
                 emit sendCmdStatus("03","02");//提示放入钥匙

                 int ii=0;
                 while(true)
                 {    
                     if(antocloseTime>1)//等待用户放入钥匙
                     {
                         ii++;
                         qDebug()<<ii;
                         if(ii>closeTimeFlag*4)
                         {
                         emit signalautoCloseTimer(1);
                         antocloseTime=-4;
                         break;
                         }
                         QString flag =readCardId(drawnumber+100);
                         if(flag.size()>10)
                         {
                             flag=flag.mid(10,2);
                         }
                         if(flag=="01")//钥匙正确
                         {
                              msleep(1500);
                              antocloseTime=-5;//
                              emit sendTimeTipUser(0);
                              emit  signalautoCloseTimer(1);
                              break;
                         }
                         if(flag=="02")
                         {
                              emit sendCmdStatus("03","钥匙错误,请放入你正确的钥匙！！！");//提示放入钥匙
                         }

                     }
                     else
                         break;
                 }
            }
            else
            {
            rebackData(cmd.rebackInfor(strHex,"E1"));//门打开失败
             IsCmdRuning=false;
            }
       }
       else
       {
        rebackData(cmd.rebackInfor(strHex,"F1"));
        IsCmdRuning=false;
       }

    }
    if(cmdHead=="04")//读取钥匙柜状态
    {

        QString status=drawStatus+keyStatus;
        QString flag;
        if(drawStatus.contains("ffffffffff",Qt::CaseInsensitive))
        {
            flag="a8";
        }
        else
        {
            flag="e4";
        }
        QString changeStr=strHex;
        QString countDraw=QString::number(maxDrawNo);
        int sizeDraw=countDraw.size();
        if(sizeDraw<4)
        {
            for(int i=0;i<4-sizeDraw;i++)
            {
                countDraw="0"+countDraw;
            }
        }
        changeStr.replace(20,4,countDraw);
        changeStr.replace(44,status.size(),status);
        QString mycmd=cmd.rebackInfor(changeStr,flag);
        qDebug()<<"读取钥匙柜状态："<<strHex;
        qDebug()<<"读取钥匙柜状态："<<changeStr;
        qDebug()<<"读取钥匙柜状态："<<mycmd;
        rebackData(mycmd);
    }
    if(cmdHead=="05")//读取某抽屉钥匙状态命令
    {
        msleep(1000);
        int KeyNo=cmd.getCabinetNo(strHex); //钥匙号
        QString statuflag=readCardId(KeyNo+100);
        QString statuscmd,cardid="0000000000";//执行命令返回的数据，命令头，命令执行结果返回
        QString rebackinfor="00";
        if(statuflag.size()<=10)
        {
        msleep(1000);
        statuflag=readCardId(KeyNo+100);
        }
        if(statuflag.size()<=10)
        {
        msleep(1000);
        statuflag=readCardId(KeyNo+100);
        }
        /************************************/
        if(statuflag.size()>10)
        {
        cardid=statuflag.mid(0,10);
        statuscmd=statuflag.mid(10,2);
        qDebug()<<"stataus falg:"<<statuscmd;
             if( statuscmd=="01")
             {
             rebackinfor="a8";
             }
             else
             {
              rebackinfor="e2";
             }
        }
        else
        {
                rebackinfor="e5";
        }


        QString mycode="000000000000";
        QString back=cardid+mycode;
        QString mycmd=strHex;
        mycmd.replace(30,22,back);
        rebackData(cmd.rebackInfor(mycmd,rebackinfor));

        IsCmdRuning=false;
       qDebug()<<"读取某抽屉钥匙状态命令";
    }
    if(cmdHead=="14")//远程报警的开关命令
    {
         qDebug()<<"远程报警received";
         QString cmdstatus=cmd.getCmdFlag(strHex);
         if((cmdstatus=="a8")||(cmdstatus=="A8"))
         {
         port.WriteMyCom(guncmd.open_command(maxDrawNo+103));
         }
         else
         {
         port.WriteMyCom(guncmd.releasewarn_command(maxDrawNo+103));
         }
         QString statuflag=port.ReadMyCom();
         QString backcmd,handcmd,statuscmd;//执行命令返回的数据，命令头，命令执行结果返回
         QString mycmd=strHex;
         if(statuflag.size()>=18)
         {
         backcmd=statuflag.mid(statuflag.size()-18,18);
         handcmd=backcmd.mid(0,2);
         statuscmd=backcmd.mid(14,2);
         qDebug()<<"报警状态返回："<<backcmd;
         if(statuscmd=="01")
         {
             if(handcmd=="b3")//打开报警成功
             {
             rebackData(cmd.rebackInfor(mycmd,"a8"));
               emit sendCmdStatus("14","打开报警成功");
             }
             if(handcmd=="bf")//关闭报警成功
             {
              emit sendCmdStatus("14","关闭报警成功");
             rebackData(cmd.rebackInfor(mycmd,"a9"));
             }
         }
         else if(statuscmd=="04")
         {
             //操作报警器失败
             emit sendCmdStatus("14","报警器没有响应");
             rebackData(cmd.rebackInfor(mycmd,"e8"));
         }
         else
         {
             emit sendCmdStatus("14","报警器超时未响应响应");
             rebackData(cmd.rebackInfor(mycmd,"e8"));
         }

         }
         else
         {
         //操作报警器失败
         emit sendCmdStatus("14","报警器没有响应");
         rebackData(cmd.rebackInfor(mycmd,"e8"));
         }

    IsCmdRuning=false;
     qDebug()<<"远程报警的开关命令";
     // emit sendCmdStatus(cmdHead,"01");
    }

    if(cmdHead=="15")//远程登入管理员命令
    {
      qDebug()<<"远程登入received";
      emit sendCmdStatus(cmdHead,"01");
      rebackData(cmd.rebackInfor(strHex,"a8"));

      IsCmdRuning=false;
      qDebug()<<"远程登入管理员命令";
    }

    if(cmdHead=="16")//管理员更换钥匙
    {

        drawnumber=cmd.getCabinetNo(strHex);
        int lockId=cmd.getCabinetNo(strHex)+100;
        QString cardid=readCardId(lockId);
        QString cardflag="00";
        if(cardid.size()<=10)
        {
        msleep(1000);
        cardid=readCardId(lockId);
        }
        if(cardid.size()<=10)
        {
        msleep(1000);
        cardid=readCardId(lockId);
        }
        if(cardid.size()>10)
        {
        cardflag=cardid.mid(10,2);
        cardid=cardid.mid(0,10);
        }
        qDebug()<<"取钥匙id：《《《"<<cardid<<"    "<<cardflag;
        QString serverCardId=cmd.getCardId(strHex);
        if(serverCardId==cardid)//服务器命令中的车id与抽屉的相同
        {
            QString openFlag=openDraw(drawnumber);
            if(openFlag=="00")
            {
            openFlag=openDraw(drawnumber);
            }
               if(openFlag=="01")//抽屉打开成功
               {
               antocloseTime=closeTimeFlag;//重置自动关闭时间
               emit sendCmdStatus("16","02");
               rebackData(cmd.rebackInfor(strHex,"a8"));
               int ii=0;
               while(true)
               {
                   if(antocloseTime>1)//等待用户取出钥匙
                   {
                       ii++;
                       qDebug()<<ii;
                       if(ii>closeTimeFlag*4)
                       {
                       emit signalautoCloseTimer(1);
                       antocloseTime=-4;
                       break;
                       }
                       port.WriteMyCom(guncmd.readCardId(drawnumber+100));//codeNumber
                       QString inforr=port.ReadMyCom(400);
                       qDebug()<<"deng"<<inforr;
                       QString backcmd,handcmd,statuscmd;//执行命令返回的数据，命令头，命令执行结果返回
                       if(inforr.size()>=18)
                       {
                       backcmd=inforr.mid(inforr.size()-18,18);
                       handcmd=backcmd.mid(0,2);
                       statuscmd=backcmd.mid(14,2);
                       if(handcmd=="bb")
                       {
                       if(statuscmd=="03")
                       {
                       emit sendCmdStatus("16","03");//旧钥匙已经拿走
                       qDebug()<<"旧钥匙已经拿出";
                       antocloseTime=closeTimeFlag;//放入钥匙正确，提示用户

                       int iii=0;
                       QString newCardid;
                       while(true)//放入新钥匙
                            {
                                if(antocloseTime>0)//等待用户放入钥匙
                                {
                                    iii++;
                                    qDebug()<<iii;
                                    if(iii>closeTimeFlag*4)
                                    {
                                    emit signalautoCloseTimer(1);
                                    antocloseTime=-4;
                                    break;
                                    }

                                    qDebug()<<"等待新钥匙放入";
                                    port.WriteMyCom(guncmd.readCardId(drawnumber+100));//codeNumber
                                    QString inforr=port.ReadMyCom(200);
                                    qDebug()<<"deng"<<inforr;
                                    QString backcmd,handcmd,statuscmd;//执行命令返回的数据，命令头，命令执行结果返回
                                    int iiii=0;
                                    while(true)
                                    {
                                        iiii++;
                                        qDebug()<<iiii;
                                        if(iiii>closeTimeFlag*4)
                                        {
                                        emit signalautoCloseTimer(1);
                                        antocloseTime=-4;
                                        break;
                                        }

                                        port.WriteMyCom(guncmd.readCardId(drawnumber+100));//codeNumber
                                        inforr=port.ReadMyCom(200);
                                        if(inforr.size()>=18)
                                        {
                                        backcmd=inforr.mid(inforr.size()-18,18);
                                        handcmd=backcmd.mid(0,2);
                                        statuscmd=backcmd.mid(14,2);
                                        newCardid=backcmd.mid(4,10);
                                        qDebug()<<"新钥匙ID："<<backcmd;
                                        }
                                        if(handcmd=="bb")
                                        {
                                            if((statuscmd=="01")||(statuscmd=="02"))
                                                break;
                                        }
                                        if(antocloseTime<=1)
                                        {
                                            break;
                                        }

                                    }
                                    if(inforr.size()>=18)
                                    {
                                    backcmd=inforr.mid(inforr.size()-18,18);
                                    handcmd=backcmd.mid(0,2);
                                    statuscmd=backcmd.mid(14,2);
                                    qDebug()<<"新钥匙ID："<<backcmd;
                                    if(handcmd=="bb")
                                    {
                                    emit sendCmdStatus("03","03");
                                    QString writeCardFlag=(drawnumber,newCardid);
                                    if(writeCardFlag=="04")
                                    {
                                     antocloseTime=-5;
                                     emit sendTimeTipUser(0);
                                     emit  signalautoCloseTimer(1);
                                    }
                                    break;
                                    }

                                    }
                                    }
                            }

                       break;
                       }
                       else if(statuscmd=="02")//钥匙错误
                       {
                        qDebug()<<"钥匙错误放入";
                       }
                       }
                       }
                   }
                   else
                       break;
               }
               }
               else
               {
                   rebackData(cmd.rebackInfor(strHex,"e1"));
                   IsCmdRuning=false;
               }
        }
        else
        {
            rebackData(cmd.rebackInfor(strHex,"e2"));
            IsCmdRuning=false;

        }

    }
    if(cmdHead=="17")//更新文件（视频，软件，文本，图片）
    {
        qDebug()<<"cmd 17:"<<strHex;
        QString number=cmd.getCmdNumber(strHex);
        qDebug()<<"流水号："<<number;
        QString url=strHex.mid(28,strHex.indexOf("ff")-28);
        QString filename;
        for(int i=0;i<url.size()/2;i++)
        {
            filename=filename+(QChar)url.mid(2*i,2).toInt(0,16);
        }
        emit sendUpdateFileName(filename);

        QString getfilename=filename.mid(filename.lastIndexOf("/")+1,filename.size()-filename.lastIndexOf("/"));
        if(getfilename.contains("MPlayer"))
        {
            getfilename="软件";
             emit sendCmdStatus("17","******正在更新"+getfilename+"******");
        }
        else
        {
        emit sendCmdStatus("17","******正在更新文件******\n"+getfilename+"");
        }
        qDebug()<<"filename:"<<filename;
        strHex=strHex.mid(0,28)+"000000000000000000000000000000000000000000000000000";
        rebackData(cmd.rebackInfor(strHex,"a8"));
        IsCmdRuning=false;
        qDebug()<<"正在更新文件";

    }
    if(cmdHead=="18")//用户提示异地换车！
    {
      emit sendCmdStatus("18","1");
      rebackData(cmd.rebackInfor(strHex,"a8"));
      IsCmdRuning=false;
    }
    if(cmdHead=="19")//远程设时间，查看版本号，远程重启
    {
        QString flag=strHex.mid(74,2);
        if(flag=="01")//设置时间
        {
        QString timestr=strHex.mid(28,14);
        setDate(timestr);
        rebackData(cmd.rebackInfor(strHex,"a1"));
        }
        else if(flag=="02")//读取版本号
        {
        config conf;
        QString copyright=conf.read("copyright","copyright").toString();
        strHex.replace(42,8,copyright);
        rebackData(cmd.rebackInfor(strHex,"a2"));
        }
        else if(flag=="0f")//重启系统
        {
            emit sendCmdStatus("19","1");
            rebackData(cmd.rebackInfor(strHex,"af"));
            msleep(3000);
            QProcess::execute("reboot");
        }
        else
        {
        //##############rebackData(cmd.rebackInfor(strHex,"ee"));
        }  
        IsCmdRuning=false;
    }
    qDebug()<<"idelData函数运行时间："<<time.elapsed()/1000.0<<"s";
    }
    if((!IsCmdRuning))//如果当前没有命令在执行，则执行网络
    {
    runNetworkcmd();
    IsCmdRuning=false;
    }
   // qDebug()<<"idelData end 运行时间："<<time.elapsed()/1000.0<<"s";

}

void receiveDataFromServer::takeCode(int index,QString code)//用户扫描二维码处理,第一个参数2是手动输入，还是直接扫条码1
{
    timecountNodataflag=0;
    if(!IsCmdRuning)
    {
    datebase.updateuserData();
    IsCmdRuning=true;
    drawKeyThread->pauseThread();//暂停线程
    bool hastakeok=false;//是否有在队列中
    msleep(1500);
    int typeCode=0;//是二维码还是刷卡，1密码，2，扫条码，3刷卡
    bool isHaveDate=false;//是否有数据在位
    QSqlQuery query;
    QString sql="select * from userdata where code='"+code+"' or cardid='"+code+"'";
    qDebug()<<sql;
    qDebug()<<"***********************database**************************";
    datebase.readData();
    qDebug()<<"***********************database***********************";
    if(query.exec(sql))
    {
        qDebug()<<"#############uery.exec(sql)###########";
        if(query.next())
        {
        hastakeok=true;
        int id=query.value(0).toInt();
        QString dataCode=query.value(3).toString();
        QString dataCardId=query.value(4).toString();
        QString comd=query.value(2).toString();
        qDebug()<<"#############takecode###query.next()########";
        qDebug()<<dataCode<<"   "<<dataCardId<<"  "<<comd ;
        if((dataCode==code)||(dataCardId==code))
        {
            datebase.deletData(id);
            isHaveDate=true;
            if(dataCardId==code)
            {
            typeCode=3;//刷卡
            }
            if(dataCode==code)
            {
            typeCode=2;//二维码输入
            }

            cmdCurrentRun=comd;
            cmdHead=cmd.getCmdHead(comd);
            drawnumber=cmd.getCabinetNo(comd);

            QString drawnostr=QString::number(drawnumber);
            if(drawnostr.size()==1)
                drawnostr="000"+drawnostr;
            if(drawnostr.size()==2)
                drawnostr="00"+drawnostr;

            /*****************命令02，03取还钥匙开抽屉*******************/
             if(cmdHead=="02")//取钥匙
             {
                 IsCmdRuning=true;
                 QString strHex=comd;
                 drawnumber=cmd.getCabinetNo(strHex);
                 int lockId=cmd.getCabinetNo(strHex)+100;
                 QString cardid=readCardId(lockId);
                 QString cardflag="00";
                 if(cardid.size()<10)
                 {
                      msleep(400);
                 cardid=readCardId(lockId);
                 }
                 if(cardid.size()<10)
                 {
                      msleep(400);
                 cardid=readCardId(lockId);
                 }
                 if(cardid.size()>10)
                 {
                 cardflag=cardid.mid(10,2);
                 cardid=cardid.mid(0,10);
                 }
                // QString serverCardId=cmd.getCardId(strHex);
                 QString openFlag=openDraw(drawnumber);
                 if(openFlag!="01")
                 {
                 openFlag=openDraw(drawnumber);
                 }
                 if(openFlag!="01")
                 {
                 openFlag=openDraw(drawnumber);
                 }

                      QString numberno=cmd.getCmdNumber(strHex);//命令流水号
                      QString newcode=code+"ffffffffffff";
                      newcode=newcode.mid(0,12);
                      QString backserver="ba55b55b0df2"+CabinetNo+drawnostr+"0000"+numberno+"00"+cardid+newcode+"0000000000000000000000";
                        if(openFlag=="01")//抽屉打开成功
                        {
                        emit sendCmdStatus("02","02");
                        antocloseTime=closeTimeFlag;//重置自动关闭时间
                        if(index==2)//密码输入
                        {
                            backserver=backserver+"b4";
                        }
                        if(index==1)//二位码输入
                        {
                            backserver=backserver+"b5";
                        }
                        backserver=cmd.rebackInfor(backserver);
                        rebackData(backserver);//上传
                        int ii=0;
                        while(true)
                        {
                                if(antocloseTime>1)//等待用户拿走钥匙
                                {
                                    ii++;
                                    qDebug()<<ii;
                                    if(ii>closeTimeFlag*4)
                                    {
                                    emit signalautoCloseTimer(1);
                                    antocloseTime=-4;
                                    break;
                                    }

                                   QString flag =readCardId(drawnumber+100);
                                   if(flag=="01")
                                   {
                                   }
                                   if(flag=="02")
                                   {

                                   }
                                   if(flag=="03")
                                   {
                                    msleep(1500);
                                   antocloseTime=-5;//
                                   emit sendTimeTipUser(0);
                                   emit  signalautoCloseTimer(1);
                                   break;
                                   }
                                }
                                else
                                    break;
                        }
                        }
                        else
                        {

                                backserver=backserver+"e1";
                                backserver=cmd.rebackInfor(backserver);
                                rebackData(backserver);//上传
                                IsCmdRuning=false;

                        }

             }
             /********************start cmd 03***********************/
      if(cmdHead=="03")//还
        {
          IsCmdRuning=true;
           drawKeyThread->pauseThread();//暂停线程
          QString strHex=comd;
           drawnumber=cmd.getCabinetNo(strHex);
           QString carid=cmd.getCardId(strHex);
            QString openFlag=openDraw(drawnumber);
            if(openFlag!="01")
            {
            openFlag=openDraw(drawnumber);
            }
            if(openFlag!="01")
            {
            openFlag=openDraw(drawnumber);
            }
            if(openFlag=="01")//抽屉打开成功
               {
                 QString numberno=cmd.getCmdNumber(strHex);//命令流水号
                 QString newcode=code+"ffffffffffff";
                 newcode=newcode.mid(0,12);
                 QString backserver;
                 if(index==1)//二维码
                 {
                     backserver="ba55b55b0df2"+CabinetNo+drawnostr+"0000"+numberno+"00"+carid+newcode+"0000000000000000000000";
                     backserver=backserver+"a5";
                 }
                 else if(index==2)//密码
                 {
                     backserver="ba55b55b0df2"+CabinetNo+drawnostr+"0000"+numberno+"00"+carid+newcode+"0000000000000000000000";
                    backserver=backserver+"a4";
                 }
                 else //刷卡
                 {
                    backserver="ba55b55b0df2"+CabinetNo+drawnostr+"0000"+numberno+"00"+carid+"000000000000"+"0000000000000000000000";
                    backserver=backserver+"a3";
                 }
                 backserver=cmd.rebackInfor(backserver);
                 rebackData(backserver);//上传

                 antocloseTime=closeTimeFlag;//重置自动关闭时间
                 emit sendCmdStatus("03","02");//提示放入钥匙

                 int ii=0;
                    while(true)
                   {
                        if(antocloseTime>0)//等待用户放入钥匙
                        {
                            ii++;
                            qDebug()<<ii;
                            if(ii>closeTimeFlag*4)
                            {
                            emit signalautoCloseTimer(1);
                            antocloseTime=-4;
                            break;
                            }

                            QString flag =readCardId(drawnumber+100);
                            if(flag.size()>10)
                            {
                                flag=flag.mid(10,2);
                            }
                            if(flag=="01")//钥匙正确
                            {
                                 msleep(1500);
                                 antocloseTime=-5;//
                                 emit sendTimeTipUser(0);
                                 emit  signalautoCloseTimer(1);

                                 break;
                            }
                            if(flag=="02")
                            {
                                 emit sendCmdStatus("03","钥匙错误,请放入你正确的钥匙！！！");//提示放入钥匙
                            }
                            if(flag=="03")
                            {

                            }
                        }
                        else
                            break;
                    }
                   //提示用户放入钥匙
               }
               else
              {
               rebackData(cmd.rebackInfor(strHex,"E1"));//门打开失败
               IsCmdRuning=false;
               }
          }

      /**********************end cmd 03**************************************/
        }
        }
    }
    else
    {
    qDebug()<<"************database takecode*********"<<query.lastError().text();
    datebase.openDatabase();
    }
/**********************************************************/

sql="select * from borrow where cardid='"+code+"'";
bool operaFalg=false;
if(index==3)
{
    operaFalg=true;
}
else
{
    operaFalg=false;
}
if((!hastakeok)&&(operaFalg))//如果队列中没有，则找取出记录中拿取
{
if(query.exec(sql))
{
    if(query.next())
        {
         IsCmdRuning=true;
         drawKeyThread->pauseThread();
         msleep(1500);
         QString dataCardId=query.value(1).toString();
         QString drawno=query.value(3).toString();
         QString drawnotr=drawno;
           if(drawnotr.size()==1)
           {
           drawnotr="000"+drawnotr;
           }
           if(drawnotr.size()==2)
           {
           drawnotr="00"+drawnotr;
           }
           cmdCurrentRun="aa55a55a03fc0005"+drawnotr+"0000"+getNumberflag()+"00"+code+"000000000000"+"0000000000000000000000086f";
           cmdHead=cmd.getCmdHead(cmdCurrentRun);
           drawnumber=cmd.getCabinetNo(cmdCurrentRun);

        isHaveDate=true;
        typeCode=3;//刷卡
        drawnumber=drawno.toInt();
        QString openFlag=openDraw(drawnumber);
         if(openFlag!="01")
         {
         openFlag=openDraw(drawnumber);
         }
          if(openFlag=="01")//抽屉打开成功
            {
             QString numberno=cmd.getCmdNumber(cmdCurrentRun);//获得流水号
             returnbyidnumber=numberno;
             QString backserver="ba55b55b0df2"+CabinetNo+drawnotr+"0000"+numberno+"00"+dataCardId+"000000000000"+"0000000000000000000000a2";
             backserver=cmd.rebackInfor(backserver);
             rebackData(backserver);//上传

                 antocloseTime=closeTimeFlag;//重置自动关闭时间
                 emit sendCmdStatus("03","02");//提示放入钥匙
                 int ii=0;
                 while(true)
                 {
                     if(antocloseTime>0)//等待用户放入钥匙
                     {
                         ii++;
                         qDebug()<<ii;
                         if(ii>closeTimeFlag*4)
                         {
                         emit signalautoCloseTimer(1);
                         antocloseTime=-4;
                         break;
                         }

                         port.WriteMyCom(guncmd.readCardId(drawnumber+100));//codeNumber
                         QString inforr=port.ReadMyCom(200);
                         qDebug()<<"flash:"<<inforr;
                         QString backcmd,handcmd,statuscmd;//执行命令返回的数据，命令头，命令执行结果返回
                         if(inforr.size()>=18)
                         {
                         backcmd=inforr.mid(inforr.size()-18,18);
                         handcmd=backcmd.mid(0,2);
                         statuscmd=backcmd.mid(14,2);
                         if(handcmd=="bb")
                         {
                         if(statuscmd=="01")//放入钥匙正确，提示用户
                         {
                         msleep(1500);
                         antocloseTime=-5;//放入钥匙正确，提示用户
                         emit sendTimeTipUser(0);
                         emit  signalautoCloseTimer(1);
                         break;
                         }
                         else if(statuscmd=="02")//钥匙错误
                         {
                          emit sendCmdStatus("03","钥匙错误,请发放入正确钥匙");//提示放入钥匙
                         }
                         }
                         }
                     }
                     else
                         break;
                 }
                //提示用户放入钥匙
            }
            else//抽屉打开失败
            {
              QString numberno=cmd.getCmdNumber(cmdCurrentRun);//获得流水号
              QString backserver="ba55b55b0df2"+CabinetNo+drawnotr+"0000"+numberno+"00"+dataCardId+"000000000000"+"0000000000000000000000e1";
              backserver=cmd.rebackInfor(backserver);
              rebackData(backserver);//上传
              IsCmdRuning=false;
            }

       }
   }
}
/**********************************************************/
   if(isHaveDate)//有条码
   {
       // emit sendcodestatus(index,"01");
       emit sendcodestatus(index,"01");
   }
   else
   {
       IsCmdRuning=false;
     qDebug()<<"no code no code &&&&&&&&&&&&&&&";
     if(typeCode<3)
        {
        //等待队列中没有条码，则返回该条码数据给服务器
        emit sendcodestatus(index,"00");
        backcodetoServer(index,code);
        }
        else
        {
        emit sendcodestatus(3,"00");//刷卡返回用户显示
        backcodetoServer(3,code);
        }

    }

    }
}
void receiveDataFromServer::timeTimer()//条码失效时间定时函数，以及心跳包发送
{
    qDebug()<<"main running ok!";
    timePacktoserver++;
    if(((timePacktoserver/10)==1)||((timePacktoserver/10)==2))
    {
        if(timePacktoserver%10==0)
        {
        updateManagerChange();
        }
    }
    /*********************************************************/

    if(timePacktoserver>=24)
    {
    timePacktoserver=0;
    packToServer();
    }

    emit sigalhasdata();

    if(IsCmdRuning)
    {
    emit signaldata(3);
    }
    else
    {
    emit signaldata(0);
    }

}
QString receiveDataFromServer::getNumberflag()//获取流水号
{
    QString number=QString::number(numberFlag,16);
    if(numberFlag<65535)
    {
    numberFlag++;
    }
    else
    {
     numberFlag=40959;
    }
    qDebug()<<"#####################################"<<numberFlag;
    QString strcmd="echo \""+QString::number(numberFlag)+"\""+">atempnumber";//newsted number
    system(strcmd.toAscii());
    return number;
}
void receiveDataFromServer::backcodetoServer(int index, QString code)
{
    QString str="BA55B55B09F6"+CabinetNo+"00000000"+getNumberflag()+"00";
    if(index==1)
    {
        code=code+"ffffffffffff";
        code=code.mid(0,12);
        str=str+"0000000000"+code+"0000000000000000000000"+"a5";
        str=cmd.rebackInfor(str);
        qDebug()<<"BACKCODE:"<<str;
        rebackData(str);
    }
    if(index==2)
    {
        code=code+"ffffffffffff";
        code=code.mid(0,12);
        str=str+"0000000000"+code+"0000000000000000000000"+"a4";
        str=cmd.rebackInfor(str);
        rebackData(str);
    }
    if(index==3)//刷卡获得车钥匙id;
    {
        str=str+code+"000000000000"+"0000000000000000000000"+"a3";
        qDebug()<<"getinfor:"<<str<<"   size:"<<str.size();
        str=cmd.rebackInfor(str);
        rebackData(str);
    }

}
void receiveDataFromServer::autoCloseTimer()
{
    useruifalg++;
    timecountNodataflag++;
    long timelabel=QDateTime::currentDateTime().toString("hhmmss").toLong();
    if((timelabel>60045)&&(timelabel<60059))//自动重启
    {
        QProcess::execute("reboot");
    }
    if((timelabel>60000)&&(timelabel<60002))
    {
        if(networkmode=="1")//4G网络
        {
         qDebug()<<"reset model 4《《《《《《《《《《《《《《《《《《《《《《《《《《";
         QString timecurrent=QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
         QString logdata="echo \""+timecurrent+"\" >signalSever.txt";
         system(logdata.toAscii());
        }
    }
    if(!IsCmdRuning)
    {
     emit sigalhasdata();
    }
    if(antocloseTime>=1)
    {
        QTime time;
        time.start();
        IsCmdRuning=true;
        drawKeyThread->pauseThread();//暂停线程
        msleep(1000);
        qDebug()<<"current cmd is :"<<cmdCurrentRun;
        emit sendTimeTipUser(antocloseTime);
        if(antocloseTime==1)
        {
            antocloseTime=-5;
            QString statusflag=closeDraw(drawnumber);
            if(statusflag!="01")
            {
            statusflag=closeDraw(drawnumber);
            }
            if(statusflag!="01")
            {
            statusflag=closeDraw(drawnumber);
            }
            cmdHead=cmd.getCmdHead(cmdCurrentRun);
            if(statusflag=="01")//关闭抽屉成功
            {

                qDebug()<<"clase draw ok! and current cmd:"<<cmdCurrentRun;
                int drawno=cmd.getCabinetNo(cmdCurrentRun);
                QString carid=readCardId(drawno+100);
                if(carid.size()<10)
                {
                    msleep(400);
                carid=readCardId(drawno+100);
                }
                if(carid.size()<10)
                {
                     msleep(800);
                carid=readCardId(drawno+100);
                }
                if(carid.size()<10)
                {
                     msleep(800);
                carid=readCardId(drawno+100);
                }
                if(carid.size()>10)//钥匙在
                {
                QString id,flag;
                id=carid.mid(0,10);
                flag=carid.mid(carid.size()-2,2);
                qDebug()<<"flag:"<<flag;
                QString drawnostr=QString::number(drawno);
                if(drawnostr.size()==1)
                    drawnostr="000"+drawnostr;
                if(drawnostr.size()==2)
                    drawnostr="00"+drawnostr;
                QString cmdnumber=cmd.getCmdNumber(cmdCurrentRun);//流水号
                QString backserver="ba55b55b07f8"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000";

                     if(flag=="01")//钥匙匹配
                     {
                         if(cmdHead=="01")
                         {
                            emit sendCmdStatus("01","02");//抽屉已经关闭
                            backserver=backserver+"a0";
                         }
                         else if(cmdHead=="02")
                         {
                         backserver=backserver+"e0";
                          emit sendCmdStatus("02","04");
                         }
                         else if(cmdHead=="03")
                         {
                         emit sendCmdStatus("03","03");
                         datebase.deletBorrow(cmd.getCardId(cmdCurrentRun));
                         if(returnbyidnumber.size()>1)
                         {

                         backserver=backserver.replace(24,4,returnbyidnumber);
                         returnbyidnumber.clear();
                         }
                         backserver=backserver+"a2";
                         QString cmdstatus=cmd.getCmdFlag(cmdCurrentRun);
                         if(cmdstatus=="09")//用户换车
                         {
                             emit sendCmdStatus("03","04");//提示正在处理
                             qDebug()<<"用户换车";
                         }
                         }
                         else if(cmdHead=="16")
                         {
                            backserver="ba55b55b0cf3"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000a8";
                         }
                         else
                         {
                              qDebug()<<"钥匙柜返回异常："<<cmdHead;
                             backserver=backserver+"ef";
                         }
                      }
                     if(flag=="02")//钥匙不匹配
                     {
                         if(cmdHead=="01")
                         {
                          emit sendCmdStatus("01","02");//抽屉已经关闭
                          backserver=backserver+"a0";
                         }
                         else if(cmdHead=="02")
                         {
                         backserver=backserver+"a1";
                         emit sendCmdStatus("02","03");
                         datebase.writeData("",cmd.getCardId(cmdCurrentRun),QString::number(drawnumber));
                         }
                         else if(cmdHead=="03")
                         {
                         backserver=backserver+"e2";
                         }
                         else if(cmdHead=="16")
                         {
                         backserver="ba55b55b0cf3"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000e5";
                         }
                         else
                         {
                             qDebug()<<"钥匙柜返回异常："<<cmdHead;
                          backserver=backserver+"ef";
                         }
                     }
                     backserver=cmd.rebackInfor(backserver);
                     qDebug()<<"backserver:"<<backserver;


                     updateStatus(0,drawno,1);
                     packToServer();
                     msleep(100);
                     rebackData(backserver);//上传
                     msleep(100); 
                     packToServer();
                }
                else //钥匙不在
                {
                    QString drawnostr=QString::number(drawno);
                    if(drawnostr.size()==1)
                        drawnostr="000"+drawnostr;
                    if(drawnostr.size()==2)
                        drawnostr="00"+drawnostr;
                     QString cmdnumber=cmd.getCmdNumber(cmdCurrentRun);//流水号
                     qDebug()<<"抽屉号："<<drawnostr;
                QString backserver="ba55b55b07f8"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+"0000000000"+"0000000000"+"000000000000000000000000";
                QString id="0000000000";
                switch(cmdHead.toInt())
                {
                case 1:
                    emit sendCmdStatus("01","02");//抽屉已经关闭
                    backserver=backserver+"a0";
                    break;
                case 2:
                    backserver=backserver+"a1";//取成功
                     emit sendCmdStatus("02","03");
                    datebase.writeData("",cmd.getCardId(cmdCurrentRun),QString::number(drawnumber));

                    break;
                case 3:
                    backserver=backserver+"e9";//还失败
                    break;
                case 16:
                    backserver="ba55b55b0cf3"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000e5";//管理员换失败
                    break;
                default:
                    backserver=backserver+"e9";//还失败
                    break;


                }
                updateStatus(0,drawno,0);
                packToServer();
                msleep(10);
                backserver=cmd.rebackInfor(backserver);
                rebackData(backserver);//上传
                msleep(10);
                packToServer();
                }

            }
            else//关闭抽屉失败
            {
                QString drawstr="0000"+QString::number(drawnumber,16);
                drawstr=drawstr.mid(drawstr.size()-4,4);
                QString id="0000000000";
                QString cmdnumber=cmd.getCmdNumber(cmdCurrentRun);//流水号
                QString backserver="ba55b55b08f7"+CabinetNo+drawstr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000";
                if(cmdHead=="01")
                {

                }
                else if(cmdHead=="02")
                {
                 backserver=backserver+"e5";
                }
                else if(cmdHead=="03")
                {
                backserver=backserver+"e6";
                }
                else if(cmdHead=="16")
                {
                backserver=backserver+"e7";
                }
                else
                {

                }
                backserver=cmd.rebackInfor(backserver);
                rebackData(backserver);//上传
            }
            qDebug()<<"autoCloseTimer函数运行时间："<<time.elapsed()/1000.0<<"s";

            cmdCurrentRun.clear();
            IsCmdRuning=false;
            returnbyidnumber.clear();
        }
    }
    else
    {
        if(antocloseTime==-60)
        {
            qDebug()<<"自动关闭-60";
        IsCmdRuning=false;
        }
    }
    antocloseTime--;
}
void receiveDataFromServer::autoCloseTimer(int index)
{
            QTime time;
            time.start();
            qDebug()<<"current cmd is 1:"<<cmdCurrentRun;
            IsCmdRuning=true;
            drawKeyThread->pauseThread();//暂停线程
            QString statusflag=closeDraw(drawnumber);
            if(statusflag!="01")
            {
            statusflag=closeDraw(drawnumber);
            }
            if(statusflag!="01")
            {
            statusflag=closeDraw(drawnumber);
            }
            cmdHead=cmd.getCmdHead(cmdCurrentRun);
            if(statusflag=="01")//关闭抽屉成功
            {
                qDebug()<<"clase draw ok! and current cmd:"<<cmdCurrentRun;
                int drawno=cmd.getCabinetNo(cmdCurrentRun);
                QString carid=readCardId(drawno+100);
                if(carid.size()<10)
                {
                msleep(400);
                carid=readCardId(drawno+100);
                }
                if(cmdHead=="03")
                {
                 if(carid.size()<10)
                  {
                   msleep(1000);
                  carid=readCardId(drawno+100);
                 }
                  if(carid.size()<10)
                  {
                  msleep(1000);
                  carid=readCardId(drawno+100);
                  }
                }
                if(carid.size()>10)//钥匙在
                {
                QString id,flag;
                id=carid.mid(0,10);
                flag=carid.mid(carid.size()-2,2);
                qDebug()<<"key status flag:"<<flag;
                QString drawnostr=QString::number(drawno);
                if(drawnostr.size()==1)
                    drawnostr="000"+drawnostr;
                if(drawnostr.size()==2)
                    drawnostr="00"+drawnostr;
                QString cmdnumber=cmd.getCmdNumber(cmdCurrentRun);//流水号 
                QString backserver="ba55b55b07f8"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000";
                qDebug()<<"##################################################\n";
                 qDebug()<<"##################################################\n";

                   qDebug()<<"cmd:"<<cmdCurrentRun;
                   qDebug()<<"get serial number:"<<cmdnumber;
                   qDebug()<<"back cmd:"<<backserver;

                   qDebug()<<"##################################################\n";
                    qDebug()<<"##################################################\n";
                     if(flag=="01")//钥匙匹配
                     {
                         qDebug()<<"钥匙匹配：cmdHead"<<cmdHead;
                         if(cmdHead=="01")
                         {
                            emit sendCmdStatus("01","02");//抽屉已经关闭
                            backserver=backserver+"a0";
                         }
                         else if(cmdHead=="02")
                         {
                         backserver=backserver+"e0";
                         emit sendCmdStatus("02","04");
                         }
                         else if(cmdHead=="03")
                         {

                         emit sendCmdStatus("03","03");
                         datebase.deletBorrow(cmd.getCardId(cmdCurrentRun));
                         if(returnbyidnumber.size()>1)
                         {
                             backserver=backserver.replace(24,4,returnbyidnumber);
                             returnbyidnumber.clear();
                         }
                         backserver=backserver+"a2";
                         QString cmdstatus=cmd.getCmdFlag(cmdCurrentRun);
                         if(cmdstatus=="09")//用户换车
                         {
                             emit sendCmdStatus("03","04");//提示正在处理
                             qDebug()<<"用户换车";
                         }
                         }
                         else if(cmdHead=="16")
                         {
                            backserver="ba55b55b0cf3"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000a8";

                         }
                         else
                         {
                             qDebug()<<"钥匙柜返回异常："<<cmdHead;
                             backserver=backserver+"ef";
                         }
                      }
                     if(flag=="02")//钥匙不匹配
                     {
                         if(cmdHead=="01")
                         {
                          emit sendCmdStatus("01","02");//抽屉已经关闭
                          backserver=backserver+"a0";
                         }
                         else if(cmdHead=="02")
                         {
                         backserver=backserver+"a1";
                         emit sendCmdStatus("02","03");
                         datebase.writeData("",cmd.getCardId(cmdCurrentRun),QString::number(drawnumber));
                         }
                         else if(cmdHead=="03")
                         {
                         backserver=backserver+"e2";
                         emit sendCmdStatus("03","05");
                         }
                         else if(cmdHead=="16")
                         {
                          backserver="ba55b55b0cf3"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000e5";
                         }
                         else
                         {
                             qDebug()<<"钥匙柜返回异常："<<cmdHead;
                          backserver=backserver+"ef";
                         }
                     }
                     backserver=cmd.rebackInfor(backserver);
                     qDebug()<<"backserver:"<<backserver;

                     updateStatus(0,drawno,1);
                     packToServer();
                     msleep(100);
                     rebackData(backserver);//上传
                     msleep(100);
                     packToServer();
                }
                else //钥匙不在
                {
                    QString drawnostr=QString::number(drawno);
                    if(drawnostr.size()==1)
                        drawnostr="000"+drawnostr;
                    if(drawnostr.size()==2)
                        drawnostr="00"+drawnostr;
                     QString cmdnumber=cmd.getCmdNumber(cmdCurrentRun);//流水号
                     qDebug()<<"抽屉号："<<drawnostr;
                QString backserver="ba55b55b07f8"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+"0000000000"+"0000000000"+"000000000000000000000000";
             QString id="0000000000";
                switch(cmdHead.toInt())
                {
                case 1:
                    emit sendCmdStatus("01","02");//抽屉已经关闭
                    backserver=backserver+"a0";
                    break;
                case 2:
                    backserver=backserver+"a1";//取成功
                     emit sendCmdStatus("02","03");
                    datebase.writeData("",cmd.getCardId(cmdCurrentRun),QString::number(drawnumber));

                    break;
                case 3:
                    backserver=backserver+"e9";//还失败
                    emit sendCmdStatus("03","05");
                    break;
                case 16:
                    backserver="ba55b55b0cf3"+CabinetNo+drawnostr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000e5";//管理员换失败
                    break;
                default:
                    backserver=backserver+"e9";//还失败
                    break;


                }
                updateStatus(0,drawno,0);
                packToServer();
                msleep(100);
                backserver=cmd.rebackInfor(backserver);

                rebackData(backserver);//上传
                msleep(100);
                packToServer();
                }

            }
            else//关闭抽屉失败
            {
                QString drawstr="0000"+QString::number(drawnumber,16);
                drawstr=drawstr.mid(drawstr.size()-4,4);
                QString id="0000000000";
                QString cmdnumber=cmd.getCmdNumber(cmdCurrentRun);//流水号
                QString backserver="ba55b55b08f7"+CabinetNo+drawstr+"0000"+cmdnumber+"00"+id+"0000000000"+"000000000000000000000000";
                if(cmdHead=="01")
                {

                }
                else if(cmdHead=="02")
                {
                 backserver=backserver+"e5";
                 emit sendCmdStatus("02","04");
                }
                else if(cmdHead=="03")
                {
                backserver=backserver+"e6";
                emit sendCmdStatus("02","05");
                }
                else if(cmdHead=="16")
                {
                backserver=backserver+"e7";
                }
                else
                {

                }
                backserver=cmd.rebackInfor(backserver);
                rebackData(backserver);//上传
            }
    msleep(100);
    cmdCurrentRun.clear();
    returnbyidnumber.clear();
    IsCmdRuning=false;

    qDebug()<<"autoCloseTimer(int)函数运行时间："<<time.elapsed()/1000.0<<"s";
}


void receiveDataFromServer::getReadKeyStatues(QString g_key)//读取钥匙柜状态，数据获取从KeyStatus;
{
    qDebug()<<"收到钥匙柜状态："<<g_key;
}
void receiveDataFromServer::updateKeyStatus(QString str)
{
    keyStatus=str;
}
void receiveDataFromServer::updateDrawStatus(QString str)
{
    drawStatus=str;

}
void receiveDataFromServer::updateWarningStatus(QString str)
{
    if(str=="01")//正常
    {

    }
    if(str=="02")//震动报警，并上传到服务器
    {
      QString infor="ba55b55b0bf4"+CabinetNo+"0000"+"0000"+getNumberflag()+"0000000000000000000000000000000000000000000000"+"eb";
          infor=cmd.rebackInfor(infor);
          rebackData(infor);
    }

}
void receiveDataFromServer::updatecodeStatus(QString str)
{
    if(str.size()>5)//密码至少为六位
    {
    emit sendSoundPlay(33);//刷卡语音
    takeCode(3,str);
    qDebug()<<"主线程中获得扫卡区的获取的钥匙ID："<<str;
    }
}

bool receiveDataFromServer::packToServer()
{
    QString AllCabinetnumber;//柜体抽屉总数
    QString numberNo=getNumberflag();//流水号
    if(maxDrawNo<=9)
    {
      AllCabinetnumber="000"+QString::number(maxDrawNo);
    }
    else
    {
      AllCabinetnumber="00"+QString::number(maxDrawNo);
     if(maxDrawNo>=100)
         AllCabinetnumber="0"+QString::number(maxDrawNo);
    }

    QString heartbeatackage="ba55b55b06f9"+CabinetNo+"0000"+AllCabinetnumber+numberNo+"0000000000000000"+drawStatus+keyStatus+"0000000000a8";
    heartbeatackage=cmd.rebackInfor(heartbeatackage);
    qDebug()<<"hearpakage:"<<heartbeatackage;
    if(rebackData(heartbeatackage))//
        return true;
        else
        return true;
}
void receiveDataFromServer::setThread(int index)
{  
    emit signalthread(index);
}
void receiveDataFromServer::threadRunset(int index)
{
    useruifalg=0;//用户界面响应标志
    if(IsCmdRuning)
    {

    }
    else
    {
        if(index==0)//暂停线程
        {
        drawKeyThread->pauseThread();
        }
        else
        {
            if(!IsCmdRuning)
            {
            qDebug()<<"###########&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&start thread&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"<<IsCmdRuning;
            drawKeyThread->startThread();
            }
        }
    }
}
QString receiveDataFromServer::readCardId(int lockId)
{
    QMutex lock;
    lock.lock();
    IsCmdRuning=true;
    drawKeyThread->pauseThread();//暂停线程
    QString inforr;
    port.WriteMyCom(guncmd.readCardId(lockId));
    QString drawstr=QString::number(lockId,16);
    if(drawstr.size()==1)
    {
        drawstr="0"+drawstr;
    }

    int timeoutflag=0;
    while(true)
    {
        timeoutflag++;
        inforr=port.ReadMyCom(200);
        qDebug()<<"readCardId getinfor:"<<inforr;
        if(inforr.contains("bb"+drawstr))
        {
        inforr=inforr+port.ReadMyCom(100);
        inforr=inforr.mid(inforr.indexOf("bb"+drawstr),18); //一次读取发送和返回数据
        break;
        }
        if(timeoutflag>8)
        {
        timeoutflag=0;
        break;
        }

    }

    QString handcmd=inforr.mid(0,2);
    QString statuscmd=inforr.mid(14,2);
    lock.unlock();
    if(handcmd=="bb")
    {

      if(cmd.check_cmdSun(inforr))//核对校验和
      {
          qDebug()<<"核对校验和";
      }
      else
      {
          return "00";
      }

      qDebug()<<handcmd<<"   "<<statuscmd;
      if((statuscmd=="01")||(statuscmd=="02"))//
      {
          QString code=inforr.mid(4,10);
          return code+statuscmd;
      }
       else
      {
         return "03";
      }
    }
    else
    {
       return "00";
    }

}
QString receiveDataFromServer::openDraw(int index)
{
    IsCmdRuning=true;
    timecountNodataflag=0;
    drawKeyThread->pauseThread();//暂停线程
     port.WriteMyCom(guncmd.open_command(index));
     QString inforr;//=port.ReadMyCom(2000);
     QString statusflag="00";
     QString drawstr=QString::number(index,16);
     if(drawstr.size()==1)
     {
         drawstr="0"+drawstr;
     }
     int timeoutflag=0;
     while(true)
     {
         timeoutflag++;
         inforr=port.ReadMyCom(200);
         qDebug()<<"opendraw draw:"<<inforr;
         if(inforr.contains("b3"+drawstr))
         {
         inforr=inforr+port.ReadMyCom(200);
         inforr=inforr.mid(inforr.indexOf("b3"+drawstr),18); //一次读取发送和返回数据
         timeoutflag=0;
         statusflag=inforr.mid(14,2);
         break;
         }
         if(timeoutflag>30)
         {
         timeoutflag=0;
         break;
         }
     }
     return statusflag;

}
QString receiveDataFromServer::writeCardId(int cardno,QString cardid)
{
    IsCmdRuning=true;
    drawKeyThread->pauseThread();//暂停线程
    port.WriteMyCom(guncmd.catCardIDw(cardno+100,cardid));
    QString inforr;//=port.ReadMyCom(200);
    int timeoutflag=0;
    while(true)
    {
        timeoutflag++;
        inforr=port.ReadMyCom(200);
        QString drawstr=QString::number(cardno+100,16);
        if(drawstr.size()==1)
        {
            drawstr="0"+drawstr;
        }
         qDebug()<<"writeCardId draw:"<<drawstr;
        if(inforr.contains("b4"+drawstr))
        {
        qDebug()<<"writeCardId infor:"<<inforr;
        inforr=inforr+port.ReadMyCom(200);
        inforr=inforr.mid(inforr.indexOf("b4"+drawstr),18); //一次读取发送和返回数据
        timeoutflag=0;
        qDebug()<<"writeCardId getinfor:"<<inforr;
        break;
        }
        if(timeoutflag>10)
        {
        timeoutflag=0;
        break;
        }
        if(timeoutflag==3)
        {
        port.WriteMyCom(guncmd.catCardIDw(cardno+100,cardid));
        }
    }
    if(inforr.size()>=18)
    {
        if(cmd.check_cmdSun(inforr))//校验和
        {
          QString statuscmd=inforr.mid(14,2);
             return statuscmd;
        }
    }
    else
    {
        return "00";
    }
    return "00";
}



bool receiveDataFromServer::getLinkServerStatus()
{
     return networkflag;
}
void receiveDataFromServer::getNetworkFlag(bool flag)
{
    networkflag=flag;
}
void receiveDataFromServer::updateStatus(int flag,int lockno, int status)//status 0关闭，1打开
{
int status1,status2,status3,status4,status5;
        if(flag>1)
        {
        status1=drawStatus.mid(0,2).toInt(0,16);
        status2=drawStatus.mid(2,2).toInt(0,16);
        status3=drawStatus.mid(4,2).toInt(0,16);
        status4=drawStatus.mid(6,2).toInt(0,16);
        status5=drawStatus.mid(8,2).toInt(0,16);
        }
        else
        {
            status1=keyStatus.mid(0,2).toInt(0,16);
            status2=keyStatus.mid(2,2).toInt(0,16);
            status3=keyStatus.mid(4,2).toInt(0,16);
            status4=keyStatus.mid(6,2).toInt(0,16);
            status5=keyStatus.mid(8,2).toInt(0,16);
        }
uchar b;
int numbera=lockno/8;//除数
int numberb=lockno%8;//余数
if(numberb==0)
{
    numbera=numbera-1;
    numberb=8;
}
switch(numberb)
{
case 1:
       b=0x01;break;
case 2:
       b=0x02;break;
case 3:
       b=0x04;break;
case 4:
       b=0x08; break;
case 5:
       b=0x10;break;
case 6:
       b=0x20;break;
case 7:
       b=0x40;break;
case 8:
       b=0x80;break;
}
int ch=0;
switch(numbera)
{
case 0:
    ch=status1&b; break;
case 1:
    ch=status2&b; break;
case 2:
    ch=status3&b; break;
case 3:
    ch=status4&b; break;
case 4:
    ch=status5&b;break;
}
int old=0;
//qDebug()<<"ch size:"<<ch;
if(ch>0)
{
  old=1;
}
if(old==status)
{
//位没有改变
}
else
{
    if(old==0)
    {
        switch(numbera)
        {
        case 0:
            status1=status1+b; break;
        case 1:
            status2=status2+b; break;
        case 2:
            status3=status3+b; break;
        case 3:
            status4=status4+b; break;
        case 4:
            status5=status5+b; break;
        }
    }
    else
    {
        switch(numbera)
        {
        case 0:
            status1=status1-b; break;
        case 1:
            status2=status2-b; break;
        case 2:
            status3=status3-b; break;
        case 3:
            status4=status4-b; break;
        case 4:
            status5=status5-b; break;
        }
    }
}

    QString str1,str2,str3,str4,str5;
    str1=QString::number(status1,16);
    str2=QString::number(status2,16);
    str3=QString::number(status3,16);
    str4=QString::number(status4,16);
    str5=QString::number(status5,16);
    if(str1.size()<2)
        str1="0"+str1;
    if(str2.size()<2)
        str2="0"+str2;
    if(str3.size()<2)
        str3="0"+str3;
    if(str4.size()<2)
        str4="0"+str4;
    if(str5.size()<2)
        str5="0"+str5;

if(flag>1)
{
    drawStatus=str1+str2+str3+str4+str5;
   // qDebug()<<"抽屉状态更新 :"<<drawstatus;
}
else
{
    keyStatus=str1+str2+str3+str4+str5;
   //  qDebug()<<"钥匙状态更新:"<<keystatus;
}

}
void receiveDataFromServer::updateManagerChange(int drawno,QString keyid)
{
    qDebug()<<"#############收到数据############：";
    if(keyid.isEmpty())//上传抽屉打开命令
    {

    }
    else//上传钥匙更换命令
    {

    }
}
void receiveDataFromServer::updateManagerChange()
{
    qDebug()<<"update manager operation!";
    QSqlQuery query,query1;
    QString sql="select * from mangerdata";
    QString deletesql="delete from mangerdata where id=";
    QString newkey;//管理员更换的钥匙号；
            QString AllCabinetnumber;//柜体抽屉总数
            if(maxDrawNo<=9)
            {
                AllCabinetnumber="000"+QString::number(maxDrawNo);
            }
            else
            {
                AllCabinetnumber="00"+QString::number(maxDrawNo);
             if(maxDrawNo>=100)
                 AllCabinetnumber="0"+QString::number(maxDrawNo);
            }


    if(query.exec(sql))
    {
    while(query.next())
    {
        QString numberNo=getNumberflag();//流水号
        qDebug()<<"上传管理员操作记录："<<query.value(0).toString();
        QString doorno=query.value(1).toString();
        if(doorno.size()==1)
        {
            doorno="000"+doorno;
        }
        if(doorno.size()==2)
        {
             doorno="00"+doorno;
        }
        if(doorno.size()==3)
        {
             doorno="0"+doorno;
        }
        if(query.value(3).toInt()==1)
        {
             QString senddrawdata="ba55b55b0af5"+CabinetNo+doorno+"0000"+numberNo+"0000000000000000000000000000000000000000000000a8";
             qDebug()<<"上传抽屉打开"<<senddrawdata;
             if(rebackData(cmd.rebackInfor(senddrawdata)))//成功上传
            {
                query1.exec(deletesql+query.value(0).toString());
            }
        }
        if(query.value(3).toInt()==2)
         {

             newkey=query.value(2).toString();
             QString sendkeydata="ba55b55b07f8"+CabinetNo+doorno+"0000"+numberNo+"00"+"0000000000"+newkey+"000000000000000000000000a3";
          //  qDebug()<<"上传管理员操更换钥匙"<<sendkeydata;
             if(rebackData(cmd.rebackInfor(sendkeydata)))//成功上传
            {
             query1.exec(deletesql+query.value(0).toString());
            }
         }
        //msleep(1000);
    }
    }
    else
    {
        qDebug()<<"sql err :"<<query.lastError().text();
    }
}
void receiveDataFromServer::setDate(QString timelistr)
{
    QProcess *date=new QProcess(this);
    QProcess *sync=new QProcess(this);
    QStringList cmdList;
    QString timelist=timelistr;
    timelist.insert(4,"-");
    timelist.insert(7,"-");
    timelist.insert(10," ");
    timelist.insert(13,":");
    timelist.insert(16,":");

    qDebug()<<timelist;
    cmdList<<"-s"<<timelist;
    date->start("date",cmdList);
    cmdList.clear();
    cmdList<<"-w";
    sync->start("hwclock",cmdList);

    if(date->waitForFinished(1000))
    {
        qDebug()<<"成功1";
    }

    if(sync->waitForFinished(1000))
    {
        qDebug()<<"成功2";
    }

}

void receiveDataFromServer::runNetworkcmd()
{
    if(cmdlistServer.size()>0)
    {
        QString strHex;
        strHex=cmdlistServer.first();
        cmdlistServer.removeFirst(); 
        qDebug()<<"server cmd is ok:"<<strHex;
        IsCmdRuning=true;
        drawKeyThread->pauseThread();
        drawnumber=cmd.getCabinetNo(strHex);
        msleep(1500);
        QStringList netcmdlist;

        if(strHex.size()>78)
        {
            if(strHex.contains("aa55a55a17e8"))//更新命令
            {
            netcmdlist<<strHex;
            }
            else
            {
            int cmdsize=strHex.size()/78;
            for(int i=0;i<cmdsize;i++)
            {
            QString cmd_r=strHex.mid(78*i,78);
            qDebug()<<"解析命令："<<cmd_r;
            netcmdlist<<cmd_r;
            }
            }
        }
        else
        {
        netcmdlist<<strHex;
        }

        for(int i=0;i<netcmdlist.size();i++)
        {
        strHex=netcmdlist.at(i);
        qDebug()<<"开始执行网络命令："<<strHex;
        cmdNumber<<cmd.getCmdNumber(strHex);
        QString usercode=cmd.codeType(strHex);
        QString carId=cmd.getCardId(strHex);
        qDebug()<<"usercode :"<<usercode;
        if(!usercode.isEmpty())//是否是二维码
        {
            codeStore code;
            code.timeLimit=0;
            code.command=strHex;
            code.code=usercode;
            code.cardid=carId;
            QString dateStr=QDateTime::currentDateTime().toString("yyyyMMhhmm");
            QString headcmd=cmd.getCmdHead(strHex);
            if(headcmd=="02")
            {
                drawnumber=cmd.getCabinetNo(strHex);
                int lockId=cmd.getCabinetNo(strHex)+100;
                QString cardid=readCardId(lockId);

                if(cardid.size()<10)
                {
                    msleep(1000);
                cardid=readCardId(lockId);
                }
                if(cardid.size()<10)
                {
                    msleep(2000);
                cardid=readCardId(lockId);
                }
                QString cardflag="00";
                if(cardid.size()>10)
                {
                cardflag=cardid.mid(10,2);
                cardid=cardid.mid(0,10);
                }
                else
                {
                      cardid=readCardId(lockId);
                      cardflag="00";
                      if(cardid.size()>10)
                      {
                       cardflag=cardid.mid(10,2);
                       cardid=cardid.mid(0,10);
                       }
                }

                QString serverCardId=cmd.getCardId(strHex);

                qDebug()<<"serverCarid:"<<serverCardId;
                qDebug()<<"cardid:"<<cardid;
                if(serverCardId==cardid)//服务器命令中的车id与抽屉的相同
                {

                  bool ok=datebase.writeData(dateStr,strHex,usercode,carId,QString::number(drawnumber));
                  if(ok)//写入数据库成功
                  {
                  datebase.deletBorrow(drawnumber);
                  datebase.readData();
                  rebackData(cmd.rebackInfor(strHex,"A7"));
                  emit sendCmdStatus("02","分配取"+QString::number(drawnumber)+"号抽屉钥匙二维码成功");
                  }
                }
                else
                {
                 emit sendCmdStatus("02","分配取"+QString::number(drawnumber)+"号抽屉钥匙二维码失败");
                 rebackData(cmd.rebackInfor(strHex,"e2"));
                }
            }

            if(headcmd=="03")
            {
                IsCmdRuning=true;
                drawKeyThread->pauseThread();
                drawnumber=cmd.getCabinetNo(strHex);

                QString servercardid=cmd.getCardId(strHex);
                if((servercardid=="ffffffffff")||(servercardid=="0000000000"))//发送空的钥匙过来，删掉原来的
                {
                int drowno=cmd.getCabinetNo(strHex);
                datebase.deletBorrow(drowno);
                rebackData(cmd.rebackInfor(strHex,"a7"));
                }
                else
                {
                QString writeflag=writeCardId(drawnumber,carId);
                msleep(200);
                if(writeflag!="04")
                {
                msleep(200);
                writeflag=writeCardId(drawnumber,carId);
                }
                if(writeflag!="04")
                {
                msleep(300);
                writeflag=writeCardId(drawnumber,carId);
                }
                if(writeflag=="04")
                {
                 datebase.deletBorrow(drawnumber);
                 bool ok=datebase.writeData(dateStr,strHex,usercode,carId,QString::number(drawnumber));
                 rebackData(cmd.rebackInfor(strHex,"A7"));
                 if(ok)
                     ;
                     else
                 {
                  datebase.writeData(dateStr,strHex,usercode,carId,QString::number(drawnumber));
                 }
                 emit sendCmdStatus("02","分配还"+QString::number(drawnumber)+"号抽屉钥匙二维码成功");
                }
                else if(writeflag=="01")
                {
                    rebackData(cmd.rebackInfor(strHex,"F0"));
                }
                else
                {
                    rebackData(cmd.rebackInfor(strHex,"F1"));
                }
                }//end (servercardid=="ffffffffff")||(servercardid=="0000000000")
            }

        }
        else//直接开抽屉相关的命令
        {
        queue<<strHex;
        emit sigalhasdata();
        }
        }
    }
    else
    {
    }
}

QString receiveDataFromServer::getnetworkTime()
{
       QUdpSocket udp_sock;
       udp_sock.connectToHost("time.windows.com",123);
        //发送请求时间的请求
        qint8 LI = 0;
        qint8 VN = 3;
        qint8 MODE = 3;
        qint8 STRATUM = 0;
        qint8 POLL = 4;
        qint8 PREC = -6;
        QDateTime epoch(QDate(1900, 1, 1));
        qint32 second = quint32(epoch.secsTo(QDateTime::currentDateTime()));
        qint32 temp = 0;
        QByteArray time_req(48, 0);
        time_req[0] = (LI <<6) | (VN <<3) | (MODE);
        time_req[1] = STRATUM;
        time_req[2] = POLL;
        time_req[3] = PREC & 0xff;
        time_req[5] = 1;
        time_req[9] = 1;
        time_req[40] = (temp=(second&0xff000000)>>24);
        temp = 0;
        time_req[41]=(temp=(second&0x00ff0000)>>16);
        temp = 0;
        time_req[42] = (temp=(second&0x0000ff00)>>8);
        temp = 0;
        time_req[43] = ((second&0x000000ff));

        QDateTime cur_dt;
        //阻塞等待连接成功
        if(udp_sock.waitForConnected(60000))
        {

            udp_sock.flush();
            //发送时间请求
            udp_sock.write(time_req);
            udp_sock.flush();

            //阻塞等待消息返回
            if(udp_sock.waitForReadyRead(120000))
            {

                QByteArray new_time;
                QDateTime epoch(QDate(1900, 1, 1));
                QDateTime unix_start(QDate(1970, 1, 1));
                while(udp_sock.hasPendingDatagrams())
                {

                    new_time.resize(udp_sock.pendingDatagramSize());
                    udp_sock.read(new_time.data(), new_time.size());

    }
                QByteArray TransmitTimeStamp ;
                TransmitTimeStamp = new_time.right(8);
                quint32 seconds=TransmitTimeStamp[0];
                quint8 temp=0;
                for(int j=1;j<=3;j++)
                {

                    seconds=seconds<<8;
                    temp=TransmitTimeStamp[j];
                    seconds=seconds+temp;

    }

                cur_dt.setTime_t(seconds- epoch.secsTo(unix_start));

    }
            else
            {

                cur_dt = QDateTime::currentDateTime();

    }

            udp_sock.disconnectFromHost();
            udp_sock.close();

    }
        else
        {

            QHostInfo info = QHostInfo::fromName("time.windows.com");
            QString ip_addr = info.addresses().first().toString();
            qDebug()<< (QString::fromLocal8Bit("不能连接到服务器%1，请检查网络！")).arg(ip_addr);
            cur_dt = QDateTime::currentDateTime();
    }
    qDebug()<<">>>>>>>>>>getnetworktime>>>>>>>>>>>>>>"<<cur_dt.toString("yyyyMMddhhmmss");
        return cur_dt.toString("yyyyMMddhhmmss");
}

QString receiveDataFromServer::closeDraw(int drawnumberg)
{
    IsCmdRuning=true;
    drawKeyThread->pauseThread();//暂停线程
    timecountNodataflag=0;
    QString inforr;
    QString statusflag;
    port.WriteMyCom(guncmd.close_command(drawnumberg));
    QString drawstr=QString::number(drawnumberg,16);
    if(drawstr.size()==1)
    {
        drawstr="0"+drawstr;
    }
    int timeoutflag=0;
    while(true)
    {
        timeoutflag++;
        inforr=port.ReadMyCom(200);
         qDebug()<<"close draw:"<< inforr;
        if(inforr.contains("b5"+drawstr))
        {
        inforr=inforr+port.ReadMyCom(200);
        inforr=inforr.mid(inforr.indexOf("b5"+drawstr),18); //一次读取发送和返回数据
        timeoutflag=0;
        statusflag=inforr.mid(14,2);
        if(statusflag=="04")
        {
         port.WriteMyCom(guncmd.close_command(drawnumberg));
        }
        else
        {
          timeoutflag=0;
          break;
        }
        }
        if(timeoutflag>30)
        {
        timeoutflag=0;
        break;
        }
    }
    return statusflag;
}
