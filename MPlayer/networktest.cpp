#include "networktest.h"
#include<QTimer>
#include<QProcess>
networkTest::networkTest(QWidget *parent) :
    QDialog(parent)
{
     port.openMycom(2,2);
     networktype=0;//2表示2G，3表示3G，4表示4G
     NetworksignalsSize=-1;
}

networkTest::~networkTest()
{

}


 bool networkTest::initNetwork()
 {

    QString test="AT+CMEE=2";
    port.WriteSmsCom(test);
    test=port.ReadSmsCom(500);
    qDebug()<<"ok:"<<test;
    int one=stepone();
    qDebug()<<"one:"<<one;
    if(one==1)//跳到5，6，7
    {
        qDebug()<<"five:"<<stepfive();
        qDebug()<<"six:"<<stepsix();
        qDebug()<<"seven:"<<stepseven();
        qDebug()<<"get dns:"<<getDns();
        qDebug()<<"get addr:"<<getAddr();
    }
    else if(one==0)//按顺序全部执行
    {
        qDebug()<<"two:"<<steptwo();
        qDebug()<<"three:"<<stepthree();
        qDebug()<<"four:"<<stepfour();
        qDebug()<<"five:"<<stepfive();
        qDebug()<<"six:"<<stepsix();
        qDebug()<<"seven:"<<stepseven();
        qDebug()<<"get dns:"<<getDns();
        qDebug()<<"get addr:"<<getAddr();
    }
    else
    {
    return false;
    }

    return true;
 }

bool networkTest::getAddr()
{
    port.WriteSmsCom(cmd.cmd_cgpaddr);
    QString testinfor=port.ReadSmsCom(500);
    testinfor=testinfor.simplified();
    int trytime=0;
    while(true)
    {
    trytime++;
    if(testinfor.contains("OK"))
    {
       break;
    }
    else if(testinfor.contains("ABORTED"))
    {
     port.WriteSmsCom(cmd.cmd_cgpaddr);
     testinfor=port.ReadSmsCom(500);
     testinfor.simplified();
    }
    else
    {
        testinfor=port.ReadSmsCom(200);
         testinfor.simplified();
        if(trytime>6)
        {
         break;
        }
    }
   }//end while
    if(testinfor.contains("OK"))
    {
        QStringList list=testinfor.split(",");
        QString ipstr;
        if(list.size()>=2)
        {     
            ipstr=list.at(1);
            ipstr=ipstr.mid(0,ipstr.lastIndexOf("\""));
            ipstr=ipstr.mid(ipstr.indexOf("\"")+1,ipstr.size());
            if(ip!=ipstr)
            {
                if(ipstr!="0.0.0.0")
                {
                ip=ipstr;
                getDns();
                }
            }
            else
            {
            }
            qDebug()<<"ip :"<<ipstr;
            if(ipstr!="0.0.0.0")
            return true;
            else
                return false;
        }
        else
        {
         return false;
        }

    }
    else
    {
       return false;
    }

}
bool networkTest::getDns()
{
    port.WriteSmsCom(cmd.cmd_xdns_n);
    QString testinfor=port.ReadSmsCom(500);
    int trytime=0;
    testinfor=testinfor.simplified();
    while(true)
    {
    trytime++;
    if(testinfor.contains("OK"))
    {
       break;
    }
    else if(testinfor.contains("ABORTED"))
    {
     port.WriteSmsCom(cmd.cmd_xdns_n);
     testinfor=port.ReadSmsCom(500);
     testinfor.simplified();
    }
    else
    {
        testinfor=port.ReadSmsCom(200);
         testinfor.simplified();
        if(trytime>6)
        {
         break;
        }
    }
   }//end while
    if(testinfor.contains("OK"))
    {
        QStringList list = testinfor.split(",");
        if(list.size()>=3)
        {
            QString dns1,dns2;
           dns1=list.at(1);
           dns2=list.at(2);

           dns1=dns1.mid(0,dns1.lastIndexOf("\""));
           dns1=dns1.mid(dns1.indexOf("\"")+1,dns1.size());

           dns2=dns2.mid(0,dns2.lastIndexOf("\""));
           dns2=dns2.mid(dns2.indexOf("\"")+1,dns2.size());

           qDebug()<<"dns1:"<<dns1;
           qDebug()<<"dns2:"<<dns2;

           QString dns = "echo \"nameserver "+dns1+"\" > /etc/resolv.conf";
           system(dns.toAscii());
           QProcess::execute(dns);

           dns = "echo \"nameserver "+dns2+"\" >> /etc/resolv.conf";
           system(dns.toAscii());
           QProcess::execute(dns);


           system("cat /etc/resolv.conf");
           QString ipcmd = "ifconfig usb0 "+ip+" netmask 255.255.255.255 -arp";
           system(ipcmd.toAscii());
           ipcmd ="ip r add "+ip+" dev usb0\n";
           system(ipcmd.toAscii());
           ipcmd ="ip r add 0.0.0.0/0 via "+ip+" dev usb0\n";
           system(ipcmd.toAscii());
        return true;
        }
        else
        {
        return false;
        }


    }
    else
    {
       return false;
    }

}
 int networkTest::stepone()//第一步
 {
    qDebug()<<"1,send at cmd :"<<cmd.cmd_cgdcont;
     port.WriteSmsCom(cmd.cmd_cgdcont);
     QString testinfor=port.ReadSmsCom(500);
     testinfor=testinfor.simplified();
     qDebug()<<"testfinor:"<<testinfor;
     int trytime=0;
     while(true)
     {
         trytime++;
        // qDebug()<<"try time:"<<trytime;
     if(testinfor.contains("OK"))
     {
        break;
     }
     else if(testinfor.contains("ABORTED"))
     {
         port.WriteSmsCom(cmd.cmd_cgdcont);
         testinfor=port.ReadSmsCom(500);
         testinfor=testinfor.simplified();
     }
     else
     {
         testinfor=port.ReadSmsCom(500);
         testinfor=testinfor.simplified();
         if(trytime>6)
         {
         break;
         }
     }
     }

     if(testinfor.contains("OK"))
     {
        QStringList list=testinfor.split(",");
        if(list.size()>2)
        {
        if(!list.at(2).isEmpty())
        {
        return 1;
        }
        else
        {
        return 0;
        }
        }
        else
            return 0;
     }
     else
         return -1;


 }
 bool networkTest::steptwo()
 {
     qDebug()<<"2,send at cmd :"<<cmd.cmd_cgact;
     port.WriteSmsCom(cmd.cmd_cgact);
     QString testinfor=port.ReadSmsCom(500);
     qDebug()<<"cgact result:"<<testinfor.simplified();
     int trytime=0;
     while(true)
     {
     trytime++;
     if(testinfor.contains("OK"))
     {
        break;
     }
     else if(testinfor.contains("ABORTED"))
     {
      port.WriteSmsCom(cmd.cmd_cgact);
      testinfor=port.ReadSmsCom(500);
      testinfor.simplified();
     }
     else
     {
         testinfor=port.ReadSmsCom(200);
          testinfor.simplified();
         if(trytime>6)
         {
          break;
         }
     }
    }//end while
     if(testinfor.contains("OK"))
     {
        return true;
     }
     else
     {
        return false;
     }
 }
 bool networkTest::stepthree()
 {
     qDebug()<<"3,send at cmd :"<<cmd.cmd_cgdcont_2;
     port.WriteSmsCom(cmd.cmd_cgdcont_2);
     QString testinfor=port.ReadSmsCom(500);
     qDebug()<<"cgdcont_2 result:"<<testinfor.simplified();

     int trytime=0;
     while(true)
     {
     trytime++;
     if(testinfor.contains("OK"))
     {
        break;
     }
     else if(testinfor.contains("ABORTED"))
     {
      port.WriteSmsCom(cmd.cmd_cgdcont_2);
      testinfor=port.ReadSmsCom(500);
      testinfor.simplified();
     }
     else
     {
         testinfor=port.ReadSmsCom(200);
          testinfor.simplified();
         if(trytime>6)
         {
          break;
         }
     }
    }//end while
     if(testinfor.contains("OK"))
     {
        return true;
     }
     else
     {
        return false;
     }

 }
 bool networkTest::stepfour()
 {
     qDebug()<<"4,send at cmd :"<<cmd.cmd_cgact_1;
     port.WriteSmsCom(cmd.cmd_cgact_1);
     QString  testinfor=port.ReadSmsCom(3000);
     qDebug()<<"cmd_cgact_1 result:"<<testinfor.simplified();


     int trytime=0;
     while(true)
     {
     trytime++;
     if(testinfor.contains("OK"))
     {
        break;
     }
     else if(testinfor.contains("ABORTED"))
     {
      port.WriteSmsCom(cmd.cmd_cgact_1);
      testinfor=port.ReadSmsCom(500);
      testinfor.simplified();
     }
     else
     {
         testinfor=port.ReadSmsCom(500);
          testinfor.simplified();
         if(trytime>8)
         {
          break;
         }
     }
    }//end while
     if(testinfor.contains("OK"))
     {
        return true;
     }
     else
     {
        return false;
     }

 }
 bool networkTest::stepfive()
 {
     qDebug()<<"5,send at cmd :"<<cmd.cmd_xdns;
     port.WriteSmsCom(cmd.cmd_xdns);
     QString  testinfor=port.ReadSmsCom(500);
     qDebug()<<"cmd_xdns result:"<<testinfor.simplified();


     int trytime=0;
     while(true)
     {
     trytime++;
     if(testinfor.contains("OK"))
     {
        break;
     }
     else if(testinfor.contains("ABORTED"))
     {
      port.WriteSmsCom(cmd.cmd_xdns);
      testinfor=port.ReadSmsCom(500);
      testinfor.simplified();
     }
     else
     {
         testinfor=port.ReadSmsCom(500);
         testinfor.simplified();
         if(trytime>8)
         {
          break;
         }
     }
    }//end while
     if(testinfor.contains("OK"))
     {
        return true;
     }
     else
     {
        return false;
     }
 }
 bool networkTest::stepsix()
 {
     qDebug()<<"6,send at cmd :"<<cmd.cmd_xdatachannel;
     port.WriteSmsCom(cmd.cmd_xdatachannel);
    QString   testinfor=port.ReadSmsCom(1000);
     qDebug()<<"xdatachannel result:"<<testinfor.simplified();

     int trytime=0;
     while(true)
     {
     trytime++;
     if(testinfor.contains("OK"))
     {
        break;
     }
     else if(testinfor.contains("ABORTED"))
     {
      port.WriteSmsCom(cmd.cmd_xdatachannel);
      testinfor=port.ReadSmsCom(500);
      testinfor.simplified();
     }
     else
     {
         testinfor=port.ReadSmsCom(500);
         testinfor.simplified();
         if(trytime>4)
         {
          break;
         }
     }
    }//end while
     if(testinfor.contains("OK"))
     {
        return true;
     }
     else
     {
        return false;
     }
 }
 bool networkTest::stepseven()
 {
     qDebug()<<"7,send at cmd :"<<cmd.cmd_cgdata;
     port.WriteSmsCom(cmd.cmd_cgdata);
     QString  testinfor=port.ReadSmsCom(500);
     qDebug()<<"cmd_cgdata result:"<<testinfor.simplified();

     int trytime=0;
     while(true)
     {
     trytime++;
     if(testinfor.contains("OK"))
     {
        break;
     }
     else if(testinfor.contains("ABORTED"))
     {
      port.WriteSmsCom(cmd.cmd_cgdata);
      testinfor=port.ReadSmsCom(500);
      testinfor.simplified();
     }
     else
     {
         testinfor=port.ReadSmsCom(500);
         testinfor.simplified();
         if(trytime>4)
         {
          break;
         }
     }
    }//end while
     if(testinfor.contains("OK"))
     {
        return true;
     }
     else
     {
        return false;
     }
 }


int networkTest::netSignal()
{
   getnetworkSignalSize();
   return NetworksignalsSize;
}


void networkTest::getnetworkSignalSize()
{
    port.WriteSmsCom(cmd.cmd_cops);
    QString testinfor=port.ReadSmsCom(500);
    int trytime=0;
    while(true)
    {
        trytime++;
    if(testinfor.contains("OK"))
    {
        break;
    }
    else if(testinfor.contains("ABORTED"))
    {
        port.WriteSmsCom(cmd.cmd_cops);
        testinfor=port.ReadSmsCom(500);
    }
    else
    {
        testinfor=port.ReadSmsCom(500);
        if(trytime>5)
        {
            break;
        }
    }
    }
    testinfor.simplified();
    if(testinfor.contains("OK"))
    {
        qDebug()<<"testinfor:"<<testinfor;
        QStringList list=testinfor.split(",");
        if(list.size()<4)//没有网络数据
        {

        }
        else
        {
            QString test=list.at(3);
            test=test.mid(0,1);
            networktype=test.toInt();
            qDebug()<<"test:"<<test;
            if((networktype==7)||(networktype==2)||(networktype==0))
            {
            getSinal();//获取网络信号强度
            }
            else
            {
            NetworksignalsSize=-1;
            }
        }

    }
    else
    {
    NetworksignalsSize=-1;
    }
}
void networkTest::getSinal()
{
    port.WriteSmsCom(cmd.cmd_xcesq);
    QString testinfor=port.ReadSmsCom(500);
    int trytime=0;
    while(true)
    {
        trytime++;
    if(testinfor.contains("OK"))
    {
        break;
    }
    else if(testinfor.contains("ABORTED"))
    {
        port.WriteSmsCom(cmd.cmd_xcesq);
        testinfor=port.ReadSmsCom(500);
    }
    else
    {
        testinfor=port.ReadSmsCom(500);
        if(trytime>5)
        {
            break;
        }
    }
    }
    testinfor.simplified();
    qDebug()<<"signal size:"<<testinfor;
    if(testinfor.contains("OK"))
    {
        QStringList list=testinfor.split(",");
        int net2=0,net3=0,net4=0;

        if(list.size()<7)//没有正确数据
        {

        }
        else
        {
            net2=list.at(1).toInt();//2G
            net3=list.at(3).toInt();//3G
            net4=list.at(6).toInt();//4G
//            qDebug()<<"2g:"<<net2;
//            qDebug()<<"3g:"<<net3;
//            qDebug()<<"4g:"<<net4;
            int sigalVisualsize=0;
            switch(networktype)
            {
            case 0:
                sigalVisualsize=net2-110;
                break;
            case 2:
                sigalVisualsize=net3-120;
                break;
            case 7:
                sigalVisualsize=net4-140;
                break;
            default:
                NetworksignalsSize=-1;
                break;
            }
            if(sigalVisualsize<-125)
           {
               NetworksignalsSize=0;
           }

           else if((sigalVisualsize>=-125)&&(sigalVisualsize<-115))
           {
               NetworksignalsSize=1;
           }
           else if((sigalVisualsize>=-115)&&(sigalVisualsize<-105))
           {
               NetworksignalsSize=2;
           }
           else if((sigalVisualsize>=-105)&&(sigalVisualsize<-95))
           {
               NetworksignalsSize=3;
           }
           else if((sigalVisualsize>=-95)&&(sigalVisualsize<-85))
           {
               NetworksignalsSize=4;
           }
           else if(sigalVisualsize>=-85 )
           {
               NetworksignalsSize=5;
           }
           else
           {
            NetworksignalsSize=-1;
           }
            qDebug()<<"network signal quantity:"<<NetworksignalsSize;
        }
    }
    else
    {
    NetworksignalsSize=-1;
    }
}

bool networkTest::getNetworkStatus()//读取网络状态
{
   return getAddr();
}
bool networkTest::rebootDev()
{
    if(port.comisopen())
    {

    }
    else
    {
       // port.openMycom(2,2);
        if(!port.comisopen())
        {
        port.openMycom();
        }
    }
    port.WriteSmsCom(cmd.cmd_reboot);
    QString testinfor=port.ReadSmsCom(500);
    qDebug()<<"reboot:"<<testinfor;
    if(testinfor.contains("OK"))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void networkTest::updateDev()
{
    // port.closeMycom();
     port.openMycom(2,2);
     if(!port.comisopen())
     {
      port.openMycom();
     }
}
