#ifndef CHANGEKEY_H
#define CHANGEKEY_H
#include"newqserialport.h"
#include <QDialog>
#include"gunconman.h"

namespace Ui {
    class changeKey;
}

class changeKey : public QDialog
{
    Q_OBJECT

public:
    explicit changeKey(QWidget *parent = 0);
    ~changeKey();

private:
    Ui::changeKey *ui;
    newqserialport serial;
    gunconman comman;
    int lockNumber;//抽屉号


private slots:
    void getLockNo(int);
    void on_cancelBtn_clicked();
    void openCabinet(int);//打开抽屉
    QString closeCabinet(int);//关闭抽屉
    QString getinfor(QString);
    void on_okBtn_clicked();
    QString readCardId(int);
    void on_closeBtn_clicked();

signals:

    void sendManagerOp(QString,QString,QString);//管理员操作记录
};

#endif // CHANGEKEY_H
