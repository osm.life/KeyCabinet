/********************************************************************************
** Form generated from reading UI file 'menu.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MENU_H
#define UI_MENU_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDateTimeEdit>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_menu
{
public:
    QListWidget *listWidget;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QWidget *page_2;
    QWidget *page_3;
    QWidget *page_4;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QLabel *timeLabel;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QDateTimeEdit *dateTimeEdit;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QPushButton *timeOk_btn;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;
    QWidget *page_5;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_4;
    QLabel *tipCabinetNo;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_3;
    QComboBox *cabinetNo_1;
    QComboBox *cabinetNo_2;
    QComboBox *cabinetNo_3;
    QComboBox *cabinetNo_4;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *cabinetNo_btn;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_3;
    QWidget *page_6;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *VoiceBtn;
    QSpacerItem *horizontalSpacer_8;
    QWidget *page_7;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_6;
    QLabel *tipIpinfor;
    QSpacerItem *verticalSpacer_22;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_38;
    QLabel *label_4;
    QLineEdit *ipEdit;
    QSpacerItem *horizontalSpacer_16;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_39;
    QLabel *label_5;
    QLineEdit *serverPort;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *verticalSpacer_21;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *serverIpOk;
    QSpacerItem *horizontalSpacer_10;
    QSpacerItem *verticalSpacer_7;
    QWidget *page_8;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer_18;
    QGridLayout *gridLayout;
    QLabel *tipPhone;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_6;
    QLineEdit *phone;
    QPushButton *phone_btn;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *verticalSpacer_19;
    QWidget *page_9;
    QVBoxLayout *verticalLayout_6;
    QSpacerItem *verticalSpacer_8;
    QHBoxLayout *horizontalLayout_14;
    QSpacerItem *horizontalSpacer_18;
    QLabel *tipuserpassword;
    QSpacerItem *horizontalSpacer_19;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_17;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_14;
    QLabel *label_9;
    QLineEdit *newcode2;
    QSpacerItem *horizontalSpacer_15;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_20;
    QPushButton *newcode_btn;
    QSpacerItem *horizontalSpacer_21;
    QSpacerItem *verticalSpacer_9;
    QWidget *page_10;
    QVBoxLayout *verticalLayout_7;
    QSpacerItem *verticalSpacer_10;
    QHBoxLayout *horizontalLayout_16;
    QSpacerItem *horizontalSpacer_22;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_23;
    QLabel *tipCodeTime;
    QHBoxLayout *horizontalLayout_17;
    QSpacerItem *horizontalSpacer_24;
    QComboBox *time_code;
    QPushButton *timecode_btn;
    QSpacerItem *horizontalSpacer_25;
    QSpacerItem *verticalSpacer_11;
    QWidget *page_11;
    QVBoxLayout *verticalLayout_8;
    QSpacerItem *verticalSpacer_12;
    QHBoxLayout *horizontalLayout_19;
    QSpacerItem *horizontalSpacer_28;
    QLabel *label_11;
    QSpacerItem *horizontalSpacer_29;
    QLabel *tipAntuclaseTime;
    QHBoxLayout *horizontalLayout_18;
    QSpacerItem *horizontalSpacer_26;
    QComboBox *cabinet_autoClose;
    QPushButton *CabinetAutoTime_btn;
    QSpacerItem *horizontalSpacer_27;
    QSpacerItem *verticalSpacer_13;
    QWidget *page_12;
    QVBoxLayout *verticalLayout_9;
    QSpacerItem *verticalSpacer_14;
    QLabel *tipsystem;
    QSpacerItem *verticalSpacer_20;
    QHBoxLayout *horizontalLayout_21;
    QSpacerItem *horizontalSpacer_32;
    QLabel *label_12;
    QSpacerItem *horizontalSpacer_33;
    QLabel *tipNetworkMode;
    QHBoxLayout *horizontalLayout_20;
    QSpacerItem *horizontalSpacer_30;
    QComboBox *networkMode;
    QPushButton *networdMode_btn;
    QSpacerItem *horizontalSpacer_31;
    QSpacerItem *verticalSpacer_15;
    QWidget *page_16;
    QFrame *line;
    QFrame *line_2;
    QGroupBox *groupBox;
    QPushButton *picSettingBtn;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_8;
    QLineEdit *pictureTimeEdit;
    QLabel *label_7;
    QPushButton *pictureSureBtn;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_26;
    QPushButton *shutdown;
    QSpacerItem *horizontalSpacer_40;
    QPushButton *reboot;
    QWidget *layoutWidget2;
    QGridLayout *gridLayout_2;
    QPushButton *initBtn;
    QPushButton *warnBtn;
    QPushButton *updatavoice;
    QPushButton *clearlogBtn;
    QPushButton *copyLogToBtn;
    QWidget *page_13;
    QVBoxLayout *verticalLayout_10;
    QSpacerItem *verticalSpacer_16;
    QHBoxLayout *horizontalLayout_23;
    QSpacerItem *horizontalSpacer_34;
    QLabel *label_13;
    QSpacerItem *horizontalSpacer_35;
    QLabel *tipDrawNum;
    QHBoxLayout *horizontalLayout_22;
    QSpacerItem *horizontalSpacer_36;
    QComboBox *CabinetNumber;
    QPushButton *CabinetNumber_btn;
    QSpacerItem *horizontalSpacer_37;
    QSpacerItem *verticalSpacer_17;
    QWidget *page_14;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_24;
    QPushButton *writelockNo;
    QPushButton *writeBoardNo;
    QHBoxLayout *horizontalLayout_25;
    QTableWidget *table__lock;
    QTableWidget *table_Board;
    QWidget *page_15;

    void setupUi(QDialog *menu)
    {
        if (menu->objectName().isEmpty())
            menu->setObjectName(QString::fromUtf8("menu"));
        menu->resize(1024, 600);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(menu->sizePolicy().hasHeightForWidth());
        menu->setSizePolicy(sizePolicy);
        menu->setMinimumSize(QSize(0, 0));
        menu->setMaximumSize(QSize(1024, 600));
        QFont font;
        font.setPointSize(13);
        menu->setFont(font);
        listWidget = new QListWidget(menu);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(9, 9, 200, 582));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(listWidget->sizePolicy().hasHeightForWidth());
        listWidget->setSizePolicy(sizePolicy1);
        listWidget->setMinimumSize(QSize(200, 0));
        listWidget->setMaximumSize(QSize(200, 16777215));
        QFont font1;
        font1.setPointSize(12);
        listWidget->setFont(font1);
        listWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        frame = new QFrame(menu);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(220, 10, 791, 581));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        stackedWidget = new QStackedWidget(frame);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setMinimumSize(QSize(0, 0));
        QFont font2;
        font2.setPointSize(16);
        stackedWidget->setFont(font2);
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        stackedWidget->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        stackedWidget->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        verticalLayout = new QVBoxLayout(page_4);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 67, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        timeLabel = new QLabel(page_4);
        timeLabel->setObjectName(QString::fromUtf8("timeLabel"));
        timeLabel->setLineWidth(0);
        timeLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(timeLabel);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(page_4);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(label);

        dateTimeEdit = new QDateTimeEdit(page_4);
        dateTimeEdit->setObjectName(QString::fromUtf8("dateTimeEdit"));
        dateTimeEdit->setFocusPolicy(Qt::ClickFocus);
        dateTimeEdit->setFrame(true);
        dateTimeEdit->setAlignment(Qt::AlignCenter);
        dateTimeEdit->setButtonSymbols(QAbstractSpinBox::PlusMinus);
        dateTimeEdit->setAccelerated(true);
        dateTimeEdit->setDateTime(QDateTime(QDate(2016, 10, 11), QTime(0, 9, 5)));
        dateTimeEdit->setDate(QDate(2016, 10, 11));
        dateTimeEdit->setTime(QTime(0, 9, 5));
        dateTimeEdit->setCurrentSection(QDateTimeEdit::YearSection);
        dateTimeEdit->setCalendarPopup(true);

        horizontalLayout_3->addWidget(dateTimeEdit);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        timeOk_btn = new QPushButton(page_4);
        timeOk_btn->setObjectName(QString::fromUtf8("timeOk_btn"));
        timeOk_btn->setMinimumSize(QSize(200, 0));
        timeOk_btn->setMaximumSize(QSize(360, 16777215));

        horizontalLayout_4->addWidget(timeOk_btn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_4);

        verticalSpacer = new QSpacerItem(20, 67, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        stackedWidget->addWidget(page_4);
        page_5 = new QWidget();
        page_5->setObjectName(QString::fromUtf8("page_5"));
        verticalLayout_2 = new QVBoxLayout(page_5);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer_4 = new QSpacerItem(20, 145, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_4);

        tipCabinetNo = new QLabel(page_5);
        tipCabinetNo->setObjectName(QString::fromUtf8("tipCabinetNo"));
        tipCabinetNo->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(tipCabinetNo);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        label_3 = new QLabel(page_5);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_5->addWidget(label_3);

        cabinetNo_1 = new QComboBox(page_5);
        cabinetNo_1->setObjectName(QString::fromUtf8("cabinetNo_1"));

        horizontalLayout_5->addWidget(cabinetNo_1);

        cabinetNo_2 = new QComboBox(page_5);
        cabinetNo_2->setObjectName(QString::fromUtf8("cabinetNo_2"));

        horizontalLayout_5->addWidget(cabinetNo_2);

        cabinetNo_3 = new QComboBox(page_5);
        cabinetNo_3->setObjectName(QString::fromUtf8("cabinetNo_3"));

        horizontalLayout_5->addWidget(cabinetNo_3);

        cabinetNo_4 = new QComboBox(page_5);
        cabinetNo_4->setObjectName(QString::fromUtf8("cabinetNo_4"));

        horizontalLayout_5->addWidget(cabinetNo_4);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_5);

        verticalSpacer_5 = new QSpacerItem(20, 78, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_3);

        cabinetNo_btn = new QPushButton(page_5);
        cabinetNo_btn->setObjectName(QString::fromUtf8("cabinetNo_btn"));
        cabinetNo_btn->setMinimumSize(QSize(200, 0));
        cabinetNo_btn->setMaximumSize(QSize(360, 16777215));

        horizontalLayout_6->addWidget(cabinetNo_btn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout_6);

        verticalSpacer_3 = new QSpacerItem(20, 148, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);

        stackedWidget->addWidget(page_5);
        page_6 = new QWidget();
        page_6->setObjectName(QString::fromUtf8("page_6"));
        verticalLayout_3 = new QVBoxLayout(page_6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);

        VoiceBtn = new QPushButton(page_6);
        VoiceBtn->setObjectName(QString::fromUtf8("VoiceBtn"));
        VoiceBtn->setMinimumSize(QSize(150, 50));

        horizontalLayout_7->addWidget(VoiceBtn);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_8);


        verticalLayout_3->addLayout(horizontalLayout_7);

        stackedWidget->addWidget(page_6);
        page_7 = new QWidget();
        page_7->setObjectName(QString::fromUtf8("page_7"));
        verticalLayout_4 = new QVBoxLayout(page_7);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalSpacer_6 = new QSpacerItem(20, 207, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_6);

        tipIpinfor = new QLabel(page_7);
        tipIpinfor->setObjectName(QString::fromUtf8("tipIpinfor"));
        tipIpinfor->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(tipIpinfor);

        verticalSpacer_22 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_4->addItem(verticalSpacer_22);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_38 = new QSpacerItem(120, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_38);

        label_4 = new QLabel(page_7);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(150, 0));
        label_4->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_8->addWidget(label_4);

        ipEdit = new QLineEdit(page_7);
        ipEdit->setObjectName(QString::fromUtf8("ipEdit"));
        ipEdit->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(ipEdit);

        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_16);


        verticalLayout_4->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_39 = new QSpacerItem(120, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_39);

        label_5 = new QLabel(page_7);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(150, 0));
        label_5->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_9->addWidget(label_5);

        serverPort = new QLineEdit(page_7);
        serverPort->setObjectName(QString::fromUtf8("serverPort"));
        serverPort->setMinimumSize(QSize(141, 0));
        serverPort->setMaxLength(6);
        serverPort->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(serverPort);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_12);


        verticalLayout_4->addLayout(horizontalLayout_9);

        verticalSpacer_21 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_4->addItem(verticalSpacer_21);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_9);

        serverIpOk = new QPushButton(page_7);
        serverIpOk->setObjectName(QString::fromUtf8("serverIpOk"));
        serverIpOk->setMinimumSize(QSize(120, 45));
        serverIpOk->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_10->addWidget(serverIpOk);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_10);


        verticalLayout_4->addLayout(horizontalLayout_10);

        verticalSpacer_7 = new QSpacerItem(20, 206, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_7);

        stackedWidget->addWidget(page_7);
        page_8 = new QWidget();
        page_8->setObjectName(QString::fromUtf8("page_8"));
        verticalLayout_5 = new QVBoxLayout(page_8);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalSpacer_18 = new QSpacerItem(20, 226, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_18);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tipPhone = new QLabel(page_8);
        tipPhone->setObjectName(QString::fromUtf8("tipPhone"));
        tipPhone->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(tipPhone, 0, 0, 1, 5);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_13, 1, 0, 1, 1);

        label_6 = new QLabel(page_8);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 1, 1, 1, 1);

        phone = new QLineEdit(page_8);
        phone->setObjectName(QString::fromUtf8("phone"));
        phone->setMaximumSize(QSize(300, 16777215));

        gridLayout->addWidget(phone, 1, 2, 1, 1);

        phone_btn = new QPushButton(page_8);
        phone_btn->setObjectName(QString::fromUtf8("phone_btn"));

        gridLayout->addWidget(phone_btn, 1, 3, 1, 1);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_11, 1, 4, 1, 1);


        verticalLayout_5->addLayout(gridLayout);

        verticalSpacer_19 = new QSpacerItem(20, 226, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_19);

        stackedWidget->addWidget(page_8);
        page_9 = new QWidget();
        page_9->setObjectName(QString::fromUtf8("page_9"));
        verticalLayout_6 = new QVBoxLayout(page_9);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalSpacer_8 = new QSpacerItem(20, 199, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_8);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_18);

        tipuserpassword = new QLabel(page_9);
        tipuserpassword->setObjectName(QString::fromUtf8("tipuserpassword"));

        horizontalLayout_14->addWidget(tipuserpassword);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_19);


        verticalLayout_6->addLayout(horizontalLayout_14);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_17);


        verticalLayout_6->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_14);

        label_9 = new QLabel(page_9);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_13->addWidget(label_9);

        newcode2 = new QLineEdit(page_9);
        newcode2->setObjectName(QString::fromUtf8("newcode2"));
        newcode2->setEchoMode(QLineEdit::Password);

        horizontalLayout_13->addWidget(newcode2);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_15);


        verticalLayout_6->addLayout(horizontalLayout_13);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalSpacer_20 = new QSpacerItem(88, 28, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_20);

        newcode_btn = new QPushButton(page_9);
        newcode_btn->setObjectName(QString::fromUtf8("newcode_btn"));

        horizontalLayout_15->addWidget(newcode_btn);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_21);


        verticalLayout_6->addLayout(horizontalLayout_15);

        verticalSpacer_9 = new QSpacerItem(20, 198, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_9);

        stackedWidget->addWidget(page_9);
        page_10 = new QWidget();
        page_10->setObjectName(QString::fromUtf8("page_10"));
        verticalLayout_7 = new QVBoxLayout(page_10);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalSpacer_10 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_10);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_22);

        label_10 = new QLabel(page_10);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setAlignment(Qt::AlignCenter);

        horizontalLayout_16->addWidget(label_10);

        horizontalSpacer_23 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_23);


        verticalLayout_7->addLayout(horizontalLayout_16);

        tipCodeTime = new QLabel(page_10);
        tipCodeTime->setObjectName(QString::fromUtf8("tipCodeTime"));
        tipCodeTime->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(tipCodeTime);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        horizontalSpacer_24 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_24);

        time_code = new QComboBox(page_10);
        time_code->setObjectName(QString::fromUtf8("time_code"));

        horizontalLayout_17->addWidget(time_code);

        timecode_btn = new QPushButton(page_10);
        timecode_btn->setObjectName(QString::fromUtf8("timecode_btn"));

        horizontalLayout_17->addWidget(timecode_btn);

        horizontalSpacer_25 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_25);


        verticalLayout_7->addLayout(horizontalLayout_17);

        verticalSpacer_11 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_11);

        stackedWidget->addWidget(page_10);
        page_11 = new QWidget();
        page_11->setObjectName(QString::fromUtf8("page_11"));
        verticalLayout_8 = new QVBoxLayout(page_11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalSpacer_12 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_8->addItem(verticalSpacer_12);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        horizontalSpacer_28 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_28);

        label_11 = new QLabel(page_11);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_19->addWidget(label_11);

        horizontalSpacer_29 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_29);


        verticalLayout_8->addLayout(horizontalLayout_19);

        tipAntuclaseTime = new QLabel(page_11);
        tipAntuclaseTime->setObjectName(QString::fromUtf8("tipAntuclaseTime"));
        tipAntuclaseTime->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(tipAntuclaseTime);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        horizontalSpacer_26 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_26);

        cabinet_autoClose = new QComboBox(page_11);
        cabinet_autoClose->setObjectName(QString::fromUtf8("cabinet_autoClose"));

        horizontalLayout_18->addWidget(cabinet_autoClose);

        CabinetAutoTime_btn = new QPushButton(page_11);
        CabinetAutoTime_btn->setObjectName(QString::fromUtf8("CabinetAutoTime_btn"));

        horizontalLayout_18->addWidget(CabinetAutoTime_btn);

        horizontalSpacer_27 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_27);


        verticalLayout_8->addLayout(horizontalLayout_18);

        verticalSpacer_13 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_8->addItem(verticalSpacer_13);

        stackedWidget->addWidget(page_11);
        page_12 = new QWidget();
        page_12->setObjectName(QString::fromUtf8("page_12"));
        verticalLayout_9 = new QVBoxLayout(page_12);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalSpacer_14 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_14);

        tipsystem = new QLabel(page_12);
        tipsystem->setObjectName(QString::fromUtf8("tipsystem"));
        tipsystem->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(tipsystem);

        verticalSpacer_20 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_20);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        horizontalSpacer_32 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_32);

        label_12 = new QLabel(page_12);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setAlignment(Qt::AlignCenter);

        horizontalLayout_21->addWidget(label_12);

        horizontalSpacer_33 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_33);


        verticalLayout_9->addLayout(horizontalLayout_21);

        tipNetworkMode = new QLabel(page_12);
        tipNetworkMode->setObjectName(QString::fromUtf8("tipNetworkMode"));
        tipNetworkMode->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(tipNetworkMode);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_30);

        networkMode = new QComboBox(page_12);
        networkMode->setObjectName(QString::fromUtf8("networkMode"));
        networkMode->setMinimumSize(QSize(200, 0));
        networkMode->setMaximumSize(QSize(300, 16777215));

        horizontalLayout_20->addWidget(networkMode);

        networdMode_btn = new QPushButton(page_12);
        networdMode_btn->setObjectName(QString::fromUtf8("networdMode_btn"));

        horizontalLayout_20->addWidget(networdMode_btn);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_31);


        verticalLayout_9->addLayout(horizontalLayout_20);

        verticalSpacer_15 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_15);

        stackedWidget->addWidget(page_12);
        page_16 = new QWidget();
        page_16->setObjectName(QString::fromUtf8("page_16"));
        line = new QFrame(page_16);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(10, 450, 761, 16));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(page_16);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(20, 260, 751, 41));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        groupBox = new QGroupBox(page_16);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(30, 30, 701, 221));
        picSettingBtn = new QPushButton(groupBox);
        picSettingBtn->setObjectName(QString::fromUtf8("picSettingBtn"));
        picSettingBtn->setGeometry(QRect(200, 50, 361, 41));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(130, 130, 451, 39));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(186, 0));
        label_8->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_8);

        pictureTimeEdit = new QLineEdit(layoutWidget);
        pictureTimeEdit->setObjectName(QString::fromUtf8("pictureTimeEdit"));
        pictureTimeEdit->setMinimumSize(QSize(50, 0));

        horizontalLayout_2->addWidget(pictureTimeEdit);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_2->addWidget(label_7);

        pictureSureBtn = new QPushButton(layoutWidget);
        pictureSureBtn->setObjectName(QString::fromUtf8("pictureSureBtn"));

        horizontalLayout_2->addWidget(pictureSureBtn);

        layoutWidget1 = new QWidget(page_16);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(40, 480, 711, 39));
        horizontalLayout_26 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        horizontalLayout_26->setContentsMargins(0, 0, 0, 0);
        shutdown = new QPushButton(layoutWidget1);
        shutdown->setObjectName(QString::fromUtf8("shutdown"));
        shutdown->setMinimumSize(QSize(200, 0));

        horizontalLayout_26->addWidget(shutdown);

        horizontalSpacer_40 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_26->addItem(horizontalSpacer_40);

        reboot = new QPushButton(layoutWidget1);
        reboot->setObjectName(QString::fromUtf8("reboot"));
        reboot->setMinimumSize(QSize(200, 0));

        horizontalLayout_26->addWidget(reboot);

        layoutWidget2 = new QWidget(page_16);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(31, 321, 614, 82));
        gridLayout_2 = new QGridLayout(layoutWidget2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        initBtn = new QPushButton(layoutWidget2);
        initBtn->setObjectName(QString::fromUtf8("initBtn"));

        gridLayout_2->addWidget(initBtn, 0, 0, 1, 1);

        warnBtn = new QPushButton(layoutWidget2);
        warnBtn->setObjectName(QString::fromUtf8("warnBtn"));

        gridLayout_2->addWidget(warnBtn, 0, 1, 1, 1);

        updatavoice = new QPushButton(layoutWidget2);
        updatavoice->setObjectName(QString::fromUtf8("updatavoice"));
        updatavoice->setMinimumSize(QSize(200, 0));

        gridLayout_2->addWidget(updatavoice, 0, 2, 1, 1);

        clearlogBtn = new QPushButton(layoutWidget2);
        clearlogBtn->setObjectName(QString::fromUtf8("clearlogBtn"));
        clearlogBtn->setMinimumSize(QSize(200, 0));

        gridLayout_2->addWidget(clearlogBtn, 1, 0, 1, 1);

        copyLogToBtn = new QPushButton(layoutWidget2);
        copyLogToBtn->setObjectName(QString::fromUtf8("copyLogToBtn"));
        copyLogToBtn->setMinimumSize(QSize(200, 0));

        gridLayout_2->addWidget(copyLogToBtn, 1, 1, 1, 1);

        stackedWidget->addWidget(page_16);
        page_13 = new QWidget();
        page_13->setObjectName(QString::fromUtf8("page_13"));
        verticalLayout_10 = new QVBoxLayout(page_13);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalSpacer_16 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_16);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        horizontalSpacer_34 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_34);

        label_13 = new QLabel(page_13);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setAlignment(Qt::AlignCenter);

        horizontalLayout_23->addWidget(label_13);

        horizontalSpacer_35 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_35);


        verticalLayout_10->addLayout(horizontalLayout_23);

        tipDrawNum = new QLabel(page_13);
        tipDrawNum->setObjectName(QString::fromUtf8("tipDrawNum"));
        tipDrawNum->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(tipDrawNum);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        horizontalSpacer_36 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_36);

        CabinetNumber = new QComboBox(page_13);
        CabinetNumber->setObjectName(QString::fromUtf8("CabinetNumber"));

        horizontalLayout_22->addWidget(CabinetNumber);

        CabinetNumber_btn = new QPushButton(page_13);
        CabinetNumber_btn->setObjectName(QString::fromUtf8("CabinetNumber_btn"));

        horizontalLayout_22->addWidget(CabinetNumber_btn);

        horizontalSpacer_37 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_37);


        verticalLayout_10->addLayout(horizontalLayout_22);

        verticalSpacer_17 = new QSpacerItem(20, 240, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_17);

        stackedWidget->addWidget(page_13);
        page_14 = new QWidget();
        page_14->setObjectName(QString::fromUtf8("page_14"));
        verticalLayout_11 = new QVBoxLayout(page_14);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        writelockNo = new QPushButton(page_14);
        writelockNo->setObjectName(QString::fromUtf8("writelockNo"));

        horizontalLayout_24->addWidget(writelockNo);

        writeBoardNo = new QPushButton(page_14);
        writeBoardNo->setObjectName(QString::fromUtf8("writeBoardNo"));

        horizontalLayout_24->addWidget(writeBoardNo);


        verticalLayout_11->addLayout(horizontalLayout_24);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        table__lock = new QTableWidget(page_14);
        table__lock->setObjectName(QString::fromUtf8("table__lock"));

        horizontalLayout_25->addWidget(table__lock);

        table_Board = new QTableWidget(page_14);
        table_Board->setObjectName(QString::fromUtf8("table_Board"));

        horizontalLayout_25->addWidget(table_Board);


        verticalLayout_11->addLayout(horizontalLayout_25);

        stackedWidget->addWidget(page_14);
        page_15 = new QWidget();
        page_15->setObjectName(QString::fromUtf8("page_15"));
        stackedWidget->addWidget(page_15);

        horizontalLayout->addWidget(stackedWidget);


        retranslateUi(menu);

        stackedWidget->setCurrentIndex(12);


        QMetaObject::connectSlotsByName(menu);
    } // setupUi

    void retranslateUi(QDialog *menu)
    {
        menu->setWindowTitle(QApplication::translate("menu", "Dialog", 0, QApplication::UnicodeUTF8));
        timeLabel->setText(QString());
        label->setText(QApplication::translate("menu", "\346\227\245\346\234\237\346\227\266\351\227\264\357\274\232", 0, QApplication::UnicodeUTF8));
        dateTimeEdit->setDisplayFormat(QApplication::translate("menu", "yyyy-MM-dd hh:mm", 0, QApplication::UnicodeUTF8));
        timeOk_btn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        tipCabinetNo->setText(QString());
        label_3->setText(QApplication::translate("menu", "\346\237\234\345\217\267\357\274\232", 0, QApplication::UnicodeUTF8));
        cabinetNo_btn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        VoiceBtn->setText(QString());
        tipIpinfor->setText(QString());
        label_4->setText(QApplication::translate("menu", "\346\234\215\345\212\241\345\231\250\345\234\260\345\235\200\357\274\232", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("menu", "\346\234\215\345\212\241\345\231\250\347\253\257\345\217\243\357\274\232", 0, QApplication::UnicodeUTF8));
        serverIpOk->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        tipPhone->setText(QString());
        label_6->setText(QApplication::translate("menu", "\346\211\213\346\234\272\345\217\267\347\240\201\357\274\232", 0, QApplication::UnicodeUTF8));
        phone_btn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        tipuserpassword->setText(QApplication::translate("menu", "\350\257\267\350\276\223\345\205\245\346\227\247\345\257\206\347\240\201", 0, QApplication::UnicodeUTF8));
        label_9->setText(QString());
        newcode_btn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("menu", "\350\257\267\351\200\211\346\213\251\346\235\241\347\240\201\347\232\204\346\234\211\346\225\210\346\234\237\357\274\210\345\215\225\344\275\215\357\274\232\345\210\206\351\222\237\357\274\211", 0, QApplication::UnicodeUTF8));
        tipCodeTime->setText(QString());
        timecode_btn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("menu", "\350\257\267\351\200\211\346\213\251\346\212\275\345\261\211\350\207\252\345\212\250\345\205\263\351\227\255\347\232\204\346\227\266\351\227\264\357\274\210\345\215\225\344\275\215\357\274\232\347\247\222\357\274\211", 0, QApplication::UnicodeUTF8));
        tipAntuclaseTime->setText(QString());
        CabinetAutoTime_btn->setText(QApplication::translate("menu", "\347\241\256\345\256\232", 0, QApplication::UnicodeUTF8));
        tipsystem->setText(QString());
        label_12->setText(QApplication::translate("menu", "\350\257\267\351\200\211\346\213\251\344\270\212\347\275\221\346\250\241\345\274\217", 0, QApplication::UnicodeUTF8));
        tipNetworkMode->setText(QString());
        networdMode_btn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("menu", "\345\271\277\345\221\212\350\256\276\347\275\256", 0, QApplication::UnicodeUTF8));
        picSettingBtn->setText(QApplication::translate("menu", "\345\233\276\347\211\207\345\271\277\345\221\212\346\222\255\346\224\276\345\274\200\345\205\263", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("menu", "\350\267\263\345\205\245\346\227\266\351\227\264", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("menu", "\357\274\210\347\247\222\357\274\211", 0, QApplication::UnicodeUTF8));
        pictureSureBtn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        shutdown->setText(QApplication::translate("menu", "\345\205\263\351\227\255\347\263\273\347\273\237", 0, QApplication::UnicodeUTF8));
        reboot->setText(QApplication::translate("menu", "\351\207\215\345\220\257\347\263\273\347\273\237", 0, QApplication::UnicodeUTF8));
        initBtn->setText(QApplication::translate("menu", "\345\210\235\345\247\213\345\214\226\350\256\260\345\277\206\345\272\223", 0, QApplication::UnicodeUTF8));
        warnBtn->setText(QApplication::translate("menu", "\350\247\243\351\231\244\346\212\245\350\255\246", 0, QApplication::UnicodeUTF8));
        updatavoice->setText(QApplication::translate("menu", "\346\233\264\346\226\260\350\257\255\351\237\263", 0, QApplication::UnicodeUTF8));
        clearlogBtn->setText(QApplication::translate("menu", "\346\270\205\347\251\272\346\227\245\345\277\227", 0, QApplication::UnicodeUTF8));
        copyLogToBtn->setText(QApplication::translate("menu", "\345\257\274\345\207\272\346\227\245\345\277\227\345\210\260u\347\233\230", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("menu", "\350\257\267\350\276\223\345\205\245\346\212\275\345\261\211\351\227\250\346\225\260\357\274\210\345\215\225\344\275\215\357\274\232\344\270\252\357\274\211", 0, QApplication::UnicodeUTF8));
        tipDrawNum->setText(QString());
        CabinetNumber_btn->setText(QApplication::translate("menu", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        writelockNo->setText(QApplication::translate("menu", "\345\206\231\346\212\275\345\261\211\351\224\201\345\217\267", 0, QApplication::UnicodeUTF8));
        writeBoardNo->setText(QApplication::translate("menu", "\345\206\231\346\204\237\345\272\224\346\235\277\351\224\201\345\217\267", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class menu: public Ui_menu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MENU_H
