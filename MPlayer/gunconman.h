#ifndef GUNCONMAN_H
#define GUNCONMAN_H
#include<QString>
class gunconman
{
public:
    gunconman();
    char * open_command(int number);//存枪命令
    char * close_command(int number);//关闭抽屉命令
    char * openover_command(int number);//无条件开锁命令
    char * readstatus_command(int number);//读取状态命令
    char * readwarn_command(int number);//读取警报命令
    char * releasewarn_command(int number);//解除警报命令

    char *readCardId(int number);//读取卡号ID
    char *writeCardID(int number);//锁号写入修改命令
    char *catCardID(int number);//钥匙配对命令
    char *catCardIDw(int number,QString cardid);//钥匙配对命令
    unsigned char QStringToUnchar(QString sendstring);

private:
    char buf[9];

};

#endif // GUNCONMAN_H
