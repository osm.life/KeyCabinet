#ifndef MYBUTTON_H
#define MYBUTTON_H

#include <QToolButton>

class myButton : public QToolButton
{
    Q_OBJECT
public:
    explicit myButton(QWidget *parent = 0);
    void setWindowName(QString);
    void setMyPicture(QString,int);


signals:
    void sendNum(QString);

public slots:
    void senddata();

};

#endif // MYBUTTON_H
