#include "showwritelock.h"
#include "ui_showwritelock.h"
#include"newqserialport.h"
#include"gunconman.h"
#include<QDebug>
#include<QTimer>
showWriteLock::showWriteLock(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::showWriteLock)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);

}

showWriteLock::~showWriteLock()
{
    delete ui;
}

void showWriteLock::on_cancelBtn_clicked()
{
    this->close();
}

void showWriteLock::on_okBtn_clicked()
{

    //发送命令写入锁号
    if(typeflag==1)
    {
        ui->tipUser->setText("正在写入抽屉琐号，请等待***");
    }
    if(typeflag==2)
    {
    ui->tipUser->setText("正在写入感应板琐号，请等待***");
    }
    this->update();
    gunconman comman;
    newqserialport serial;
    serial.openMycom(1,1);
    serial.WriteMyCom(comman.writeCardID(locknumber));
    QString backinfor=serial.ReadMyCom(3000);
    qDebug()<<"backinfor:"<<backinfor;
    QString infor=getInfor(backinfor);
    ui->tipUser->setText(infor);
     ui->cancelBtn->setText("关闭");

}

void showWriteLock::getLockNo(int type,int num)//type 1写锁号，type 2写版号
{
    ui->cancelBtn->setText("取消");

    if(type==1)//写锁号
    {
    ui->tipUser->setText("是否写入锁号："+QString::number(num));
    locknumber=num;
    typeflag=1;
    }
    if(type==2)//写版号
    {
    ui->tipUser->setText("是否写入板号："+QString::number(num));
    locknumber=100+num;
    typeflag=2;
    }
}
QString showWriteLock::getInfor(QString inforr)
{
    QString reback=inforr.mid(inforr.size()/2,inforr.size()); //一次读取发送和返回数据
  qDebug()<<"reback:"<<reback;
   QString reselt;
  if(reback.mid(0,2)=="b0")
  {

      QString statusflag=reback.mid(14,2);
      if(statusflag=="01")
      {

          if(typeflag==1)
          {
               QString num=QString::number(reback.mid(2,2).toInt(0,16));
              reselt=num+"号抽屉琐号写入成功！";

          }
          if(typeflag==2)
          {
                QString num=QString::number(reback.mid(2,2).toInt(0,16)-100);
              reselt=num+"号感应板琐号写入成功！";
          }
      }
      else
      {
           if(typeflag==1)
           {
                QString num=QString::number(reback.mid(2,2).toInt(0,16));
          reselt=num+"号抽屉号写入失败！";

           }
            if(typeflag==2)
            {
                  QString num=QString::number(reback.mid(2,2).toInt(0,16)-100);
           reselt=num+"号抽感应板写入失败！";
            }
      }
      return reselt;
  }
  else
  {
      return "锁号写入出错！";
    }
}
