/********************************************************************************
** Form generated from reading UI file 'showtime.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWTIME_H
#define UI_SHOWTIME_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_showTime
{
public:
    QHBoxLayout *horizontalLayout;
    QLabel *tipuser;

    void setupUi(QDialog *showTime)
    {
        if (showTime->objectName().isEmpty())
            showTime->setObjectName(QString::fromUtf8("showTime"));
        showTime->resize(342, 50);
        QFont font;
        font.setPointSize(15);
        showTime->setFont(font);
        showTime->setStyleSheet(QString::fromUtf8("background-color: rgb(249, 249, 249);"));
        showTime->setSizeGripEnabled(false);
        showTime->setModal(false);
        horizontalLayout = new QHBoxLayout(showTime);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tipuser = new QLabel(showTime);
        tipuser->setObjectName(QString::fromUtf8("tipuser"));
        tipuser->setFont(font);
        tipuser->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(tipuser);


        retranslateUi(showTime);

        QMetaObject::connectSlotsByName(showTime);
    } // setupUi

    void retranslateUi(QDialog *showTime)
    {
        showTime->setWindowTitle(QString());
        tipuser->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class showTime: public Ui_showTime {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWTIME_H
