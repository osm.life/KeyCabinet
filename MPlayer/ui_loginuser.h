/********************************************************************************
** Form generated from reading UI file 'loginuser.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINUSER_H
#define UI_LOGINUSER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_loginUser
{
public:
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QLineEdit *password;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_3;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QPushButton *num1;
    QPushButton *num2;
    QPushButton *num3;
    QPushButton *num4;
    QPushButton *num5;
    QPushButton *num6;
    QPushButton *num7;
    QPushButton *num8;
    QPushButton *num9;
    QPushButton *num0;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *cancel_btn;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *ok_btn;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QDialog *loginUser)
    {
        if (loginUser->objectName().isEmpty())
            loginUser->setObjectName(QString::fromUtf8("loginUser"));
        loginUser->resize(1024, 600);
        QFont font;
        font.setPointSize(23);
        loginUser->setFont(font);
        verticalLayout_2 = new QVBoxLayout(loginUser);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 137, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(loginUser);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("background-color: rgb(250, 250, 250);"));

        horizontalLayout->addWidget(label);

        password = new QLineEdit(loginUser);
        password->setObjectName(QString::fromUtf8("password"));
        QFont font1;
        font1.setPointSize(18);
        password->setFont(font1);
        password->setMaxLength(12);
        password->setFrame(false);
        password->setEchoMode(QLineEdit::Normal);
        password->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(password);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        num1 = new QPushButton(loginUser);
        num1->setObjectName(QString::fromUtf8("num1"));

        gridLayout->addWidget(num1, 0, 1, 1, 1);

        num2 = new QPushButton(loginUser);
        num2->setObjectName(QString::fromUtf8("num2"));

        gridLayout->addWidget(num2, 0, 2, 1, 1);

        num3 = new QPushButton(loginUser);
        num3->setObjectName(QString::fromUtf8("num3"));

        gridLayout->addWidget(num3, 0, 3, 1, 1);

        num4 = new QPushButton(loginUser);
        num4->setObjectName(QString::fromUtf8("num4"));

        gridLayout->addWidget(num4, 0, 4, 1, 1);

        num5 = new QPushButton(loginUser);
        num5->setObjectName(QString::fromUtf8("num5"));

        gridLayout->addWidget(num5, 0, 5, 1, 1);

        num6 = new QPushButton(loginUser);
        num6->setObjectName(QString::fromUtf8("num6"));

        gridLayout->addWidget(num6, 1, 1, 1, 1);

        num7 = new QPushButton(loginUser);
        num7->setObjectName(QString::fromUtf8("num7"));

        gridLayout->addWidget(num7, 1, 2, 1, 1);

        num8 = new QPushButton(loginUser);
        num8->setObjectName(QString::fromUtf8("num8"));

        gridLayout->addWidget(num8, 1, 3, 1, 1);

        num9 = new QPushButton(loginUser);
        num9->setObjectName(QString::fromUtf8("num9"));

        gridLayout->addWidget(num9, 1, 4, 1, 1);

        num0 = new QPushButton(loginUser);
        num0->setObjectName(QString::fromUtf8("num0"));

        gridLayout->addWidget(num0, 1, 5, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_6, 0, 0, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_7, 0, 6, 1, 1);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        cancel_btn = new QPushButton(loginUser);
        cancel_btn->setObjectName(QString::fromUtf8("cancel_btn"));

        horizontalLayout_2->addWidget(cancel_btn);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        ok_btn = new QPushButton(loginUser);
        ok_btn->setObjectName(QString::fromUtf8("ok_btn"));

        horizontalLayout_2->addWidget(ok_btn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_2->addLayout(verticalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 128, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);


        retranslateUi(loginUser);

        QMetaObject::connectSlotsByName(loginUser);
    } // setupUi

    void retranslateUi(QDialog *loginUser)
    {
        loginUser->setWindowTitle(QApplication::translate("loginUser", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("loginUser", "\350\257\267\350\276\223\345\205\245\346\235\241\347\240\201\346\210\226\345\257\206\347\240\201", 0, QApplication::UnicodeUTF8));
        num1->setText(QApplication::translate("loginUser", "1", 0, QApplication::UnicodeUTF8));
        num2->setText(QApplication::translate("loginUser", "2", 0, QApplication::UnicodeUTF8));
        num3->setText(QApplication::translate("loginUser", "3", 0, QApplication::UnicodeUTF8));
        num4->setText(QApplication::translate("loginUser", "4", 0, QApplication::UnicodeUTF8));
        num5->setText(QApplication::translate("loginUser", "5", 0, QApplication::UnicodeUTF8));
        num6->setText(QApplication::translate("loginUser", "6", 0, QApplication::UnicodeUTF8));
        num7->setText(QApplication::translate("loginUser", "7", 0, QApplication::UnicodeUTF8));
        num8->setText(QApplication::translate("loginUser", "8", 0, QApplication::UnicodeUTF8));
        num9->setText(QApplication::translate("loginUser", "9", 0, QApplication::UnicodeUTF8));
        num0->setText(QApplication::translate("loginUser", "0", 0, QApplication::UnicodeUTF8));
        cancel_btn->setText(QApplication::translate("loginUser", "\345\217\226\346\266\210", 0, QApplication::UnicodeUTF8));
        ok_btn->setText(QApplication::translate("loginUser", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class loginUser: public Ui_loginUser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINUSER_H
