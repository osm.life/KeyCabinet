#include "mymessagebox.h"

myMessageBox::myMessageBox(QWidget *parent) :
    QMessageBox(parent)
{
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setText("是否退出柜体管理？");
    QFont font;
    font.setPointSize(16);
    this->setFont(font);
    ok=new QToolButton(this);
    cancel=new QToolButton(this);

    connect(ok,SIGNAL(clicked()),this,SLOT(getChooseYes()));
    connect(cancel,SIGNAL(clicked()),this,SLOT(getChooseNo()));
    ok->setIcon(QIcon(QPixmap(":/images/sure.png")));
    ok->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    ok->setIconSize(QSize(30,30));
    cancel->setIcon(QIcon(QPixmap(":/images/cancel.png")));
    cancel->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    cancel->setIconSize(QSize(30,30));
    this->addButton(ok,QMessageBox::ActionRole);
    this->addButton(cancel,QMessageBox::ActionRole);
    this->setButtonText(0,"确认");
    this->setButtonText(1,"取消");
    this->resize(QSize(300,400));
    keytime=-1;


}
void myMessageBox::keyPressEvent(QKeyEvent *e)
{

}
void myMessageBox::keyReleaseEvent(QKeyEvent *e)
{
//qDebug()<<e->text();

    if(keytime>0)
    {
if(e->key()==Qt::Key_Escape)
{
    qDebug()<<"escape";
    keyFlag=0;
    cancel->click();
}
if(e->key()==Qt::Key_Return)
{
     qDebug()<<"enter";
     keyFlag=1;
     ok->click();

}
    }
keytime++;
}
int myMessageBox::choose()
{
    return keyFlag;
}
void myMessageBox::getChooseNo()
{
    keyFlag=0;

}
void myMessageBox::getChooseYes()
{
    keyFlag=1;
}

