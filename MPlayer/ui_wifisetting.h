/********************************************************************************
** Form generated from reading UI file 'wifisetting.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIFISETTING_H
#define UI_WIFISETTING_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_wifisetting
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *updateBtn;
    QPushButton *closeBtn;

    void setupUi(QDialog *wifisetting)
    {
        if (wifisetting->objectName().isEmpty())
            wifisetting->setObjectName(QString::fromUtf8("wifisetting"));
        wifisetting->resize(400, 300);
        QFont font;
        font.setPointSize(16);
        wifisetting->setFont(font);
        verticalLayout_2 = new QVBoxLayout(wifisetting);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        listWidget = new QListWidget(wifisetting);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        verticalLayout->addWidget(listWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        updateBtn = new QPushButton(wifisetting);
        updateBtn->setObjectName(QString::fromUtf8("updateBtn"));

        horizontalLayout->addWidget(updateBtn);

        closeBtn = new QPushButton(wifisetting);
        closeBtn->setObjectName(QString::fromUtf8("closeBtn"));

        horizontalLayout->addWidget(closeBtn);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(wifisetting);

        QMetaObject::connectSlotsByName(wifisetting);
    } // setupUi

    void retranslateUi(QDialog *wifisetting)
    {
        wifisetting->setWindowTitle(QApplication::translate("wifisetting", "Dialog", 0, QApplication::UnicodeUTF8));
        updateBtn->setText(QApplication::translate("wifisetting", "\345\210\267\346\226\260", 0, QApplication::UnicodeUTF8));
        closeBtn->setText(QApplication::translate("wifisetting", "\345\205\263\351\227\255", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class wifisetting: public Ui_wifisetting {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIFISETTING_H
