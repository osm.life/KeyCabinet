#include "config.h"
#include<QSettings>
config::config(QObject *parent) :
    QObject(parent)
{
}
QVariant config::read(QString group, QString values)
{
    QSettings settings("config.ini", QSettings::IniFormat); // 当前目录的INI文件
    QVariant result = settings.value(group+"/"+values);
    return result;
}
bool config::write(QString group, QString values, QVariant arg)
{
        QSettings settings("config.ini", QSettings::IniFormat); // 当前目录的INI文件
        settings.beginGroup(group);
        settings.setValue(values, arg);
        settings.endGroup();
        return true;
}
QVariant config::readtemp(QString group, QString values)
{
    QSettings settings("userlogin.ini", QSettings::IniFormat); // 当前目录的INI文件
    QVariant result = settings.value(group+"/"+values);
    return result;
}
bool config::writetemp(QString group, QString values, QVariant arg)
{
        QSettings settings("userlogin.ini", QSettings::IniFormat); // 当前目录的INI文件
        settings.beginGroup(group);
        settings.setValue(values, arg);
        settings.endGroup();
        return true;
}



