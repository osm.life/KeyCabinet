#ifndef RECEIVENETWORKTHREAD_H
#define RECEIVENETWORKTHREAD_H

#include <QThread>
#include<QtNetwork/QTcpSocket>
#include<QtNetwork/QTcpServer>
#include"config.h"
#include"command.h"
#include<QTimer>
#include<QMutex>
#include"networktest.h"
#include<QQueue>
#include"watchdog.h"
#include"syslog.h"


class receiveNetworkThread : public QThread
{
    Q_OBJECT
public:
    explicit receiveNetworkThread(QObject *parent = 0);
    char ConvertHexChar(char ch);
    QByteArray GetHexValue(QString str);
    void run();

signals:
    void sendData(QString);
    void sigalRelink();//数据重新连接网络
    void sendNetworkflag(bool);
    void signalCmdBacktoServer(QString);

    void signaldata(int);

private slots:
    void newConnect(); //连接服务器
    QString readMessage();  //接收数据
    void displayError(QAbstractSocket::SocketError);  //显示错误
    void getData(QString);//获取的数据
    bool writeMessage(QString);//发送数据到服务器

    QString readGprsMessage();
    bool writeGprsMessage(QString);
    bool checkCmdNumber(QString);//判断命令是否超过抽屉数，以及流水号重复错误

    void senddataTimer();
    void sendcmdToserver();

    void updatelinkFlag();

    bool readSmsCmd(QString);//读取短信

    void updateconf(QString);//配置文件检测


    void faileCmdBacktoServer(QString);//将失败的命令重新发送


private:
  QTcpSocket *tcpSocket;
  QString message;  //存放从服务器接收到的字符串
  quint16 blockSize;  //存放文件的大小信息
  bool networkflag;//网络连接标志

   QString ip,Ipport,phonenumber,networkmode;
  // networkTest gprs;//网络配置
   //Gprs smsClass;//短信收发
   config conf;
   QString CabinetNo;//柜号
   int maxDrawNo;//最大抽屉号
    command cmd;

    QStringList cmdNumber;//存一百条流水号

    int cmdindex;//命令序号
    QStringList netcmdlist;
    QStringList sendcmdlist;
    QTimer *timersenddata;
   // QTimer *timerreaddata;

    QTimer *timeServer;//服务器状态更新定时器

    QMutex mutex;

    int timeReceiveFlag;
    int timeSendretry;//重发计时

    int readSmsCmdTime;//时间读取短信时间
    QStringList failsecmdlist;

    watchdog *dog;

    syslog log;

    bool connectFlag;//服务器连接标志



};

#endif // RECEIVENETWORKTHREAD_H
