/****************************************************************************
** Meta object code from reading C++ file 'MyIpAddrEdit.h'
**
** Created: Wed Sep 2 14:36:04 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "MyIpAddrEdit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MyIpAddrEdit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MyIpAddrEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   14,   13,   13, 0x05,
      40,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      60,   14,   13,   13, 0x08,
      85,   14,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MyIpAddrEdit[] = {
    "MyIpAddrEdit\0\0text\0textchanged(QString)\0"
    "textedited(QString)\0textchangedslot(QString)\0"
    "texteditedslot(QString)\0"
};

void MyIpAddrEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MyIpAddrEdit *_t = static_cast<MyIpAddrEdit *>(_o);
        switch (_id) {
        case 0: _t->textchanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->textedited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->textchangedslot((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->texteditedslot((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MyIpAddrEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MyIpAddrEdit::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MyIpAddrEdit,
      qt_meta_data_MyIpAddrEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MyIpAddrEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MyIpAddrEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MyIpAddrEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MyIpAddrEdit))
        return static_cast<void*>(const_cast< MyIpAddrEdit*>(this));
    return QWidget::qt_metacast(_clname);
}

int MyIpAddrEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void MyIpAddrEdit::textchanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MyIpAddrEdit::textedited(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
