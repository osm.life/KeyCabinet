#ifndef MYMESSAGEBOX_H
#define MYMESSAGEBOX_H

#include <QMessageBox>
#include<QKeyEvent>
#include<QDebug>
#include<QToolButton>
class myMessageBox : public QMessageBox
{
    Q_OBJECT
public:
    explicit myMessageBox(QWidget *parent=0);
    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    QToolButton *ok;
    QToolButton *cancel;
    int choose();


signals:

public slots:
    void getChooseNo();
    void getChooseYes();
private:
    int keyFlag;
    int keytime;

};

#endif // MYMESSAGEBOX_H
