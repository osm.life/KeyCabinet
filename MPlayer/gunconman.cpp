#include "gunconman.h"

gunconman::gunconman()
{
}
char* gunconman::open_command(int number)
{
            buf[0]=0XF3;
            buf[1]=number;
            buf[2]=0X00;
            buf[3]=0X00;
            buf[4]=0X00;
            buf[5]=0XA3;
            buf[6]=0XA3;
            buf[7]=0XA3;
            buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
            return buf;

}
char *  gunconman::close_command(int number)
{
    buf[0]=0XF5;
    buf[1]=number;
    buf[2]=0X00;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0XA5;
    buf[6]=0XA5;
    buf[7]=0XA5;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;

}
char *  gunconman::openover_command(int number)
{
    buf[0]=0XF6;
    buf[1]=number;
    buf[2]=0X00;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0XA6;
    buf[6]=0XA6;
    buf[7]=0XA6;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;

}
char * gunconman::readstatus_command(int number)
{
    buf[0]=0XF9;
    buf[1]=number;
    buf[2]=0X00;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0XA9;
    buf[6]=0XA9;
    buf[7]=0XA9;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;

}
char * gunconman::readwarn_command(int number)
{
    buf[0]=0XFA;
    buf[1]=number;
    buf[2]=0X00;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0XAA;
    buf[6]=0XAA;
    buf[7]=0XAA;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;

}
char * gunconman::releasewarn_command(int number)
{
    buf[0]=0XFF;
    buf[1]=number;
    buf[2]=0X00;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0XAF;
    buf[6]=0XAF;
    buf[7]=0XAF;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;

}
/************卡号命令*******************/
char *gunconman::readCardId(int number)//读取卡号ID
{
    buf[0]=0XFB;
    buf[1]=number;
    buf[2]=0X00;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0XAB;
    buf[6]=0XAB;
    buf[7]=0XAB;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;
}
char *gunconman::writeCardID(int number)//锁号写入修改命令
{
    buf[0]=0XF0;
    buf[1]=0X00;
    buf[2]=number;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0XA0;
    buf[6]=0XA0;
    buf[7]=0XA0;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;
}
char *gunconman::catCardID(int number)//钥匙配对命令
{
    buf[0]=0XF4;
    buf[1]=number;
    buf[2]=0X00;
    buf[3]=0X00;
    buf[4]=0X00;
    buf[5]=0X00;
    buf[6]=0X00;
    buf[7]=0X02;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;
}
char *gunconman::catCardIDw(int number, QString cardid)
{

    buf[0]=0XF4;
    buf[1]=number;
    buf[2]=QStringToUnchar(cardid.mid(0,2));
    buf[3]=QStringToUnchar(cardid.mid(2,2));
    buf[4]=QStringToUnchar(cardid.mid(4,2));
    buf[5]=QStringToUnchar(cardid.mid(6,2));
    buf[6]=QStringToUnchar(cardid.mid(8,2));
    buf[7]=0x01;
    buf[8]=~(buf[0]+buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]+buf[7])&0x00ff;
    return buf;
}
unsigned char gunconman::QStringToUnchar(QString sendstring)
{
std::string datestr=sendstring.toStdString();
if(sendstring.size()==2)
{
    unsigned char temp= datestr[0];
    unsigned char temp1=datestr[1];
    if(temp>='0'&&temp<='9')
    {
     temp&=0x0f;
    }
    if(temp>='a'&&temp<='f')
    {
     temp&=0x0f;
     temp+=0x09;
    }
    if(temp>='A'&&temp<='F')
    {
     temp&=0x0f;
     temp+=0x09;
    }
    temp<<=4;

    if(temp1>='0'&&temp1<='9')
    {
     temp1&=0x0f;
    }
    if(temp1>='a'&&temp1<='f')
    {
     temp1&=0x0f;
     temp1+=0x09;
    }
    if(temp1>='A'&&temp1<='F')
    {
     temp1&=0x0f;
     temp1+=0x09;
    }
    unsigned char ok=(temp+temp1);
    return ok;
}
else if(sendstring.size()==4)
{
    unsigned char temp= datestr[0];
    unsigned char temp1=datestr[1];
    unsigned char temp2=datestr[2];
    unsigned char temp3=datestr[3];
    if(temp>='0'&&temp<='9')
    {
     temp&=0x0f00;
    }
    if(temp>='a'&&temp<='f')
    {
     temp&=0x0f00;
     temp+=0x0900;
    }
    if(temp>='A'&&temp<='F')
    {
     temp&=0x0f00;
     temp+=0x0900;
    }
    temp<<=4;

    if(temp1>='0'&&temp1<='9')
    {
     temp1&=0x0f00;
    }
    if(temp1>='a'&&temp1<='f')
    {
     temp1&=0x0f00;
     temp1+=0x0900;
    }
    if(temp1>='A'&&temp1<='F')
    {
     temp1&=0x0f00;
     temp1+=0x0900;
    }
    /****************************/
    if(temp2>='0'&&temp2<='9')
    {
     temp2&=0x0f;
    }
    if(temp2>='a'&&temp2<='f')
    {
     temp2&=0x0f;
     temp2+=0x09;
    }
    if(temp2>='A'&&temp2<='F')
    {
     temp2&=0x0f;
     temp2+=0x09;
    }
    temp2<<=4;

    if(temp3>='0'&&temp3<='9')
    {
     temp3&=0x0f;
    }
    if(temp3>='a'&&temp3<='f')
    {
     temp3&=0x0f;
     temp3+=0x09;
    }
    if(temp3>='A'&&temp3<='F')
    {
     temp3&=0x0f;
     temp3+=0x09;
    }
    unsigned char ok=temp+temp1+temp2+temp3;
    return ok;
}
else
    return 0;
}
