/****************************************************************************
** Meta object code from reading C++ file 'showwritelock.h'
**
** Created: Fri Nov 13 09:19:14 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "showwritelock.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'showwritelock.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_showWriteLock[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      38,   14,   14,   14, 0x08,
      59,   57,   14,   14, 0x08,
      86,   14,   78,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_showWriteLock[] = {
    "showWriteLock\0\0on_cancelBtn_clicked()\0"
    "on_okBtn_clicked()\0,\0getLockNo(int,int)\0"
    "QString\0getInfor(QString)\0"
};

void showWriteLock::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        showWriteLock *_t = static_cast<showWriteLock *>(_o);
        switch (_id) {
        case 0: _t->on_cancelBtn_clicked(); break;
        case 1: _t->on_okBtn_clicked(); break;
        case 2: _t->getLockNo((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: { QString _r = _t->getInfor((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData showWriteLock::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject showWriteLock::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_showWriteLock,
      qt_meta_data_showWriteLock, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &showWriteLock::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *showWriteLock::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *showWriteLock::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_showWriteLock))
        return static_cast<void*>(const_cast< showWriteLock*>(this));
    return QDialog::qt_metacast(_clname);
}

int showWriteLock::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
