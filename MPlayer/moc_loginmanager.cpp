/****************************************************************************
** Meta object code from reading C++ file 'loginmanager.h'
**
** Created: Fri Nov 13 09:19:11 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "loginmanager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'loginmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_loginManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,
      32,   30,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      59,   13,   13,   13, 0x08,
      70,   13,   13,   13, 0x08,
      95,   13,   90,   13, 0x08,
     120,  115,   13,   13, 0x08,
     152,   13,   13,   13, 0x08,
     166,   30,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_loginManager[] = {
    "loginManager\0\0sendUserLogin()\0,\0"
    "sendManagerOp(int,QString)\0showtime()\0"
    "on_ok_btn_clicked()\0bool\0checkLogin(QString)\0"
    "arg1\0on_password_textEdited(QString)\0"
    "remoteLogin()\0getManagerOp(int,QString)\0"
};

void loginManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        loginManager *_t = static_cast<loginManager *>(_o);
        switch (_id) {
        case 0: _t->sendUserLogin(); break;
        case 1: _t->sendManagerOp((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->showtime(); break;
        case 3: _t->on_ok_btn_clicked(); break;
        case 4: { bool _r = _t->checkLogin((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: _t->on_password_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->remoteLogin(); break;
        case 7: _t->getManagerOp((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData loginManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject loginManager::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_loginManager,
      qt_meta_data_loginManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &loginManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *loginManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *loginManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_loginManager))
        return static_cast<void*>(const_cast< loginManager*>(this));
    return QDialog::qt_metacast(_clname);
}

int loginManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void loginManager::sendUserLogin()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void loginManager::sendManagerOp(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
