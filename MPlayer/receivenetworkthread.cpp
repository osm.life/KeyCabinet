#include "receivenetworkthread.h"
#include<QFileSystemWatcher>
receiveNetworkThread::receiveNetworkThread(QObject *parent) :
    QThread(parent)
{
    msleep(1000);
    tcpSocket=new QTcpSocket;
    ip=conf.read("Server","serverIp").toString();
    Ipport=conf.read("Server","serverPort").toString();

    phonenumber=conf.read("Phone","phone").toString();
    networkmode=conf.read("Network","networkmode").toString();
    CabinetNo=conf.read("Cabinet","cabinetNo").toString();
    maxDrawNo=conf.read("Cabinet","cabinetNumber").toInt();

      connectFlag=true;//服务器连接标志
    readSmsCmdTime=0;
    timeReceiveFlag=0;
    cmdindex=0;
    connect(tcpSocket,SIGNAL(readyRead()),this,SLOT(readMessage()));
    connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
    connect(tcpSocket,SIGNAL(disconnected()),this,SLOT(newConnect()));
    connect(this,SIGNAL(sigalRelink()),this,SLOT(newConnect()));

    connect(this,SIGNAL(signalCmdBacktoServer(QString)),this,SLOT(faileCmdBacktoServer(QString)));

    newConnect();//连接服务器

    tcpSocket->waitForReadyRead(500);

    timersenddata=new QTimer(this);
    connect(timersenddata,SIGNAL(timeout()),this,SLOT(senddataTimer()));
    connect(timersenddata,SIGNAL(timeout()),this,SLOT(sendcmdToserver()));
    //timerreaddata=new QTimer(this);
    timersenddata->start(1000);

    timeServer=new QTimer(this);
    connect(timeServer,SIGNAL(timeout()),this,SLOT(updatelinkFlag()));
    timeServer->start(1000);
    QFileSystemWatcher *filewatcher=new QFileSystemWatcher(this);
    filewatcher->addPath(QDir::currentPath()+"/config.ini");
    connect(filewatcher,SIGNAL(fileChanged(QString)),this,SLOT(updateconf(QString)));

    dog=new watchdog;
    dog->openwatchdog();

}
void receiveNetworkThread::updateconf(QString)
{
    ip=conf.read("Server","serverIp").toString();
    Ipport=conf.read("Server","serverPort").toString();
    phonenumber=conf.read("Phone","phone").toString();
    networkmode=conf.read("Network","networkmode").toString();
    CabinetNo=conf.read("Cabinet","cabinetNo").toString();
    maxDrawNo=conf.read("Cabinet","cabinetNumber").toInt();

    qDebug()<<"config had update !";
    qDebug()<<"serverip:"<<ip;
    qDebug()<<"serverport:"<<Ipport;
    newConnect();
}
void receiveNetworkThread::newConnect()
{

   blockSize=0;
   tcpSocket->abort();
   tcpSocket->connectToHost(ip,Ipport.toInt());
   qDebug()<<"try to connect server (socket ):"<<ip<<"  "<<Ipport;
}
QString receiveNetworkThread::readMessage()
{

    qDebug()<<"server cmd is comging";
     int StrSize=tcpSocket->bytesAvailable ();
     QByteArray temp=tcpSocket->readAll();
     QString strHex;//16进制数据
     if (!temp.isEmpty())
     {
         QDataStream out(&temp,QIODevice::ReadOnly);
         while (!out.atEnd())
         {
             qint8 outChar=0;
             out>>outChar;
             QString str=QString("%1").arg(outChar&0xFF,2,16,QLatin1Char('0'));
             if (str.length()>1)
             {
               strHex+=str;
             }
             else
             {
               strHex+="0"+str;
             }
         }

     }
        if(StrSize < blockSize)// 如果没有得到全部的数据，则返回，继续接收数据
            return "";
        else
        message=strHex;

        if(message.contains("serverStop")) //如果收到是服务器停止监听的信息
        {
          networkflag=false;
          tcpSocket->close ();
          blockSize = 0;
          return "";
        }

        timeReceiveFlag=0;
        blockSize = 0;

        qDebug()<<"sssss>>>"<<strHex;
        if(cmd.check_command(strHex))
        {
        timeReceiveFlag=0;
         if(strHex.size()>78)
         {

             qDebug()<<"<<<<<<<<<<<<<<<<<<<<<<<<cmdindex>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<cmdindex;
             if(strHex.contains("aa55a55a17e8"))//更新命令
             {
             netcmdlist<<strHex;
             log.writelog("connection.log","Received > :"+strHex);
             }
             else
             {
             int cmdsize=strHex.size()/78;
             for(int i=0;i<cmdsize;i++)
             {
             QString cmd_r=strHex.mid(78*i,78);
             bool ok=checkCmdNumber(cmd_r);
             emit signalCmdBacktoServer(cmd_r);//有数据过来
             log.writelog("connection.log","Received > :"+cmd_r);
             if(ok)
             {

               netcmdlist<<cmd_r;
             }
             else
             {
                //流水号重复了
             }

             }
             }
         }
         else
         {
             log.writelog("connection.log","Received > :"+strHex);
             emit signalCmdBacktoServer(strHex);//有数据过来
             bool ok=checkCmdNumber(strHex);
             if(ok)
             {
               netcmdlist<<strHex;

             }
             else
             {

              //流水号重复了
                 return "";
             }

         }
        }
        else
        {
        emit signalCmdBacktoServer(strHex);//确认服务器已经收到
        }
        return  "";


}
void receiveNetworkThread::displayError(QAbstractSocket::SocketError)
{
     qDebug() << "connect happen err:"<<tcpSocket->errorString(); //输出错误信息
     networkflag=false;

     if(connectFlag)
     {
     log.writelog("connection.log","Connect happen err:"+tcpSocket->errorString());
     }

}
void receiveNetworkThread::getData(QString str)//获取的数据
{
  QString head= cmd.getCmdHead(str);
  qDebug()<<"命令头："<<head;
  if(head=="09")
  {
  sendcmdlist<<str;
  }
    sendcmdlist<<str;

}
bool receiveNetworkThread::writeMessage(QString data)//发送数据到服务器
{

       QByteArray outData=data.toAscii();
       outData=this->GetHexValue(data);
       if(tcpSocket->state()==QAbstractSocket::ConnectedState)
       {
             int datanum=tcpSocket->write(outData);
             if(datanum>0)
             {
             log.writelog("connection.log","Send     > :"+data);
             return true;
             }
             else
             {
             return false;
             }
             }
       else
       {
           return false;
       }
}
QByteArray  receiveNetworkThread::GetHexValue(QString str) //转化为16进制发送
{
    QByteArray senddata;
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    senddata.resize(len/2);
    char lstr,hstr;
    for(int i=0; i<len; )
    {
        hstr=str[i].toAscii();
        if(hstr == ' ')
        {
            i++;
            continue;
        }
        i++;
        if(i >= len)
            break;
        lstr = str[i].toAscii();
        hexdata = ConvertHexChar(hstr);
        lowhexdata = ConvertHexChar(lstr);
        if((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata*16+lowhexdata;
        i++;
        senddata[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    senddata.resize(hexdatalen);
    return senddata;
}

char receiveNetworkThread::ConvertHexChar(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
        return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
        return ch-'a'+10;
    else return (-1);
}
QString receiveNetworkThread::readGprsMessage()//收短信
{
    //QString strMessage=smsClass.Read_msg();
    //return strMessage;
}
bool receiveNetworkThread::writeGprsMessage(QString infor)//发短信
{
//   if(smsClass.GSM_Send_Message(phonenumber,infor)>0)
//   return true;
//   else
//       return false;
}
bool receiveNetworkThread::checkCmdNumber(QString mycmd)//判断流水号是否重复，重复返回流水号重复命令；
{

    QString codenumber=cmd.getCmdNumber(mycmd);
    bool ok=cmdNumber.contains(codenumber);
    int maxDraw=cmd.getCabinetNo(mycmd);
    if(!ok)//流水号不重复
    {
        if(maxDraw<=maxDrawNo)
        {
            cmdNumber<<codenumber;
            if(cmdNumber.size()>101)
            {
                cmdNumber.removeFirst();
            }
        return true;
        }
        else
        {
        writeMessage(cmd.rebackInfor(mycmd,"e8"));//返回超出最大抽屉号
        return false;
        }

    }
    else
    {
                for(int i=0;i<cmdNumber.size();i++)
                {
                    qDebug()<<"流水号>>>>>>>>>>>>>>："<<i<<"  "<<cmdNumber.at(i);
                }
        return false;
    }
}
void receiveNetworkThread::senddataTimer()
{
    if(netcmdlist.size()>0)
    {

        QString strHex=netcmdlist.first();
        bool ok=(strHex.contains("aa55a55a06f9")||strHex.contains("aa55a55a07f8")||strHex.contains("aa55a55a08f7")||strHex.contains("aa55a55a09f6")||strHex.contains("aa55a55a0af5")||strHex.contains("aa55a55a0bf4")||strHex.contains("aa55a55a0cf3")||strHex.contains("aa55a55a0df2"));
        if(ok)
        {

        }
        else
        {
             qDebug()<<"send network data to runing:"<<strHex;
             emit signaldata(1);//收到服务器数据
             emit sendData(strHex);
        }
        netcmdlist.removeFirst();
    }
}

void receiveNetworkThread::run()
{
}
void receiveNetworkThread::updatelinkFlag()
{
     dog->feeddog();//喂狗
   //qDebug()<<"socket status:"<<tcpSocket->state();
    if(tcpSocket->state()==QAbstractSocket::UnconnectedState)
    {
      networkflag=false;
       newConnect();

       if(connectFlag)//第一次断开
       {
       connectFlag=false;
       log.writelog("connection.log","UnconnectedState,disconnect");
       }

    }
    else if(tcpSocket->state()==QAbstractSocket::ConnectingState)
    {
        networkflag=false;
        if(connectFlag)
        {
        connectFlag=false;
        log.writelog("connection.log","ConnectingState,Try to Connecte Server");
        }

    }
    else if(tcpSocket->state()==QAbstractSocket::ConnectedState)
    {
     networkflag=true;
     if(connectFlag)
     {

     }
     else//第一次连接
     {
     log.writelog("connection.log","ConnectedState,Connected Server");
     connectFlag=true;
     }

    }
    else
    {
     networkflag=false;
    }
     emit sendNetworkflag(networkflag);
     if((timeReceiveFlag==150)||(timeReceiveFlag==250))
     {
        newConnect();
     }
     if(timeReceiveFlag==300)
     {
        newConnect();
     }


}
void receiveNetworkThread::sendcmdToserver()//发送命令给服务器
{
    dog->feeddog();//喂狗
    timeReceiveFlag++;
    timeSendretry++;
    if(timeSendretry>100)
    {
    timeSendretry=0;
    }
    if(timeReceiveFlag>=200)
    {
        if(networkmode=="1")//4G网络
        {
        timeReceiveFlag=0;
        QString timecurrent=QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
        QString logdata="echo \""+timecurrent+"\" >signalSever.txt";
        system(logdata.toAscii());
        }
    }
    if(sendcmdlist.size()>0)
    {
    QString str=sendcmdlist.first();
    bool okflag= writeMessage(str);
    if(okflag)
    {
     emit signaldata(2);//收到服务器数据
     sendcmdlist.removeFirst();
     if(str.contains("ba55b55b06f9")||str.contains("ba55b55b09f6"))//取还换相关的操作
     {

     }
     else if(str.contains("01fe")||str.contains("02fd")||str.contains("08f7")||str.contains("07f8")||str.contains("03fc")||str.contains("16e9")||str.contains("0df2"))//取还换相关的操作
     {
             failsecmdlist<<str;
             failsecmdlist<<str;
             failsecmdlist<<str;
             if(failsecmdlist.size()>24)
             {
             failsecmdlist.removeFirst();
             }
             timeSendretry=0;
     }
     else
     {

     }
    }
    else
    {
         networkflag=false;
         newConnect();//连接服务器
         if(sendcmdlist.size()>80)
         {
          sendcmdlist.removeFirst();
         }
         if(str.contains("ba55b55b06f9")||str.contains("ba55b55b09f6"))//取还换相关的操作
         {
          sendcmdlist.removeFirst();
         }
         }
    }

    if(timeSendretry%9==8)//每隔8秒发送一次
    {
        if(failsecmdlist.size()>0)
        {
            QString str=failsecmdlist.first();
            writeMessage(str);//发送发服务器
            log.writelog("connection.log","Send(try)> :"+str);
            failsecmdlist.removeFirst();
        }
    }


}
bool receiveNetworkThread::readSmsCmd(QString strHex)
{

}

void receiveNetworkThread::faileCmdBacktoServer(QString cmd_reback)
{
   QString client=cmd_reback.mid(8,8)+cmd_reback.mid(24,4);
   qDebug()<<"failseCmdBacktoServer>>>>>>>>>>>>>"<<client<<"  size:"<<failsecmdlist.size();
   if(failsecmdlist.size()>0)
        {
                for(int i=failsecmdlist.size()-1;i>=0;i--)
                {
                    qDebug()<<"check server cmd:"<<failsecmdlist.at(i);
                    QString servercmd=failsecmdlist.at(i);
                    servercmd=servercmd.mid(8,8)+servercmd.mid(24,4);
                    if(servercmd==client)
                    {
                        qDebug()<<"check server has got the data is ok:"<<cmd_reback;
                        failsecmdlist.removeAt(i);
                    }
                }

        }
}
