/****************************************************************************
** Meta object code from reading C++ file 'receivenetworkthread.h'
**
** Created: Fri Nov 13 09:44:54 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "receivenetworkthread.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'receivenetworkthread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_receiveNetworkThread[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x05,
      40,   21,   21,   21, 0x05,
      54,   21,   21,   21, 0x05,
      76,   21,   21,   21, 0x05,
     107,   21,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
     123,   21,   21,   21, 0x08,
     144,   21,  136,   21, 0x08,
     158,   21,   21,   21, 0x08,
     201,   21,   21,   21, 0x08,
     223,   21,  218,   21, 0x08,
     245,   21,  136,   21, 0x08,
     263,   21,  218,   21, 0x08,
     289,   21,  218,   21, 0x08,
     313,   21,   21,   21, 0x08,
     329,   21,   21,   21, 0x08,
     347,   21,   21,   21, 0x08,
     364,   21,  218,   21, 0x08,
     384,   21,   21,   21, 0x08,
     404,   21,   21,   21, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_receiveNetworkThread[] = {
    "receiveNetworkThread\0\0sendData(QString)\0"
    "sigalRelink()\0sendNetworkflag(bool)\0"
    "signalCmdBacktoServer(QString)\0"
    "signaldata(int)\0newConnect()\0QString\0"
    "readMessage()\0displayError(QAbstractSocket::SocketError)\0"
    "getData(QString)\0bool\0writeMessage(QString)\0"
    "readGprsMessage()\0writeGprsMessage(QString)\0"
    "checkCmdNumber(QString)\0senddataTimer()\0"
    "sendcmdToserver()\0updatelinkFlag()\0"
    "readSmsCmd(QString)\0updateconf(QString)\0"
    "faileCmdBacktoServer(QString)\0"
};

void receiveNetworkThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        receiveNetworkThread *_t = static_cast<receiveNetworkThread *>(_o);
        switch (_id) {
        case 0: _t->sendData((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->sigalRelink(); break;
        case 2: _t->sendNetworkflag((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->signalCmdBacktoServer((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->signaldata((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->newConnect(); break;
        case 6: { QString _r = _t->readMessage();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 7: _t->displayError((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        case 8: _t->getData((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: { bool _r = _t->writeMessage((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: { QString _r = _t->readGprsMessage();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 11: { bool _r = _t->writeGprsMessage((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->checkCmdNumber((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: _t->senddataTimer(); break;
        case 14: _t->sendcmdToserver(); break;
        case 15: _t->updatelinkFlag(); break;
        case 16: { bool _r = _t->readSmsCmd((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 17: _t->updateconf((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 18: _t->faileCmdBacktoServer((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData receiveNetworkThread::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject receiveNetworkThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_receiveNetworkThread,
      qt_meta_data_receiveNetworkThread, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &receiveNetworkThread::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *receiveNetworkThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *receiveNetworkThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_receiveNetworkThread))
        return static_cast<void*>(const_cast< receiveNetworkThread*>(this));
    return QThread::qt_metacast(_clname);
}

int receiveNetworkThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void receiveNetworkThread::sendData(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void receiveNetworkThread::sigalRelink()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void receiveNetworkThread::sendNetworkflag(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void receiveNetworkThread::signalCmdBacktoServer(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void receiveNetworkThread::signaldata(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
