#ifndef PICTUREPLAYER_H
#define PICTUREPLAYER_H

#include <QWidget>
#include <QMouseEvent>
#include<QKeyEvent>
#include <QPainter>
#include <QPixmap>
#include <QBitmap>
#include<QTimer>
#include<QDir>
#include<QLabel>

namespace Ui {
    class picturePlayer;
}

class picturePlayer : public QWidget
{
    Q_OBJECT

public:
    explicit picturePlayer(QWidget *parent = 0);
    ~picturePlayer();

    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *);


    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *);

protected slots:

    void pictures(QString);

private:
    Ui::picturePlayer *ui;
    QPixmap *pix;
    int count;
    QTimer *timer;
    QDir directory;
    QStringList files;
    QString picPath;

    QLabel secondPicture;
    QLabel firstPicture;

    int pictureCount;

   QString firstString;
   QString endString;
   QString codeString;


private slots:
    void changePicture();
signals:
    void siganalCode(int,QString);//发送条码


};

#endif // PICTUREPLAYER_H
