#include "newqserialport.h"
#include<QDebug>
#include"gunconman.h"
#include<QFileInfo>
newqserialport::newqserialport(QObject *parent) :
    QObject(parent)
{
    number_dev=2;
}

QByteArray newqserialport::GetHexValue(QString str)
{
    QByteArray senddata;
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    senddata.resize(len/2);
    char lstr,hstr;
    for(int i=0; i<len; )
    {
        hstr=str[i].toAscii();
        if(hstr == ' ')
        {
            i++;
            continue;
        }
        i++;
        if(i >= len)
            break;
        lstr = str[i].toAscii();
        hexdata = ConvertHexChar(hstr);
        lowhexdata = ConvertHexChar(lstr);
        if((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata*16+lowhexdata;
        i++;
        senddata[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    senddata.resize(hexdatalen);
    return senddata;
}

char newqserialport::ConvertHexChar(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
        return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
        return ch-'a'+10;
    else return (-1);
}

QString newqserialport::ReadMyCom()
{

    MyHelper::DelayTime(100);//延时100毫秒

    QString strHex;//16进制数据

    QByteArray temp=myCom->readAll();

    if (!temp.isEmpty())
    {
        QDataStream out(&temp,QIODevice::ReadWrite);
        while (!out.atEnd())
        {
            qint8 outChar=0;
            out>>outChar;
            QString str=QString("%1").arg(outChar&0xFF,2,16,QLatin1Char('0'));

            if (str.length()>1)
            {
               // strHex+=str+" ";
                strHex+=str;
            }
            else
            {
               // strHex+="0"+str+" ";
                strHex+="0"+str;
            }
        }

        return strHex;
    }
}

QString newqserialport::ReadMyCom(int time)
{

    MyHelper::DelayTime(time);//延时100毫秒

    QString strHex;//16进制数据

    QByteArray temp=myCom->readAll();

    if (!temp.isEmpty())
    {
        QDataStream out(&temp,QIODevice::ReadWrite);
        while (!out.atEnd())
        {
            qint8 outChar=0;
            out>>outChar;
            QString str=QString("%1").arg(outChar&0xFF,2,16,QLatin1Char('0'));

            if (str.length()>1)
            {
               // strHex+=str+" ";
                strHex+=str;
            }
            else
            {
                //strHex+="0"+str+" ";
                strHex+="0"+str;
            }
        }

        return strHex;
    }
}
QString newqserialport::ReadSmsCom()
{

    MyHelper::DelayTime(100);//延时100毫秒
    QString strHex;
    QByteArray temp=myCom->readAll();
    strHex=QString::fromAscii(temp);

    return strHex;
}

QString newqserialport::ReadSmsCom(int time)
{
    MyHelper::DelayTime(time);//延时100毫秒
    QString strHex;//16进制数据
    QByteArray temp=myCom->readAll();
    strHex=QString::fromAscii(temp);
    return strHex;
}
void newqserialport::WriteSmsCom(QString str)
{
    str=str+"\r\0";
    QByteArray outData=str.toAscii();
    myCom->write(outData);
}
void newqserialport::WriteMyCom(QString str)
{
    QByteArray outData=str.toAscii();
    int size=outData.size();
    //如果串口没有打开
    if (!myCom->isOpen())
    {
        return;
    }
    //如果发送数据为空
    if (outData.isEmpty())
    {
        return;
    }
    //linux下的回车符为一个字符\r
    if (outData[size-1]!='\r')
    {
        outData.resize(size+1);
        outData[size]='\r';
    }
    //默认16进制发送不加入回车符
    //转化为16进制发送
        outData=this->GetHexValue(str);
        size=outData.size();
        myCom->write(outData);
}
void newqserialport::WriteMyCom(char *str)
{

    if (!myCom->isOpen())
    {
        return;
    }
        QByteArray outData=QByteArray(str,9);
      // qDebug()<<"write date:"<<outData.toHex();//加上这句可以执行串口命令
        myCom->write(outData);
}
void newqserialport::WriteMyCom(char *str,int size)
{
  if(!myCom->isOpen())
    {
     return;
    }
        QByteArray outData=QByteArray(str,size);
        myCom->write(outData);
}

void newqserialport::openMycom(int type,int com)
{
 QString portName="/dev/ttyUSB0";
 //QString portName="/dev/ttySAC3";//抽屉端口
    if(com==2)
    {
    QString comName="/dev/ttyACM";
    portName="/dev/ttyACM2";//
    for(int i=128;i>=2;i--)
    {
     QString tempcom=comName+QString::number(i);
     QFileInfo info(tempcom);
     if(info.exists())
     {
         i=0;
         portName=tempcom;
         qDebug()<<tempcom<<" has";
     }
     else
     {
         qDebug()<<tempcom<<" no";
     }
    }

    }
    myCom = new Posix_QextSerialPort(portName, QextSerialBase::Polling);
   if (myCom->open(QIODevice::ReadWrite))
        {
            myCom->flush();
            if(type==1)//9600
            {
                myCom->setBaudRate((BaudRateType)12);
                myCom->setDataBits((DataBitsType)3);
                myCom->setParity((ParityType)0);
                myCom->setStopBits((StopBitsType)0);
                myCom->setFlowControl(FLOW_OFF);
                myCom->setTimeout(50);
            }
            if(type==2)//115200
            {
                myCom->setBaudRate((BaudRateType)19);
                myCom->setDataBits((DataBitsType)3);
                myCom->setParity((ParityType)0);
                myCom->setStopBits((StopBitsType)0);
                myCom->setFlowControl(FLOW_OFF);
                myCom->setTimeout(50);
            }

            qDebug()<<"open com is ok!";

        }
        else
        {
            qDebug()<<"open com failse";
            return;
        }


}
bool newqserialport::openMycom(int com)
{
   QString portName="/dev/ttyACM2";//

   if(com==2)
   {
   QString comName="/dev/ttyACM";
   QString portName="/dev/ttyACM2";//
   for(int i=100;i>=2;i--)
   {
    QString tempcom=comName+QString::number(i);
    QFileInfo info(tempcom);
    if(info.exists())
    {
        i=0;
        portName=tempcom;
        number_dev=i;
        qDebug()<<tempcom<<" has";
    }
    else
    {
        qDebug()<<tempcom<<" no";
    }
   }
   }

    myCom = new Posix_QextSerialPort(portName, QextSerialBase::Polling);
   if (myCom->open(QIODevice::ReadWrite))
        {
            myCom->flush();

                myCom->setBaudRate((BaudRateType)19);
                myCom->setDataBits((DataBitsType)3);
                myCom->setParity((ParityType)0);
                myCom->setStopBits((StopBitsType)0);
                myCom->setFlowControl(FLOW_OFF);
                myCom->setTimeout(50);

            qDebug()<<"open com is ok!";
            return true;

        }
        else
        {
            qDebug()<<"open com failse";
            return false;
        }


}

bool newqserialport::openMycom()
{
   QString comName="/dev/ttyACM";
   QString portName="/dev/ttyACM2";//
   for(int i=number_dev;i>=2;i--)
   {
    QString tempcom=comName+QString::number(i);
    QFileInfo info(tempcom);
    if(info.exists())
    {
        i=0;
        portName=tempcom;
        number_dev=i-1;
        qDebug()<<tempcom<<" has";
    }
    else
    {
        qDebug()<<tempcom<<" no";
    }
   }


    myCom = new Posix_QextSerialPort(portName, QextSerialBase::Polling);
   if (myCom->open(QIODevice::ReadWrite))
        {
            myCom->flush();

                myCom->setBaudRate((BaudRateType)19);
                myCom->setDataBits((DataBitsType)3);
                myCom->setParity((ParityType)0);
                myCom->setStopBits((StopBitsType)0);
                myCom->setFlowControl(FLOW_OFF);
                myCom->setTimeout(50);
            qDebug()<<"open com is ok!";
            return true;

        }
        else
        {
            qDebug()<<"open com failse";
            return false;
        }


}
void newqserialport::closeMycom()
{
    myCom->close();
    qDebug()<<"close mycom";
}
bool newqserialport::comisopen()//判断串口是否打开成功
{
    return myCom->isOpen();
}
