#ifndef SOUNDPLAYERTHREAD_H
#define SOUNDPLAYERTHREAD_H

#include <QThread>
#include<QProcess>
#include"config.h"
#include"QVariant"
#include<QMutex>
class soundPlayerThread : public QThread
{
    Q_OBJECT
public:
    explicit soundPlayerThread(QObject *parent = 0);
    void player(QString );//播放声音
signals:

public slots:
    void playeSound(int );
private:
    QProcess process;
    config conf;
    bool soundFlag;//语音开关

    QMutex mutex;


};

#endif // SOUNDPLAYERTHREAD_H
