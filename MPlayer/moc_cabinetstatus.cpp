/****************************************************************************
** Meta object code from reading C++ file 'cabinetstatus.h'
**
** Created: Fri Nov 13 09:19:10 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "cabinetstatus.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cabinetstatus.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CabinetStatus[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   15,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      58,   51,   14,   14, 0x0a,
      74,   51,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CabinetStatus[] = {
    "CabinetStatus\0\0index,status\0"
    "sendDatas(int,QString)\0number\0"
    "getLockNum(int)\0sendCandtoSerial(int)\0"
};

void CabinetStatus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CabinetStatus *_t = static_cast<CabinetStatus *>(_o);
        switch (_id) {
        case 0: _t->sendDatas((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->getLockNum((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->sendCandtoSerial((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CabinetStatus::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CabinetStatus::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_CabinetStatus,
      qt_meta_data_CabinetStatus, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CabinetStatus::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CabinetStatus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CabinetStatus::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CabinetStatus))
        return static_cast<void*>(const_cast< CabinetStatus*>(this));
    return QThread::qt_metacast(_clname);
}

int CabinetStatus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void CabinetStatus::sendDatas(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
