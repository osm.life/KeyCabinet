#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>

class config : public QObject
{
    Q_OBJECT
public:
    explicit config(QObject *parent = 0);
    bool write(QString group,QString values,QVariant arg);
    QVariant read(QString group,QString values);
    bool writetemp(QString group,QString values,QVariant arg);
    QVariant readtemp(QString group,QString values);
signals:

public slots:

};

#endif // CONFIG_H
