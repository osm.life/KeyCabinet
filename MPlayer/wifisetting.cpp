#include "wifisetting.h"
#include "ui_wifisetting.h"
#include<QVBoxLayout>
#include<QFile>
#include<QDebug>
#include<QMessageBox>
#include<QTextStream>
#include<QTextCodec>
#include<QTimer>
wifisetting::wifisetting(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::wifisetting)
{
    ui->setupUi(this);        
    scanwifi();
//    QTimer *timer=new QTimer(this);
//    connect(timer,SIGNAL(timeout()),this,SLOT(updatewifi()));
//    timer->start(1000);
    setWindowFlags(Qt::FramelessWindowHint);
    connect(this,SIGNAL(sendEssid(QString)),&wifi,SLOT(getEssidName(QString)));
}

wifisetting::~wifisetting()
{
    delete ui;
}

void wifisetting::on_cancelBtn_clicked()
{
    this->close();
}


void wifisetting::on_connectBtn_clicked()
{

    QMessageBox messageBox;
    messageBox.setText(tr("连接成功！"));
    messageBox.exec();
    this->close();
}
void wifisetting::scanwifi()
{

   system("scan-wifi >./wifitemp");
   QVBoxLayout *groupBoxLayout;
   groupBoxLayout = new QVBoxLayout;
    QString fileName;
    fileName = "./wifitemp";
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"!";
        return;
    }
    QTextStream in(&file);
    QString line = in.readLine();
  //  QTextCodec *codec=QTextCodec::codecForName("utf-8");

    while (!line.isEmpty())
    {

        int index=line.indexOf("%");
        qDebug()<<"index %:"<<index;
        if(index>=0)
        {
        line=line.mid(index+1,line.size());
        }

        line=line.replace("(Security)","");//替换字符串
        line= line.simplified();//去掉空字符
        //line=line.replace("\"","");//去掉双引号
        //line=codec->fromUnicode(line);
        wifiNamelist<<line;
        line = in.readLine();
    }
    for(int i=0;i<wifiNamelist.size();i++)
    {
        qDebug()<<"name"<<wifiNamelist.at(i);
    }

    ui->listWidget->addItems(wifiNamelist);
    for(int i=0;i<wifiNamelist.size();i++)
    {
        qDebug()<<"hello";
      ui->listWidget->item(i)->setIcon(QIcon(QPixmap("/home/luo/Pictures/wifi.png")));
    }

}

void wifisetting::on_closeBtn_clicked()
{
    close();
}
//void wifisetting::updatewifi()
//{

//    for(int i=0;i<wifiNamelist.size();i++)
//    {

//    delete ui->listWidget->takeItem(i);
//    }
//            wifiNamelist.clear();
//    scanwifi();
//}

void wifisetting::on_updateBtn_clicked()
{
     scanwifi();
}

void wifisetting::on_listWidget_clicked(const QModelIndex &index)
{
    qDebug()<<index.data().toString();
    emit sendEssid(index.data().toString());
    wifi.show();
}
