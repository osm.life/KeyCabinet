#ifndef NETWORKTEST_H
#define NETWORKTEST_H

#include <QDialog>
#include"atcmd.h"
#include"newqserialport.h"
#include<QDebug>
#include<QTimer>

namespace Ui {
    class networkTest;
}

class networkTest : public QDialog
{
    Q_OBJECT

public:
    explicit networkTest(QWidget *parent = 0);
    ~networkTest();
     bool initNetwork();

public slots:

    int stepone();
    bool steptwo();
    bool stepthree();
    bool stepfour();
    bool stepfive();
    bool stepsix();
    bool stepseven();

    int netSignal();
    void getnetworkSignalSize();//获取网络类型
    void getSinal();//获取网络信号强度

    bool getAddr();
    bool getDns();

    bool getNetworkStatus();

    bool rebootDev();//重启设备

    void updateDev();//重新打开设备

private slots:


private:

    atcmd cmd;
    newqserialport port;
    int networktype;//2，3，4g网络类型
    int NetworksignalsSize;//网络信号强度
    QString ip;


    QTimer timer_network;//获取网络
};

#endif // NETWORKTEST_H
