#include "updatefilethread.h"
//网络更新文件的线程
#include<QDebug>
updateFileThread::updateFileThread(QObject *parent) :
    QThread(parent)
{
    filehttp=new HttpWindow;
    connect(filehttp,SIGNAL(downloadok(int)),this,SLOT(getfinished(int)));
}
void updateFileThread::getfineshed(int ok)
{
    qDebug()<<"getfineshed() "<<ok;
    emit sendfinished(ok);
}
void updateFileThread::getFileName(QString url)
{
    url=url.simplified();
    filehttp->downloadFile(url);
}
