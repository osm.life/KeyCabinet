#include "pictureplayer.h"
#include "ui_pictureplayer.h"
#include<QMatrix>
#include<QDebug>
#include<QFileSystemWatcher>
#include<QDir>
#include<QPropertyAnimation>

#include<math.h>
picturePlayer::picturePlayer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::picturePlayer)
{
    ui->setupUi(this);
    pix=new QPixmap;
    timer=new QTimer(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    connect(timer,SIGNAL(timeout()),this,SLOT(changePicture()));//定时器溢出关联changePicture()函数

    this->setCursor(Qt::BlankCursor);//隐藏鼠标光标
    firstPicture.setScaledContents(true);
    secondPicture.setScaledContents(true);
    count=0;
    pictureCount=0;

    picPath=QDir::currentPath()+"/picture/";
    directory.setPath("./picture/");
    files = directory.entryList(QDir::AllEntries,QDir::Time);
    files.removeAt(0);
    files.removeAt(0);
    for(int i=0;i<files.size();i++)
    {
    //qDebug()<<files.at(i);
    }
    secondPicture.show();
    secondPicture.setParent(this);

    firstPicture.setParent(this);
    firstPicture.show();
    changePicture();


}
void picturePlayer::pictures(QString)
{

}

picturePlayer::~picturePlayer()
{
    delete ui;
}

void picturePlayer::changePicture()//使用switch语句根据情况选择不同的图片
{
    files = directory.entryList(QDir::AllEntries,QDir::Time);
    files.removeAt(0);
    files.removeAt(0);
    if(pictureCount!=files.size())
    {
      pictureCount=files.size();
    }

    secondPicture.setGeometry(0,0,1024,600);
    firstPicture.setGeometry(0,0,1024,600);
    QString second;
    if(count<pictureCount-1)
    {
        count++;
    }
    else
    {
        count=0;
    }
    qDebug()<<count<<"   "<<files.size();
    QString ok;
    if(files.size()==0)
    {

    }
    else
    {
    if(count==(files.size()-1))
    {
     second=picPath+files.at(0);
    }
    else
    {
     second=picPath+files.at(count+1);
    }

    ok=picPath+files.at(count);

    }

  // int num= rand()%40;

  // qDebug()<<"随机数："<<num;
    secondPicture.setPixmap(second);
    firstPicture.setPixmap(QPixmap(ok));
    QPropertyAnimation *animation = new QPropertyAnimation(&firstPicture, "geometry");
    QPropertyAnimation *animation1 = new QPropertyAnimation(&secondPicture, "geometry");
    animation->setDuration(3000);
    animation->setStartValue(QRect(0, 0,1024,600));
    animation->setEndValue(QRect(-1024,0,1024,600));
    animation->setEasingCurve(QEasingCurve::InOutQuad);
    animation->start();

    animation1->setDuration(3000);
    animation1->setStartValue(QRect(1024,0,1024,600));
    animation1->setEndValue(QRect(0,0,1024,600));
    animation1->setEasingCurve(QEasingCurve::InOutQuad);
    animation1->start();

}
void picturePlayer::mousePressEvent(QMouseEvent *event)
{
    this->hide();
    timer->stop();
}
void picturePlayer::showEvent(QShowEvent *)
{
    timer->start(10000);
    timer->start();
    this->setCursor(Qt::BlankCursor);//隐藏鼠标光标
}
void picturePlayer::closeEvent(QCloseEvent *)
{

}
void picturePlayer::mouseMoveEvent(QMouseEvent *event)
{

}
void picturePlayer::keyPressEvent(QKeyEvent *e1)
{
qDebug()<<"字符1："<<e1->text();
    if(e1->key()!=Qt::Key_Escape)
    {
    if(e1->key()==Qt::Key_E)//获取第一个字符
    {
        qDebug()<<"二维码是："<<codeString;
        if(codeString.size()>5)
        {
           emit siganalCode(1,codeString);//发送给线程确认
           this->hide();
           timer->stop();
        }
        firstString.clear();
        endString.clear();
        codeString.clear();
    }

    if(!firstString.isEmpty()&&endString.isEmpty())//
    {
        codeString=codeString+e1->text();
    }
    if((e1->key()==Qt::Key_A)||(e1->key()==Qt::Key_F))//获取第一个字符
    {
        firstString=e1->text();
        codeString.clear();
        endString.clear();
    }
    else
    {
        if(firstString.isEmpty())
        {
        this->hide();
        timer->stop();
        }
    }
  }

}


