/********************************************************************************
** Form generated from reading UI file 'loginmanager.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINMANAGER_H
#define UI_LOGINMANAGER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_loginManager
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QLabel *label_3;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLineEdit *password;
    QPushButton *ok_btn;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QDialog *loginManager)
    {
        if (loginManager->objectName().isEmpty())
            loginManager->setObjectName(QString::fromUtf8("loginManager"));
        loginManager->resize(1024, 600);
        QFont font;
        font.setPointSize(21);
        loginManager->setFont(font);
        verticalLayout = new QVBoxLayout(loginManager);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 171, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        label_3 = new QLabel(loginManager);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        label_2 = new QLabel(loginManager);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setPointSize(16);
        label_2->setFont(font1);
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        password = new QLineEdit(loginManager);
        password->setObjectName(QString::fromUtf8("password"));
        QFont font2;
        font2.setPointSize(18);
        password->setFont(font2);
        password->setMaxLength(20);
        password->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(password);

        ok_btn = new QPushButton(loginManager);
        ok_btn->setObjectName(QString::fromUtf8("ok_btn"));

        horizontalLayout->addWidget(ok_btn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(17, 236, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        retranslateUi(loginManager);

        QMetaObject::connectSlotsByName(loginManager);
    } // setupUi

    void retranslateUi(QDialog *loginManager)
    {
        loginManager->setWindowTitle(QApplication::translate("loginManager", "Dialog", 0, QApplication::UnicodeUTF8));
        label_3->setText(QString());
        label_2->setText(QString());
        ok_btn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class loginManager: public Ui_loginManager {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINMANAGER_H
