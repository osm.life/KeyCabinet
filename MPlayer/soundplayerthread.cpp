#include "soundplayerthread.h"
//播放声音线程
#include<QDebug>
soundPlayerThread::soundPlayerThread(QObject *parent) :
    QThread(parent)
{
    int flag=conf.read("Voice","voice").toInt();
    qDebug()<<"语音开关："<<flag;
    if(flag==0)
        soundFlag=true;
    else
        soundFlag=false;
}
void soundPlayerThread::playeSound(int index)//播放声音
{
    qDebug()<<"thread running";
    if(soundFlag)//
    {
    switch(index)
    {
    case 0:
        player("0");
        break;
    case 1:
        player("1");
        break;
    case 2:
        player("2");
        break;
    case 3:
        player("3");
        break;
    case 4:
        player("4");
        break;
    case 5:
        player("5");
        break;
    case 6:
        player("6");
        break;
    case 7:
        player("7");
        break;
    case 8:
        player("8");
        break;
    case 9:
        player("9");
        break;
    case 10:
        player("10");
        break;
    case 11:
        player("11");
        break;
    case 12:
        player("12");
        break;
    case 13:
        player("13");
        break;
    case 21:
        player("21");
        break;
    case 22:
        player("22");
        break;
    case 31:
        player("31");
        break;
    case 32:
        player("32");//刷二维码成功
        break;
    case 33:
        player("33");//刷卡成功
        break;
    case 35:
        player("35");//请将此钥匙归还到预定站点
        break;
    default:
        break;
    }
    }
}
void soundPlayerThread::player(QString filename)//播放语音文件
{
   QString cmd;
   if(filename.toInt()<61)
   {
   cmd="mplayer  /usr/local/mystart/voice/"+filename+".wav";
   }
   else
       cmd="mplayer  /usr/local/mystart/voice/"+filename+".mp3";
    qDebug()<<cmd;
    mutex.lock();
    process.start(cmd);
    mutex.unlock();

}
