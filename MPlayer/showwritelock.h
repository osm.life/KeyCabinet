#ifndef SHOWWRITELOCK_H
#define SHOWWRITELOCK_H

#include <QDialog>

namespace Ui {
    class showWriteLock;
}

class showWriteLock : public QDialog
{
    Q_OBJECT

public:
    explicit showWriteLock(QWidget *parent = 0);
    ~showWriteLock();

private slots:
    void on_cancelBtn_clicked();
    void on_okBtn_clicked();
    void  getLockNo(int,int);//get locknumber;
    QString getInfor(QString);
private:
    Ui::showWriteLock *ui;
    int locknumber;//锁号
    int typeflag;

};

#endif // SHOWWRITELOCK_H
