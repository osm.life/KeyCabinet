#ifndef IHTTPDOWNLOADS_H
#define IHTTPDOWNLOADS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QFile>
#include <QNetworkReply>
#include <QTimer>
#include <QProgressBar>

class iHttpDownloadS : public QObject
{
Q_OBJECT
public:
    explicit iHttpDownloadS(QObject *parent = 0, QProgressBar *_progressBar = 0);
    ~iHttpDownloadS();
    bool    getFileFromURL(const QUrl &url, const QString &filePath); /* get file from url which we need to download, and restore to filePath */

    const QString &getLastErrorMessage(); /* if error occurs, use this to get the error message */
    void setErrorMessage(const QString &msg); /* set _errMsg */

signals:

public slots:
    void replyFinished(QNetworkReply*); /* download finished */
    void replayDownloadProgress(qint64, qint64); /* downloading... */
    void slotError(QNetworkReply::NetworkError); /* handle error */
    void slotReadyRead();/* ready read */
    void handleTimeOut(); /* handle time out */

private:
    QNetworkAccessManager    *_downloadManager;
    QNetworkReply            *_reply;
    QString _errMsg;
    QFile    _file;
    QProgressBar *_progressBar;
    QTimer        *_timeOut;
    QString _filePath;
    QUrl    _url;
};

#endif // IHTTPDOWNLOADS_H
