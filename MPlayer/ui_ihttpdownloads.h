/********************************************************************************
** Form generated from reading UI file 'ihttpdownloads.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IHTTPDOWNLOADS_H
#define UI_IHTTPDOWNLOADS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_iHttpDownloadS
{
public:

    void setupUi(QWidget *iHttpDownloadS)
    {
        if (iHttpDownloadS->objectName().isEmpty())
            iHttpDownloadS->setObjectName(QString::fromUtf8("iHttpDownloadS"));
        iHttpDownloadS->resize(400, 300);

        retranslateUi(iHttpDownloadS);

        QMetaObject::connectSlotsByName(iHttpDownloadS);
    } // setupUi

    void retranslateUi(QWidget *iHttpDownloadS)
    {
        iHttpDownloadS->setWindowTitle(QApplication::translate("iHttpDownloadS", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class iHttpDownloadS: public Ui_iHttpDownloadS {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IHTTPDOWNLOADS_H
