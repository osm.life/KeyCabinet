#ifndef ADVERTISERMENTTEXT_H
#define ADVERTISERMENTTEXT_H

#include <QWidget>

namespace Ui {
    class advertisermentText;
}

class advertisermentText : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText)


public:
    void setText(const QString &newText);
    QString text() const { return myText; }
    QSize sizeHint() const;
    void setSpeed(const int timer = 50);
    void setcolor(QColor);
    explicit advertisermentText(QWidget *parent = 0);
    ~advertisermentText();
protected:
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *event);
    void showEvent(QShowEvent *event);
    void hideEvent(QHideEvent *event);
    private:
    Ui::advertisermentText *ui;
    QString myText;
    int offset;
    int myTimerId;
private slots:
    void updatetext(QString);
    void updatpath(QString);
};

#endif // ADVERTISERMENTTEXT_H
