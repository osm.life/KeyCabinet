/********************************************************************************
** Form generated from reading UI file 'showwritelock.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWWRITELOCK_H
#define UI_SHOWWRITELOCK_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_showWriteLock
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QPushButton *cancelBtn;
    QPushButton *okBtn;
    QLabel *tipUser;

    void setupUi(QDialog *showWriteLock)
    {
        if (showWriteLock->objectName().isEmpty())
            showWriteLock->setObjectName(QString::fromUtf8("showWriteLock"));
        showWriteLock->resize(400, 300);
        QFont font;
        font.setPointSize(15);
        showWriteLock->setFont(font);
        verticalLayout = new QVBoxLayout(showWriteLock);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        cancelBtn = new QPushButton(showWriteLock);
        cancelBtn->setObjectName(QString::fromUtf8("cancelBtn"));
        cancelBtn->setMinimumSize(QSize(0, 50));

        gridLayout->addWidget(cancelBtn, 1, 0, 1, 1);

        okBtn = new QPushButton(showWriteLock);
        okBtn->setObjectName(QString::fromUtf8("okBtn"));
        okBtn->setMinimumSize(QSize(0, 50));

        gridLayout->addWidget(okBtn, 1, 1, 1, 1);

        tipUser = new QLabel(showWriteLock);
        tipUser->setObjectName(QString::fromUtf8("tipUser"));
        tipUser->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(tipUser, 0, 0, 1, 2);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(showWriteLock);

        QMetaObject::connectSlotsByName(showWriteLock);
    } // setupUi

    void retranslateUi(QDialog *showWriteLock)
    {
        showWriteLock->setWindowTitle(QApplication::translate("showWriteLock", "Dialog", 0, QApplication::UnicodeUTF8));
        cancelBtn->setText(QApplication::translate("showWriteLock", "\345\217\226\346\266\210", 0, QApplication::UnicodeUTF8));
        okBtn->setText(QApplication::translate("showWriteLock", "\345\206\231\345\205\245", 0, QApplication::UnicodeUTF8));
        tipUser->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class showWriteLock: public Ui_showWriteLock {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWWRITELOCK_H
