#include "atcmd.h"

atcmd::atcmd(QObject *parent) :
    QObject(parent)
{
    cmd_csq="at+csq";
    cmd_cpin="at+cpin?";
    cmd_cops="at+cops?";

    cmd_cgdcont="at+cgdcont?";
    cmd_cgact="at+cgact=0,1";
    cmd_cgdcont_1="at+cgdcont=1,\"ip\",\"cmnet\"";//移动
    cmd_cgdcont_2="at+cgdcont=1,\"ip\",\"3gnet\"";//联通
    cmd_cgact_1="at+cgact=1,1";
    cmd_xdns="at+xdns=1,1";
    cmd_xdatachannel="at+xdatachannel=1,1,\"/USBCDC/2\",\"/USBHS/NCM/0\",0";
    cmd_cgdata="at+cgdata=\"M-RAW_IP\",1";

    cmd_xcesq="at+xcesq?";//查询信号

    cmd_xdns_n="at+xdns?";
    cmd_cgpaddr="at+cgpaddr=1";

    cmd_reboot="at+cfun=15";
   // AT+XREG?注册
}
