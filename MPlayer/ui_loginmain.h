/********************************************************************************
** Form generated from reading UI file 'loginmain.ui'
**
** Created: Thu Jul 9 15:13:08 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINMAIN_H
#define UI_LOGINMAIN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_loginMain
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTabWidget *tabWidget;
    QWidget *tab;
    QWidget *tab_2;

    void setupUi(QDialog *loginMain)
    {
        if (loginMain->objectName().isEmpty())
            loginMain->setObjectName(QString::fromUtf8("loginMain"));
        loginMain->resize(543, 467);
        verticalLayout = new QVBoxLayout(loginMain);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(loginMain);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        tabWidget = new QTabWidget(loginMain);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);


        retranslateUi(loginMain);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(loginMain);
    } // setupUi

    void retranslateUi(QDialog *loginMain)
    {
        loginMain->setWindowTitle(QApplication::translate("loginMain", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("loginMain", "Tab 1", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("loginMain", "Tab 2", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class loginMain: public Ui_loginMain {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINMAIN_H
