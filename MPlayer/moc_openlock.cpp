/****************************************************************************
** Meta object code from reading C++ file 'openlock.h'
**
** Created: Fri Nov 13 09:19:09 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "openlock.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'openlock.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Openlock[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      26,   10,    9,    9, 0x05,
      48,   10,    9,    9, 0x05,
      78,   75,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
     117,    9,    9,    9, 0x0a,
     132,    9,    9,    9, 0x0a,
     148,    9,    9,    9, 0x0a,
     167,  165,    9,    9, 0x0a,
     188,    9,    9,    9, 0x0a,
     206,  165,    9,    9, 0x0a,
     228,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Openlock[] = {
    "Openlock\0\0index,flaginfor\0"
    "senddata(int,QString)\0sendcloseData(int,QString)\0"
    ",,\0sendManagerOp(QString,QString,QString)\0"
    "getlockNo(int)\0getcloseNo(int)\0"
    "openOneLock(int)\0,\0openOneLock(int,int)\0"
    "closeoneLock(int)\0closeoneLock(int,int)\0"
    "getWarnCmd(int)\0"
};

void Openlock::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Openlock *_t = static_cast<Openlock *>(_o);
        switch (_id) {
        case 0: _t->senddata((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->sendcloseData((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->sendManagerOp((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        case 3: _t->getlockNo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->getcloseNo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->openOneLock((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->openOneLock((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->closeoneLock((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->closeoneLock((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: _t->getWarnCmd((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Openlock::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Openlock::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_Openlock,
      qt_meta_data_Openlock, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Openlock::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Openlock::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Openlock::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Openlock))
        return static_cast<void*>(const_cast< Openlock*>(this));
    return QThread::qt_metacast(_clname);
}

int Openlock::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void Openlock::senddata(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Openlock::sendcloseData(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Openlock::sendManagerOp(QString _t1, QString _t2, QString _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
