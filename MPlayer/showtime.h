#ifndef SHOWTIME_H
#define SHOWTIME_H

#include <QDialog>
#include<QTimer>
namespace Ui {
    class showTime;
}

class showTime : public QDialog
{
    Q_OBJECT

public:
    explicit showTime(QWidget *parent = 0);
    ~showTime();
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *);

private:
    Ui::showTime *ui;
    int timerCout;
    QTimer timer;
private slots:
    void getInfor(QString);
    void getTime(int);
    void willclose();
};

#endif // SHOWTIME_H
