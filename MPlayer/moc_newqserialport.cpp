/****************************************************************************
** Meta object code from reading C++ file 'newqserialport.h'
**
** Created: Fri Nov 13 09:19:08 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "newqserialport.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'newqserialport.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_newqserialport[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   15,   16,   15, 0x0a,
      36,   15,   16,   15, 0x0a,
      51,   15,   15,   15, 0x0a,
      71,   15,   15,   15, 0x0a,
      91,   89,   15,   15, 0x0a,
     113,   89,   15,   15, 0x0a,
     137,   15,  132,   15, 0x0a,
     152,   15,  132,   15, 0x0a,
     164,   15,   15,   15, 0x0a,
     185,   15,   15,   15, 0x0a,
     198,   15,   16,   15, 0x0a,
     211,   15,   16,   15, 0x0a,
     227,   15,  132,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_newqserialport[] = {
    "newqserialport\0\0QString\0ReadMyCom()\0"
    "ReadMyCom(int)\0WriteMyCom(QString)\0"
    "WriteMyCom(char*)\0,\0WriteMyCom(char*,int)\0"
    "openMycom(int,int)\0bool\0openMycom(int)\0"
    "openMycom()\0WriteSmsCom(QString)\0"
    "closeMycom()\0ReadSmsCom()\0ReadSmsCom(int)\0"
    "comisopen()\0"
};

void newqserialport::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        newqserialport *_t = static_cast<newqserialport *>(_o);
        switch (_id) {
        case 0: { QString _r = _t->ReadMyCom();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 1: { QString _r = _t->ReadMyCom((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 2: _t->WriteMyCom((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->WriteMyCom((*reinterpret_cast< char*(*)>(_a[1]))); break;
        case 4: _t->WriteMyCom((*reinterpret_cast< char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->openMycom((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: { bool _r = _t->openMycom((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->openMycom();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: _t->WriteSmsCom((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->closeMycom(); break;
        case 10: { QString _r = _t->ReadSmsCom();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 11: { QString _r = _t->ReadSmsCom((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->comisopen();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData newqserialport::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject newqserialport::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_newqserialport,
      qt_meta_data_newqserialport, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &newqserialport::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *newqserialport::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *newqserialport::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_newqserialport))
        return static_cast<void*>(const_cast< newqserialport*>(this));
    return QObject::qt_metacast(_clname);
}

int newqserialport::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
