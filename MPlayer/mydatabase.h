#ifndef MYDATABASE_H
#define MYDATABASE_H

#include <QObject>
#include<QtSql/QSqlDatabase>
#include<QtSql/QSqlQuery>
#include<QtSql/QSqlError>
#include"config.h"

class mydatabase : public QObject
{
    Q_OBJECT
public:
    explicit mydatabase(QObject *parent = 0);
    bool openDatabase();
    bool openDatabase(int);
    bool closeDatabase();
    bool initDatabase();
    bool writeData(QString,QString,QString,QString,QString);
    bool writeData(QString,QString,QString);
    bool readData();
    bool updateuserData();
    bool deletData(int );
    bool deletData(QString);//按抽屉号删除
    bool deletBorrow(int );
    bool deletBorrow(QString );
    bool wirteDateManagerOp(QString,QString,QString);//管理员操作,1抽屉，2，钥匙，3状态
    bool deleteMangerOp(int);//删除管理员操作



signals:

public slots:
private:
     QSqlDatabase *database;
     QSqlDatabase *db2;
     bool linkNameFlag;
     config conf;
     int codetime;
};

#endif // MYDATABASE_H
