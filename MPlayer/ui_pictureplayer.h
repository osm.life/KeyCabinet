/********************************************************************************
** Form generated from reading UI file 'pictureplayer.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PICTUREPLAYER_H
#define UI_PICTUREPLAYER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_picturePlayer
{
public:

    void setupUi(QWidget *picturePlayer)
    {
        if (picturePlayer->objectName().isEmpty())
            picturePlayer->setObjectName(QString::fromUtf8("picturePlayer"));
        picturePlayer->resize(1024, 600);

        retranslateUi(picturePlayer);

        QMetaObject::connectSlotsByName(picturePlayer);
    } // setupUi

    void retranslateUi(QWidget *picturePlayer)
    {
        picturePlayer->setWindowTitle(QApplication::translate("picturePlayer", "picturePlayer", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class picturePlayer: public Ui_picturePlayer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PICTUREPLAYER_H
