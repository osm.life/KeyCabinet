#include "mydatabase.h"
#include<QDebug>
#include<QDateTime>
#include<QStringList>
mydatabase::mydatabase(QObject *parent) :
    QObject(parent)
{
    codetime=conf.read("TimeCode","timecode").toInt();
    if(codetime==0)
    {
    codetime=10;
    }
    if(codetime>360)
    {
     codetime=10;
    }
}
bool mydatabase::openDatabase()
{
    linkNameFlag=false;
    database =  new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    database->setDatabaseName("database.db");
    qDebug()<<"数据库的连接名："<< database->connectionName();
    if(database->open())
    {
        qDebug()<<"数据库打开ok";
        initDatabase();
        return true;
    }
    else
    {
        qDebug()<<"打开数据库失败";
        return false;
    }

}
bool mydatabase::openDatabase(int i)
{
    linkNameFlag=true;
    db2 = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE","db2"));
    db2->setDatabaseName("database.db");
    qDebug()<<"数据库的连接名："<<db2->connectionName();
    db2->setDatabaseName("database.db");
    if(db2->open())
    {
        qDebug()<<"数据库打开ok";
        return true;
    }
    else
    {
        qDebug()<<"打开数据库失败";
        return false;
    }

}

bool mydatabase::closeDatabase()
{
    database->close();
    db2->close();
}

bool mydatabase::initDatabase()
{
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        bool ok = query.exec("CREATE TABLE IF NOT EXISTS userdata(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                             "time VARCHAR(40), "
                             "cmd VARCHAR(40), "
                             "code VARCHAR(40), "
                             "cardid VARCHAR(40),"
                             "drawno VARCHAR(20))");
               if (ok)
               {
                   qDebug()<<"ceate table partition success/n";
               }
               else
               {
                   qDebug()<<"ceate table partition failed/n"<<query.lastError().text();
               }



            ok = query.exec("CREATE TABLE IF NOT EXISTS borrow(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                    "cardid VARCHAR(12), "
                                    "code VARCHAR(20),drawno VARCHAR(10))");
                      if (ok)
                      {

                          qDebug()<<"ceate table  borrow success/n";
                      }
                      else
                      {
                          qDebug()<<"ceate table  borrow failed/n"<<query.lastError().text();
                      }


             /*****************管理员操作日志数据************************/
       ok = query.exec("CREATE TABLE IF NOT EXISTS mangerdata(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                              "drawno INTEGER,"//抽屉号
                                              "key VARCHAR(40), "//钥匙号
                                              "idleflag INTEGER default 0) ");//处理标志1表示抽屉状态，2表示更换钥匙
           if (ok)
               {
               qDebug()<<"ceate table  mangerdata success/n";

               }
               else
               {
                qDebug()<<"ceate table  mangerdata failed/n"<<query.lastError().text();
               }

    }
    else
    {

     QSqlQuery query;
     bool ok = query.exec("CREATE TABLE IF NOT EXISTS userdata(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                          "time VARCHAR(40), "
                          "cmd VARCHAR(40), "
                          "code VARCHAR(40), "
                          "cardid VARCHAR(40),"
                          "drawno VARCHAR(20))");
            if (ok)
            {
                qDebug()<<"ceate table partition success/n";
            }
            else
            {
                qDebug()<<"ceate table partition failed/n"<<query.lastError().text();
            }



         ok = query.exec("CREATE TABLE IF NOT EXISTS borrow(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                 "cardid VARCHAR(12), "
                                 "code VARCHAR(20),drawno VARCHAR(10))");
                   if (ok)
                   {

                       qDebug()<<"ceate table  borrow success/n";
                   }
                   else
                   {
                       qDebug()<<"ceate table  borrow failed/n"<<query.lastError().text();
                   }


          /*****************管理员操作日志数据************************/
    ok = query.exec("CREATE TABLE IF NOT EXISTS mangerdata(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                           "drawno INTEGER,"//抽屉号
                                           "key VARCHAR(40), "//钥匙号
                                           "idleflag INTEGER default 0) ");//处理标志1表示抽屉状态，2表示更换钥匙
        if (ok)
            {
            qDebug()<<"ceate table  mangerdata success/n";

            }
            else
            {
             qDebug()<<"ceate table  mangerdata failed/n"<<query.lastError().text();
            }
    }

}
bool mydatabase::writeData(QString time, QString cmd, QString code, QString cardid,QString drawno)
{
    //openDatabase();
    if(linkNameFlag)
    {
        QSqlQuery sql_query(*db2);
    QString ifsql="delete from userdata where drawno='"+drawno+"'";
    qDebug()<<"ifsql:"<<ifsql;
    if(sql_query.exec(ifsql))
    {
        qDebug()<<"删除旧数据，添加新数据";
    }
    QString sql = "insert into userdata(time,cmd,code,cardid,drawno) values ('"+time+"','"+cmd+"','"+code+"','"+cardid+"','"+drawno+"')";
    if(!sql_query.exec(sql))
    {
    qDebug()<<"insert data err："<<sql_query.lastError().text();
    return false;
    }
    else
    {
    qDebug()<<"insert ok! sql userdata："<<sql;
    }
    return true;
    }
    else
    {
        QSqlQuery sql_query;
        QString ifsql="delete from userdata where drawno='"+drawno+"'";
        qDebug()<<"ifsql:"<<ifsql;
        if(sql_query.exec(ifsql))
        {
            qDebug()<<"删除旧数据，添加新数据";
        }
        QString sql = "insert into userdata(time,cmd,code,cardid,drawno) values ('"+time+"','"+cmd+"','"+code+"','"+cardid+"','"+drawno+"')";
        if(!sql_query.exec(sql))
        {
        qDebug()<<"insert data err："<<sql_query.lastError().text();
        return false;
        }
        else
        {
        qDebug()<<"insert ok! sql userdata："<<sql;
        }

        //readData();
       // closeDatabase();
        return true;
    }

}
bool mydatabase::writeData(QString code, QString cardid,QString drawno)
{

    if(linkNameFlag)
    {
        QSqlQuery sql_query(*db2);
        QString sql = "insert into borrow(code,cardid,drawno) values ('"+code+"','"+cardid+"','"+drawno+"')";
        if(!sql_query.exec(sql))
        {
        qDebug()<<"insert data err："<<sql_query.lastError().text();
        }
        else
        {
        qDebug()<<"insert data ok!!!!!";
        }

        qDebug()<<"sql borrow："<<sql;
    }
    else
    {
        QSqlQuery sql_query;
        QString sql = "insert into borrow(code,cardid,drawno) values ('"+code+"','"+cardid+"','"+drawno+"')";
        if(!sql_query.exec(sql))
        {
        qDebug()<<"insert data err："<<sql_query.lastError().text();
        }
        else
        {
        qDebug()<<"insert data ok!!!!!";
        }

    }


}

bool mydatabase::readData()
{
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        QString sql="select * from userdata";
        if(query.exec(sql))
        {
        while(query.next())
        {
            qDebug()<<"************************************";
            qDebug()<<"    id："<<query.value(0).toString();
            qDebug()<<"  time："<<query.value(1).toString();
            qDebug()<<"   cmd："<<query.value(2).toString();
            qDebug()<<"  code："<<query.value(3).toString();
            qDebug()<<"cardid："<<query.value(4).toString();
            qDebug()<<"drawno："<<query.value(5).toString();
            qDebug()<<"************************************";
        }
        }
    }
    else
    {
        QSqlQuery query;
        QString sql="select * from userdata";
        if(query.exec(sql))
        {
        while(query.next())
        {
            qDebug()<<"************************************";
            qDebug()<<"    id："<<query.value(0).toString();
            qDebug()<<"  time："<<query.value(1).toString();
            qDebug()<<"   cmd："<<query.value(2).toString();
            qDebug()<<"  code："<<query.value(3).toString();
            qDebug()<<"cardid："<<query.value(4).toString();
            qDebug()<<"drawno："<<query.value(5).toString();
            qDebug()<<"************************************";
        }
        }
    }

}

bool mydatabase::updateuserData()
{
    QString CurrentimeStr=QDateTime::currentDateTime().toString("yyyyMMhhmm");
    qDebug()<<"当前时间："<<CurrentimeStr;
    long currentime=CurrentimeStr.toLong();
    long cmdTime=0;
    QStringList drawnoList;//过期的抽屉号
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        QString sql="select * from userdata";
        if(query.exec(sql))
        {
        while(query.next())
        {
            qDebug()<<"************************************";
            qDebug()<<"    id："<<query.value(0).toString();
            qDebug()<<"  time："<<query.value(1).toString();
            qDebug()<<"   cmd："<<query.value(2).toString();
            qDebug()<<"  code："<<query.value(3).toString();
            qDebug()<<"cardid："<<query.value(4).toString();
            qDebug()<<"drawno："<<query.value(5).toString();
            qDebug()<<"************************************";
            cmdTime=query.value(1).toString().toLong();
            long timesize=currentime-cmdTime;
             qDebug()<<"时间差："<<timesize;
            if(timesize>=100)
            {
            drawnoList<<query.value(5).toString();
            }
            else
            {
                 if(timesize>codetime)
                  {
                  drawnoList<<query.value(5).toString();
                  }
                  else
                  {

                 }
            }
        }
        }
    }
    else
    {
        QSqlQuery query;
        QString sql="select * from userdata";
        if(query.exec(sql))
        {
        while(query.next())
        {
            qDebug()<<"************************************";
            qDebug()<<"    id："<<query.value(0).toString();
            qDebug()<<"  time："<<query.value(1).toString();
            qDebug()<<"   cmd："<<query.value(2).toString();
            qDebug()<<"  code："<<query.value(3).toString();
            qDebug()<<"cardid："<<query.value(4).toString();
            qDebug()<<"drawno："<<query.value(5).toString();
            qDebug()<<"************************************";
            cmdTime=query.value(1).toString().toLong();
            long timesize=currentime-cmdTime;
             qDebug()<<"时间差："<<timesize;
            if(timesize>=100)
            {
            drawnoList<<query.value(5).toString();
            }
            else
            {
                 if(timesize>codetime)
                  {
                  drawnoList<<query.value(5).toString();
                  }
                  else
                  {

                 }
            }
        }
        }
    }
    for(int i=0;i<drawnoList.size();i++)
    {
        qDebug()<<"条码超时："<<drawnoList.at(i);
        deletData(drawnoList.at(i));
    }
}

bool mydatabase::deletData(int index)
{
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        QString sql="delete from userdata where id="+QString::number(index);
        if(query.exec(sql))
        return true;
        else
            return false;
    }
    else
    {
     QSqlQuery query;
     QString sql="delete from userdata where id="+QString::number(index);
     if(query.exec(sql))
     return true;
     else
     return false;
    }

}
bool mydatabase::deletData(QString index)
{
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        QString sql="delete from userdata where drawno='"+index+"'";
        if(query.exec(sql))
        return true;
        else
            return false;
    }
    else
    {
        QSqlQuery query;
        QString sql="delete from userdata where drawno=\""+index+"\"";
        if(query.exec(sql))
        return true;
        else
            return false;
    }

}
bool mydatabase::deletBorrow(int index)//index 为抽屉号
{
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        QString sql="delete from borrow where drawno='"+QString::number(index)+"'";
        if(query.exec(sql))
        return true;
        else
            return false;
    }
    else
    {
        QSqlQuery query;
        QString sql="delete from borrow where drawno='"+QString::number(index)+"'";
        if(query.exec(sql))
        return true;
        else
            return false;
    }

}
bool mydatabase::deletBorrow(QString index)
{
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        QString sql="delete from borrow where cardid='"+index+"'";
        if(query.exec(sql))
        {
            qDebug()<<"delete borrow ok";
        return true;
        }
        else
        {
            qDebug()<<"delete borrow err";
            return false;
        }
    }
    else
    {
        QSqlQuery query;
        QString sql="delete from borrow where cardid='"+index+"'";
        if(query.exec(sql))
        return true;
        else
            return false;
    }

}
bool mydatabase::wirteDateManagerOp(QString drawno,QString key,QString flag)//管理员操作
{
    if(linkNameFlag)
    {
        QSqlQuery sql_query(*db2);
        QString sql = "insert into mangerdata(drawno,key,idleflag) values ("+drawno+",'"+key+"',"+flag+")";
        if(!sql_query.exec(sql))
        {
        qDebug()<<"insert into mangerdata:"<<sql_query.lastError().text();
        }
        else
        {
        qDebug()<<"insert mangerdata ok!!!!!";
        }
        qDebug()<<"sql mangerdata："<<sql;
    }
    else
    {
        QSqlQuery sql_query;
        QString sql = "insert into mangerdata(drawno,key,idleflag) values ("+drawno+",'"+key+"',"+flag+")";
        if(!sql_query.exec(sql))
        {
        qDebug()<<"insert into mangerdata:"<<sql_query.lastError().text();
        }
        else
        {
        qDebug()<<"insert mangerdata ok!!!!!";
        }
        qDebug()<<"sql mangerdata："<<sql;
    }




}
bool mydatabase::deleteMangerOp(int index)//删除管理员操作
{
    if(linkNameFlag)
    {
        QSqlQuery query(*db2);
        QString sql="delete from mangerdata where drawno="+QString::number(index);
        if(query.exec(sql))
        return true;
        else
            return false;
    }
    else
    {
        QSqlQuery query;
        QString sql="delete from mangerdata where drawno="+QString::number(index);
        if(query.exec(sql))
        return true;
        else
            return false;
    }

}

