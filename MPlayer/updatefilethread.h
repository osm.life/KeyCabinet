#ifndef UPDATEFILETHREAD_H
#define UPDATEFILETHREAD_H

#include <QThread>
#include"httpwindow.h"
class updateFileThread : public QThread
{
    Q_OBJECT
public:
    explicit updateFileThread(QObject *parent = 0);

signals:
    void sendfinished(bool);
public slots:

    void getfineshed(int);
    void getFileName(QString);
private:
    HttpWindow *filehttp;

};

#endif // UPDATEFILETHREAD_H
