#include "cabinetstatus.h"
#include<QDebug>
#include"config.h"
CabinetStatus::CabinetStatus(QObject *parent) :
    QThread(parent)
{
    stopflag=true;
    timeoutflag=0;
    lockNumber=1;
    config conf;
    number=conf.read("Cabinet","cabinetNumber").toInt();
    serial.openMycom(1,1);

}

void CabinetStatus::run()
{
    while(!stopflag)
    {
        sendCandtoSerial(lockNumber);
        msleep(300);
    }
}
void CabinetStatus::getLockNum(int number)
{
lockNumber=number;
}
void CabinetStatus::PauseThread()
{
    stopflag=true;
    lockNumber=1;
}
void CabinetStatus::StartRunThread()
{
    stopflag=false;
    lockNumber=1;
}
void CabinetStatus::sendCandtoSerial(int index)
{
    qDebug()<<"#########################start############################";
    QString inforr;//=serial.ReadMyCom(2000);
    QString statuscmd="00";
    serial.WriteMyCom(comman.readCardId(index+100));
    while(true)
    {
    inforr=serial.ReadMyCom(200);
    if(inforr.contains("bb"))break;
    else
    {
        timeoutflag++;
        if(timeoutflag>5)
        {
            timeoutflag=0;
            break;
        }
        if(timeoutflag==3)
        {
        serial.WriteMyCom(comman.readCardId(index+100));
        }
    }
    }

    if(inforr.size()>=18)
    {
        inforr=inforr.mid(inforr.lastIndexOf("bb"),18);
    }
    QString handcmd=inforr.mid(0,2);
    statuscmd=inforr.mid(14,2);
    if(handcmd=="bb")
    {
      if((statuscmd=="01")||(statuscmd=="02"))//
      {
      }
      else
           statuscmd="03";
    }
    else
        statuscmd="00";

    qDebug()<<"钥匙状态查看:"<<index<<"号   "<<statuscmd<<"    "<<inforr;

    emit sendDatas(index,statuscmd);

    if(lockNumber<number)
    {
    lockNumber=lockNumber+1;
    }
    else
    {
    lockNumber=1;
    }
qDebug()<<"#########################end############################";
}

void CabinetStatus::stopThread()
{
    stopflag=true;
}
