#include "showtime.h"
#include "ui_showtime.h"

showTime::showTime(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::showTime)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
    connect(&timer,SIGNAL(timeout()),this,SLOT(willclose()));


}
void showTime::willclose()
{
    timerCout--;
    if((timerCout>-2)&&(timerCout<2))
    {
    this->close();
    }
}

showTime::~showTime()
{
    delete ui;
}
void showTime::getInfor(QString tip)
{
    //ui->tipuser->setText(tip);
}
void showTime::getTime(int number)
{
    QString infor=QString::number(number);
    if(number<=9)
    {
    infor="抽屉倒计时(秒):0"+QString::number(number);
    }
    else
        infor="抽屉倒计时(秒):"+QString::number(number);
    ui->tipuser->setText(infor);
    if(number<=1)
        this->close();
}
void showTime::closeEvent(QCloseEvent *)
{
    timer.stop();
}
void showTime::showEvent(QShowEvent *)
{
    timerCout=30;
    timer.start(1000);
}
