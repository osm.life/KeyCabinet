#include "initnetwork.h"
#include<QVariant>
#include<QDebug>
#include<QProcess>
#include<QTime>


initNetwork::initNetwork(QObject *parent) :
    QObject(parent)
{
     switchflag=true;
}
void initNetwork::init()
{
    int mode=conf.read("Network","networkmode").toInt();
    if(mode==2)//wifi
    {
        QString wifiname=conf.read("Network","wifiname").toString();
        QString wifipassword=conf.read("Network","wifipassword").toString();
        QString command="start-wifi wpa "+wifiname+" "+wifipassword;
        system(command.toAscii());
    }
    else if(mode==1)//4G
    {
        gps.initNetwork();
    }
}
int initNetwork::getNetSignal()
{
    int num=gps.netSignal();
    return num;
}
bool initNetwork::getnetstatus()//获取联网状态
{
    return gps.getNetworkStatus();
}

void initNetwork::rebootdev()
{

      qDebug()<<"reboot";
      gps.rebootDev();
      connect(&opentimer,SIGNAL(timeout()),this,SLOT(opendev()));
      opentimer.start(18000);//18秒后重新读取
      switchflag=false;
}
void initNetwork::opendev()
{
    switchflag=true;
    qDebug()<<"renew open";
    gps.updateDev();
    opentimer.stop();
    disconnect(&opentimer,SIGNAL(timeout()),this,SLOT(opendev()));
    int mode=conf.read("Network","networkmode").toInt();
    if(mode==1)//4G
    {
        gps.initNetwork();
    }
}
