/********************************************************************************
** Form generated from reading UI file 'mplayer.ui'
**
** Created: Mon Nov 9 10:04:31 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MPLAYER_H
#define UI_MPLAYER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mplayer
{
public:
    QVBoxLayout *verticalLayout_2;
    QWidget *widget;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QSlider *horizontalSlider;
    QSpinBox *spinBox;
    QHBoxLayout *horizontalLayout;
    QPushButton *pause_btn;
    QPushButton *stop_btn;
    QPushButton *reward_btn;
    QPushButton *previous_btn;
    QPushButton *next_btn;
    QPushButton *forward_btn;
    QPushButton *sound_btn;

    void setupUi(QDialog *mplayer)
    {
        if (mplayer->objectName().isEmpty())
            mplayer->setObjectName(QString::fromUtf8("mplayer"));
        mplayer->resize(747, 546);
        verticalLayout_2 = new QVBoxLayout(mplayer);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        widget = new QWidget(mplayer);
        widget->setObjectName(QString::fromUtf8("widget"));

        verticalLayout_2->addWidget(widget);

        frame = new QFrame(mplayer);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMaximumSize(QSize(16777215, 130));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_3->addWidget(label);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(label_2);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSlider = new QSlider(frame);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider);

        spinBox = new QSpinBox(frame);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));

        horizontalLayout_2->addWidget(spinBox);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pause_btn = new QPushButton(frame);
        pause_btn->setObjectName(QString::fromUtf8("pause_btn"));

        horizontalLayout->addWidget(pause_btn);

        stop_btn = new QPushButton(frame);
        stop_btn->setObjectName(QString::fromUtf8("stop_btn"));

        horizontalLayout->addWidget(stop_btn);

        reward_btn = new QPushButton(frame);
        reward_btn->setObjectName(QString::fromUtf8("reward_btn"));

        horizontalLayout->addWidget(reward_btn);

        previous_btn = new QPushButton(frame);
        previous_btn->setObjectName(QString::fromUtf8("previous_btn"));

        horizontalLayout->addWidget(previous_btn);

        next_btn = new QPushButton(frame);
        next_btn->setObjectName(QString::fromUtf8("next_btn"));

        horizontalLayout->addWidget(next_btn);

        forward_btn = new QPushButton(frame);
        forward_btn->setObjectName(QString::fromUtf8("forward_btn"));

        horizontalLayout->addWidget(forward_btn);

        sound_btn = new QPushButton(frame);
        sound_btn->setObjectName(QString::fromUtf8("sound_btn"));

        horizontalLayout->addWidget(sound_btn);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addWidget(frame);


        retranslateUi(mplayer);

        QMetaObject::connectSlotsByName(mplayer);
    } // setupUi

    void retranslateUi(QDialog *mplayer)
    {
        mplayer->setWindowTitle(QApplication::translate("mplayer", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        label_3->setText(QString());
        label_2->setText(QString());
        pause_btn->setText(QApplication::translate("mplayer", "\346\222\255\346\224\276", 0, QApplication::UnicodeUTF8));
        stop_btn->setText(QApplication::translate("mplayer", "\345\201\234\346\255\242", 0, QApplication::UnicodeUTF8));
        reward_btn->setText(QApplication::translate("mplayer", "\345\277\253\351\200\200", 0, QApplication::UnicodeUTF8));
        previous_btn->setText(QApplication::translate("mplayer", "\344\270\212\344\270\200\351\246\226", 0, QApplication::UnicodeUTF8));
        next_btn->setText(QApplication::translate("mplayer", "\344\270\213\344\270\200\351\246\226", 0, QApplication::UnicodeUTF8));
        forward_btn->setText(QApplication::translate("mplayer", "\345\277\253\350\277\233", 0, QApplication::UnicodeUTF8));
        sound_btn->setText(QApplication::translate("mplayer", "\351\235\231\351\237\263", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class mplayer: public Ui_mplayer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MPLAYER_H
