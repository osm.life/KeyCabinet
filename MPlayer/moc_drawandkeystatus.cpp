/****************************************************************************
** Meta object code from reading C++ file 'drawandkeystatus.h'
**
** Created: Fri Nov 13 09:19:19 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "drawandkeystatus.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'drawandkeystatus.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DrawAndKeyStatus[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x05,
      42,   17,   17,   17, 0x05,
      65,   17,   17,   17, 0x05,
      83,   17,   17,   17, 0x05,

 // slots: signature, parameters, type, tag, flags
     109,   17,  104,   17, 0x0a,
     136,  129,  125,   17, 0x0a,
     165,   17,  157,   17, 0x0a,
     184,   17,   17,   17, 0x0a,
     198,   17,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DrawAndKeyStatus[] = {
    "DrawAndKeyStatus\0\0sendDrawstatus(QString)\0"
    "sendKeyStatus(QString)\0sendCode(QString)\0"
    "sendWarning(QString)\0bool\0readstatus(int)\0"
    "int\0inforr\0rebackInfor(QString)\0QString\0"
    "checkData(QString)\0pauseThread()\0"
    "startThread()\0"
};

void DrawAndKeyStatus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DrawAndKeyStatus *_t = static_cast<DrawAndKeyStatus *>(_o);
        switch (_id) {
        case 0: _t->sendDrawstatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->sendKeyStatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->sendCode((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->sendWarning((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: { bool _r = _t->readstatus((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { int _r = _t->rebackInfor((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 6: { QString _r = _t->checkData((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 7: _t->pauseThread(); break;
        case 8: _t->startThread(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DrawAndKeyStatus::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DrawAndKeyStatus::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_DrawAndKeyStatus,
      qt_meta_data_DrawAndKeyStatus, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DrawAndKeyStatus::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DrawAndKeyStatus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DrawAndKeyStatus::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DrawAndKeyStatus))
        return static_cast<void*>(const_cast< DrawAndKeyStatus*>(this));
    return QThread::qt_metacast(_clname);
}

int DrawAndKeyStatus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void DrawAndKeyStatus::sendDrawstatus(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DrawAndKeyStatus::sendKeyStatus(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DrawAndKeyStatus::sendCode(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void DrawAndKeyStatus::sendWarning(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
