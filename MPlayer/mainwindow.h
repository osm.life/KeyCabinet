#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include"menu.h"
#include"loginmanager.h"
#include"loginuser.h"
#include"showcode.h"
#include"receivedatafromserver.h"
#include"showtime.h"
#include<QStatusBar>
#include<menu.h>
#include"soundplayerthread.h"
#include<QDebug>
#include"config.h"
#include<QMessageBox>
#include<QMovie>
#include<QTimer>
#include"advertisermenttext.h"
#include"updatefilethread.h"
#include<QPushButton>
#include<QHostInfo>
#include"mydatabase.h"
#include<QProgressBar>
#include"wiftthread.h"
#include"pictureplayer.h"
#include"syslog.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    void mousePressEvent(QMouseEvent *);

private:
    Ui::MainWindow *ui;
    loginManager manger;
    loginUser user;

    QPushButton *btn_user;

    QString firstString;//获取开始字符
    QString endString;//获取结束字符
    QString codeString;//获取扫二维码的字符串的值
    QStringList aChar;//小写字母字符串列表
    QStringList AChar;//大写字母列表
    QStringList numChar;//数字列表

    QString handwriteCode;//手动输入密码；
    showCode codeshow;
    receiveDataFromServer *receiveThread;//接收数据线程
    showTime *showTipUser;//提示用户

    int changePictureTime;//图片自动切换
    soundPlayerThread *soundThread;
    updateFileThread *updatefile;

    int CancelKeyTime;
    int enterKeyTime;
    int otherkeyTime;

    QLabel newstatuflag;//服务器连接状态
    QLabel netsignal;//信号状态
    QLabel netdataflag;//网络数据状态

    bool isLoginManager;

    wiftThread *thread;
    config conf;


    picturePlayer *picPlayer;
    bool picPlayerFlag;
    int pictureTime;
    int pictureTimeS;//设定时间

    syslog log;




private slots:
    void getcodestatus(int,QString);//二维码提示函数
    QString keyToNumber(int);
    void  getCodeUser();//读取二维码的值
    void  getHandwriteCode(QString);//获取用户手动输入的值

    void setShowPicture(int,int);
    void getDrawAutocloseTime(int);//获取提示用户关闭抽屉时间
    void updateShow(QString cmd,QString status);//更新界面函数

    void getSoundPlay(int);//播放声音
    void getdownfinished();//获取文件下载完成
    void getupdateFile(QString);

    void startplay();

    void setUserlogin();

    void lookedUp(QHostInfo &host);

    void getManagerOp(int,QString);//管理员操作

    void setnetsignal(int);

    void setdatasignal(int index);

    void getcodefromPicture(int,QString);



signals:
    void senddata(QString);//发送数据信号
    void sendcode(int,QString);//arg1=1,直接扫描，arg=2,是手动输入
    void senduserinfor(QString);//提示用户消息
    void sendTipclose(QString);
    void sendTipUserCloseDrawtime(int);
    void sendremoteLogin();
    void sendPlaySound(int);//发送播放声音
    void sendUpdateFileName(QString);

    void sendManagerOp(int,QString);//发送管理员操作


};

#endif // MAINWINDOW_H
