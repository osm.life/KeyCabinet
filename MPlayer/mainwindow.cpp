#include "mainwindow.h"
#include "ui_mainwindow.h"
#include"menu.h"
#include"ihttpdownloads.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setWindowTitle("主界面");
    ui->label->setScaledContents(true);//自动适应图片大小
    ui->label->setPixmap(QPixmap(":/images/login.png"));

    user.setWindowModality(Qt::ApplicationModal);
    manger.setWindowModality(Qt::ApplicationModal);
    //this->setWindowFlags(Qt::WindowMaximizeButtonHint);

    showTipUser=new showTime(this);
    showTipUser->setHidden(true);
    showTipUser->setGeometry(QRect(380,390,300,60));
   // codeshow.setParent(this);
    codeshow.setGeometry(QRect(100,150,800,300));
    codeshow.setHidden(true);

    btn_user=new QPushButton(this);
    btn_user->setGeometry(QRect(270,330,100,50));
    connect(btn_user,SIGNAL(clicked()),&user,SLOT(show()));
    btn_user->setText("确认");
    btn_user->setStyleSheet("height:50px;width:100px;font-size:25px;");
    btn_user->show();
    changePictureTime=0;
    CancelKeyTime=0;
    enterKeyTime=0;
    otherkeyTime=0;
    codeString="";
    aChar<<"a"<<"b"<<"c"<<"d"<<"e"<<"f"<<"g"<<"h"<<"i"<<"j"<<"k"<<"l"<<"m"<<"n"<<"o"<<"p"<<"q"<<"r"<<"s"<<"t"<<"u"<<"v"<<"w"<<"x"<<"y"<<"z";
    AChar<<"A"<<"B"<<"C"<<"D"<<"E"<<"F"<<"G"<<"H"<<"I"<<"J"<<"K"<<"L"<<"M"<<"N"<<"O"<<"P"<<"Q"<<"R"<<"S"<<"T"<<"U"<<"V"<<"W"<<"X"<<"Y"<<"Z";
    numChar<<"0"<<"1"<<"2"<<"3"<<"4"<<"5"<<"6"<<"7"<<"8"<<"9";


    QTimer *timer2=new QTimer(this);
    connect(timer2,SIGNAL(timeout()),this,SLOT(getCodeUser()));
    timer2->start(1500);

    picPlayer=new picturePlayer;
    connect(picPlayer,SIGNAL(siganalCode(int,QString)),this,SLOT(getcodefromPicture(int,QString)));

    int ok=conf.read("Player","picturePlayer").toInt();
    if(ok==1)//开
    {
    picPlayerFlag=true;
    }
    else
    {
    picPlayerFlag=false;
    }
    pictureTimeS= conf.read("Player","picturePlayerTime").toInt();
    if(pictureTimeS>40)
    {

    }
    else
    {
     pictureTimeS=60;
    }
   thread=new wiftThread;//4G网络
   connect(thread,SIGNAL(sendnetsignal(int)),this,SLOT(setnetsignal(int)));
   thread->start();
   connect(this,SIGNAL(senddata(QString)),&codeshow,SLOT(getInfor(QString)));
   connect(this,SIGNAL(senduserinfor(QString)),&codeshow,SLOT(getInfor(QString)));


    connect(this,SIGNAL(sendremoteLogin()),&manger,SLOT(remoteLogin()));//远程登入关联
    connect(&user,SIGNAL(sendCode(QString)),this,SLOT(getHandwriteCode(QString)));
    connect(&manger,SIGNAL(sendUserLogin()),this,SLOT(setUserlogin()));

    receiveThread=new receiveDataFromServer(this);
    connect(this,SIGNAL(sendcode(int,QString)),receiveThread,SLOT(takeCode(int,QString)));
    connect(this,SIGNAL(sendManagerOp(int,QString)),receiveThread,SLOT(updateManagerChange(int,QString)));
    connect(receiveThread,SIGNAL(sendcodestatus(int,QString)),this,SLOT(getcodestatus(int,QString)));
    connect(receiveThread,SIGNAL(sendTimeTipUser(int)),this,SLOT(getDrawAutocloseTime(int)));
    connect(receiveThread,SIGNAL(sendCmdStatus(QString,QString)),this,SLOT(updateShow(QString,QString)));
    connect(receiveThread,SIGNAL(sendSoundPlay(int)),this,SLOT(getSoundPlay(int)));
    connect(receiveThread,SIGNAL(sendUpdateFileName(QString)),this,SLOT(getupdateFile(QString)));
    connect(receiveThread,SIGNAL(signaldata(int)),this,SLOT(setdatasignal(int)));

    connect(this,SIGNAL(signalthread(int)),receiveThread,SLOT(setThread(int)));

    receiveThread->start();

    pictureTime=0;

    soundThread=new soundPlayerThread;
    connect(this,SIGNAL(sendPlaySound(int)),soundThread,SLOT(playeSound(int)));
    soundThread->start();
    emit sendPlaySound(0);//开机声音

    connect(this,SIGNAL(sendTipUserCloseDrawtime(int)),showTipUser,SLOT(getTime(int)));
    connect(this,SIGNAL(sendTipclose(QString)),showTipUser,SLOT(getInfor(QString)));//自动关闭抽屉



//    advertisermentText *adverText=new advertisermentText(this);
//    adverText->setText(QObject::tr("     欢迎使用！，GPRS智能钥匙柜体软件，这是广告字幕滚动TEST！！！！！   "));

//    QFont font;
//    font.setPointSize(35);
//    adverText->setFont(font);
//    adverText->setGeometry(QRect(0,510,1000,100));//广告显示的位置
//    adverText->show();

    updatefile =new updateFileThread;
    connect(updatefile,SIGNAL(sendfinished()),this,SLOT(getdownfinished()));
    connect(this,SIGNAL(sendUpdateFileName(QString)),updatefile,SLOT(getFileName(QString)));
    updatefile->start();

    newstatuflag.setParent(this);
    newstatuflag.setGeometry(QRect(-8,-8,60,60));
    netsignal.setScaledContents(true);//自动适应图片大小

    isLoginManager=false;

    netsignal.setParent(this);
    netsignal.setGeometry(QRect(976,-2,50,50));
    netsignal.setScaledContents(true);//自动适应图片大小

    netdataflag.setParent(this);
    netdataflag.setGeometry(QRect(976,40,50,40));
    netdataflag.setScaledContents(true);//自动适应图片大小
    netdataflag.setPixmap(QPixmap(":/images/database.png"));

    int netmode=conf.read("Network","networkmode").toInt();
    if(netmode==1)
    {
    netsignal.setPixmap(QPixmap(":/images/signalN.png"));
    }
    log.writelog("connection.log"," ");
    log.writelog("connection.log"," ");
    QString rundata="################"+QDateTime::currentDateTime().toString("yyyy年 MM月 dd日")+"###################";
    log.writelog("connection.log",rundata);
    log.writelog("connection.log","restart system ok!");
    conf.writetemp("userlogin","flag","0");//

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::getSoundPlay(int index)//播放声音
{
   emit sendPlaySound(index);
}

void MainWindow::mousePressEvent(QMouseEvent *)
{
     pictureTime=0;
     changePictureTime=2;
     btn_user->show();
     ui->label->setPixmap(QPixmap(":/images/login.png"));
}

void MainWindow::getcodestatus(int index,QString status)
{
    picPlayer->hide();//把图片播放隐藏
    pictureTime=0;
    if(status=="01")//01表示正在处理  00表示需要需要服务器确认
    {
        setShowPicture(0,0);    
    }
    else
    {
        changePictureTime=8;//清除条码扫描提示
        setShowPicture(0,0);
        emit senduserinfor("请求已上传到服务器\n正在处理***");
        if(!codeshow.isVisible())
        {
            codeshow.show();
        }
    }
}

void MainWindow::keyPressEvent(QKeyEvent *e1)
{
    changePictureTime=2;
    if(e1->key()!=Qt::Key_Escape)
    {
    qDebug()<<"字符："<<keyToNumber(e1->key())<<"    键值："<<e1->text();


    if(e1->key()==Qt::Key_E)//获取第一个字符
    {
         qDebug()<<"end:";
        if(codeString.size()>5)
        {
           qDebug()<<"二维码是："<<codeString;    
           emit sendPlaySound(32);//code sound
           emit sendcode(1,codeString);//发送给线程确认
           pictureTime=0;
           picPlayer->hide();
        }
        firstString.clear();
        endString.clear();
        codeString.clear();
    }
    if(!firstString.isEmpty()&&endString.isEmpty())//
    {
        codeString=codeString+keyToNumber(e1->key());
    }
    if(e1->key()==Qt::Key_A)//获取第一个字符
    {
        qDebug()<<"start:";
        firstString=keyToNumber(e1->key());
        codeString.clear();
        endString.clear();
    }

    }

}
void MainWindow::keyReleaseEvent(QKeyEvent * e2)
{
    qDebug()<<"主界面："<<this->isActiveWindow();
    if(this->isActiveWindow())
    {
    if(Qt::Key_Return==e2->key())//确认键按下
    {
        enterKeyTime++;

        if(!user.isVisible())
        {
            if(enterKeyTime>1)
            {
            enterKeyTime=0;
            user.show();
            }

        }
        if(manger.isVisible())
        {
            manger.hide();
        }
    }
    if(Qt::Key_Escape==e2->key())//取消键按下
    {
        CancelKeyTime++;
        if(user.isVisible())
        {
        user.hide();
        }

    }
    else
    {

        if((e2->key()==Qt::Key_1)||(e2->key()==Qt::Key_2)||(e2->key()==Qt::Key_3))
        {
        otherkeyTime++;
        qDebug()<<"#########################"<<e2->key();
        if(!manger.isVisible())
        {
            if((CancelKeyTime>2)&&(otherkeyTime>2))
             {
              CancelKeyTime=0;
              otherkeyTime=0;
              manger.show();
             }
        }

        }
        else
        {
            CancelKeyTime=0;
            otherkeyTime=0;

        }
    }
    }

}

QString MainWindow::keyToNumber(int num)
{
    QString number;
    if((num<=57)&&(num>=48))//48～57
    {
        number=numChar.at(num-48);
    }
    if((num>=65)&&(num<=90))
    {
        number=aChar.at(num-65);
    }
    else
    {
        if(Qt::Key_Escape==num)
            number="no";
        if(Qt::Key_Return==num)
            number="ok";
        if((16777252<=num)&&(16777278>=num))
            number=AChar.at((num-16777252));
    }
return number;
}

void MainWindow::getCodeUser()//扫描二维码   以及时间刷新
{
    if(changePictureTime>1)
    {
    btn_user->hide();
    changePictureTime--;
    }

    else
    {
    if((changePictureTime==1))
    {
    changePictureTime--;
    }
    setShowPicture(0,0);
    codeshow.clearTipinfor();
    codeshow.hide();
    btn_user->show();

    }
    if(this->isActiveWindow())
    {
    receiveThread->setThread(1);
    }
    else
    {
        if(picPlayer->isActiveWindow())
        {
            qDebug()<<"player picture!";
         receiveThread->setThread(1);
        }
        else
        {
        receiveThread->setThread(0);
        }


    }
    if(this->isActiveWindow())
    {
    pictureTime++;
    qDebug()<<pictureTime<<"   "<<pictureTimeS;
    if(pictureTime==pictureTimeS)
    {
       if(picPlayerFlag){
       picPlayer->show();
       pictureTime=0;
       }
       pictureTime=0;
    }
    }






    bool flaglinkServer=receiveThread->getLinkServerStatus();//获取网络图标

    if(flaglinkServer)
    {
        newstatuflag.setPixmap(QPixmap(":/images/network_y.png"));
    }
    else
    {
        newstatuflag.setPixmap(QPixmap(":/images/network_n.png"));
    }
}


void MainWindow::getHandwriteCode(QString code)
{
   handwriteCode=code;
   qDebug()<<"收到二维码数据："<<handwriteCode;
   emit sendcode(2,handwriteCode);//发送给线程确认
   handwriteCode.clear();

}
void MainWindow::setShowPicture(int cmd,int status)//设置取钥匙图片
{
    if(status==0)
    {
    btn_user->show();
    }
    else
    {
   btn_user->hide();
    }
    ui->label->setScaledContents(true);//自动适应图片大小
    QMovie *move=new QMovie(":/images/changeKey.gif");
    if(cmd==0)
    {
        switch(status)
        {
        case 0:
            ui->label->setPixmap(QPixmap(":/images/login.png"));
            break;
        case 1:
            ui->label->setMovie(move);
            move->start();
            break;
        }
    }
    if(cmd==1)
    {
        switch(status)
        {
        case 0:
             ui->label->setPixmap(QPixmap(":/images/login.png"));
            break;
        case 1:
            // ui->label->setPixmap(QPixmap(":/images/takekey2.png"));
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            break;
        }
    }
    if(cmd==2)
    {
        switch(status)
        {
        case 0:
             ui->label->setPixmap(QPixmap(":/images/login.png"));
            break;
        case 1://抽屉打开
              ui->label->setPixmap(QPixmap(":/images/takekey2.png"));
            break;
        case 2://抽屉关闭
              ui->label->setPixmap(QPixmap(":/images/takekey3.png"));
            break;
        case 3:
            break;
        case 4:
               ui->label->setPixmap(QPixmap(":/images/takekeyfailse.png"));
            break;
        }
    }
    if(cmd==3)
    {
        switch(status)
        {
        case 0:
             ui->label->setPixmap(QPixmap(":/images/login.png"));
            break;
        case 1://门已经打开
             ui->label->setPixmap(QPixmap(":/images/returnKey2.png"));
            break;
        case 2://门关闭
            ui->label->setPixmap(QPixmap(":/images/returnKey3.png"));
            break;
        case 3:
            break;
        case 4:
            ui->label->setMovie(move);
            move->start();
            break;
        case 5:
            ui->label->setPixmap(QPixmap(":/images/returnkeyfailse.png"));
            break;
        }
    }
    if(cmd==16)
    {
        switch(status)
        {
        case 0:
             ui->label->setPixmap(QPixmap(":/images/login.png"));
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:
            ui->label->setMovie(move);
            move->start();
            break;
        }
    }
    if(cmd==18)
    {
    emit sendPlaySound(35);//code sound
    emit senduserinfor("请将此钥匙归还到预定还车站点");
    }

}

void MainWindow::getDrawAutocloseTime(int time)
{
    if(!showTipUser->isVisible())
    {
        showTipUser->setVisible(true);
    }
    emit sendTipUserCloseDrawtime(time);
}
void MainWindow::updateShow(QString cmd, QString status)
{
    changePictureTime=15;
    if(cmd=="01")
    {
         setShowPicture(0,0);
         emit sendTipclose("抽屉关闭倒计时");
         if(status=="01")
             sendPlaySound(1);
         else
             sendPlaySound(2);

    }
    if(cmd=="02")
    {
        if(status=="01")//取
        setShowPicture(2,1);
        else if(status=="02")// 抽屉打开
        {
            setShowPicture(2,1);
            sendPlaySound(21);
            changePictureTime=20;
        }
        else if(status=="03")//抽屉关闭
        {
            setShowPicture(2,2);
            sendPlaySound(2);
            changePictureTime=6;
        }
        else if(status=="04")//取钥匙失败
        {
            setShowPicture(2,4);
            changePictureTime=6;
        }
        else
        {
           emit senduserinfor(status);
           changePictureTime=5;
        }
    }
    if(cmd=="03")
    {
        emit sendTipclose("抽屉关闭倒计时");
        if(status=="01")//取
        setShowPicture(1,0);
        else if(status=="02")// 抽屉打开
        {
        changePictureTime=20;
        setShowPicture(3,1);
        emit sendPlaySound(31);
        }
        else if(status=="03")//抽屉关闭
        {
           setShowPicture(3,2);
           changePictureTime=5;
           emit sendPlaySound(2);
        }
        else if(status=="04")//用户换车
        {
           setShowPicture(3,4);
           changePictureTime=30;
        }
        else if(status=="05")//还钥匙
        {
           setShowPicture(3,5);
           changePictureTime=6;
        }
        else
        {
            emit senduserinfor(status);
            changePictureTime=5;
        }
    }
    if(cmd=="04")
    {
    setShowPicture(0,0);
    }
    if(cmd=="14")//报警状态
    {
    //setShowPicture(0,0);
    changePictureTime=8;
    emit senduserinfor(status);
    }
    if(cmd=="15")       //"远程登入show";
    {
    emit senduserinfor("远程登入管理员");
    changePictureTime=3;
    emit sendremoteLogin();
    }
    if(cmd=="16")
    {
    setShowPicture(16,4);
    changePictureTime=30;
    }

    if(cmd=="18")
    {
    setShowPicture(18,1);
    changePictureTime=30;
    }
    if(cmd=="17")
    {
     changePictureTime=30;
     emit senduserinfor(status);
    }
}
void MainWindow::getdownfinished()
{
//     emit senduserinfor("更新已完成");
//     changePictureTime=4;
}
void MainWindow::getupdateFile(QString filename)
{
    qDebug()<<"mainwindow get file name:"<<filename;
//    if(filename.contains("MPlayer"))
//    emit senduserinfor("正在更新系统");
//    else
//         emit senduserinfor("正在更新,请稍候***");

    emit sendUpdateFileName(filename);
//    iHttpDownloadS *down=new iHttpDownloadS;
//    down->getFileFromURL(filename,"/home/luo/Desktop/");
}
void MainWindow::startplay()
{

}

void MainWindow::setUserlogin()
{
     isLoginManager=true;//说明已经进入管理员
     receiveThread->setThread(0);
}

void  MainWindow::lookedUp(QHostInfo &host)
{
    // qDebug() << host.addresses().first().toString();
}

void MainWindow::getManagerOp(int drawno,QString keyid)//发送管理员操作
{
 emit sendManagerOp(drawno,keyid);
}
void MainWindow::setnetsignal(int index)
{
    switch(index)
    {
    case -1:
        netsignal.setPixmap(QPixmap(":/images/signalN.png"));
        break;
    case 0:
        netsignal.setPixmap(QPixmap(":/images/signal0.png"));
        break;
    case 1:
        netsignal.setPixmap(QPixmap(":/images/signal1.png"));
        break;
    case 2:
        netsignal.setPixmap(QPixmap(":/images/signal2.png"));
        break;
    case 3:
        netsignal.setPixmap(QPixmap(":/images/signal3.png"));
        break;
    case 4:
        netsignal.setPixmap(QPixmap(":/images/signal4.png"));
        break;
    case 5:
        netsignal.setPixmap(QPixmap(":/images/signal5.png"));
        break;

    }
}
void MainWindow::setdatasignal(int index)
{
    switch(index)
    {
    case 0:
        netdataflag.setPixmap(QPixmap(":/images/database.png"));//命令状态
        break;
    case 1:
        netdataflag.setPixmap(QPixmap(":/images/database_g.png"));//上传
        break;
    case 2:
        netdataflag.setPixmap(QPixmap(":/images/database_b.png"));//下载
        break;
    case 3:
        netdataflag.setPixmap(QPixmap(":/images/database_y.png"));//命令正在执行
        break;
    }
}

void MainWindow::getcodefromPicture(int, QString code)
{
    emit sendPlaySound(32);//code sound
    emit sendcode(1,code);//发送给线程确认
    pictureTime=0;
    picPlayer->hide();
}
