/****************************************************************************
** Meta object code from reading C++ file 'receivedatafromserver.h'
**
** Created: Fri Nov 13 09:44:51 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "receivedatafromserver.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'receivedatafromserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_receiveDataFromServer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x05,
      44,   22,   22,   22, 0x05,
      58,   22,   22,   22, 0x05,
      75,   73,   22,   22, 0x05,
     103,   22,   22,   22, 0x05,
     123,   22,   22,   22, 0x05,
     144,   73,   22,   22, 0x05,
     175,   22,   22,   22, 0x05,
     196,   22,   22,   22, 0x05,
     215,   22,   22,   22, 0x05,
     243,   22,   22,   22, 0x05,
     269,   22,   22,   22, 0x05,
     295,   22,   22,   22, 0x05,
     313,   22,   22,   22, 0x05,

 // slots: signature, parameters, type, tag, flags
     334,   22,  329,   22, 0x08,
     354,   22,   22,   22, 0x08,
     365,   73,   22,   22, 0x08,
     387,   22,   22,   22, 0x08,
     407,   22,  399,   22, 0x08,
     423,   73,   22,   22, 0x08,
     453,   22,   22,   22, 0x08,
     470,   22,   22,   22, 0x08,
     490,   22,   22,   22, 0x08,
     517,   22,   22,   22, 0x08,
     542,   22,   22,   22, 0x08,
     568,   22,   22,   22, 0x08,
     594,   22,   22,   22, 0x08,
     623,   22,  329,   22, 0x08,
     638,   73,   22,   22, 0x08,
     671,   22,   22,   22, 0x08,
     692,   22,   22,   22, 0x08,
     713,   22,   22,   22, 0x08,
     729,   22,   22,   22, 0x08,
     747,   22,  399,   22, 0x08,
     764,   22,   22,   22, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_receiveDataFromServer[] = {
    "receiveDataFromServer\0\0sigalOpendoor(char*)\0"
    "sigalRelink()\0sigalhasdata()\0,\0"
    "sendcodestatus(int,QString)\0"
    "sendDrawNumber(int)\0sendTimeTipUser(int)\0"
    "sendCmdStatus(QString,QString)\0"
    "sendReadKeyStatues()\0sendSoundPlay(int)\0"
    "sendUpdateFileName(QString)\0"
    "sendDatatoServer(QString)\0"
    "signalautoCloseTimer(int)\0signalthread(int)\0"
    "signaldata(int)\0bool\0rebackData(QString)\0"
    "idelData()\0takeCode(int,QString)\0"
    "timeTimer()\0QString\0getNumberflag()\0"
    "backcodetoServer(int,QString)\0"
    "autoCloseTimer()\0autoCloseTimer(int)\0"
    "getReadKeyStatues(QString)\0"
    "updateKeyStatus(QString)\0"
    "updateDrawStatus(QString)\0"
    "updatecodeStatus(QString)\0"
    "updateWarningStatus(QString)\0"
    "packToServer()\0updateManagerChange(int,QString)\0"
    "readMessage(QString)\0getNetworkFlag(bool)\0"
    "runNetworkcmd()\0threadRunset(int)\0"
    "getnetworkTime()\0senddatasignal(int)\0"
};

void receiveDataFromServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        receiveDataFromServer *_t = static_cast<receiveDataFromServer *>(_o);
        switch (_id) {
        case 0: _t->sigalOpendoor((*reinterpret_cast< char*(*)>(_a[1]))); break;
        case 1: _t->sigalRelink(); break;
        case 2: _t->sigalhasdata(); break;
        case 3: _t->sendcodestatus((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->sendDrawNumber((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->sendTimeTipUser((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->sendCmdStatus((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 7: _t->sendReadKeyStatues(); break;
        case 8: _t->sendSoundPlay((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->sendUpdateFileName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->sendDatatoServer((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->signalautoCloseTimer((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->signalthread((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->signaldata((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: { bool _r = _t->rebackData((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 15: _t->idelData(); break;
        case 16: _t->takeCode((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 17: _t->timeTimer(); break;
        case 18: { QString _r = _t->getNumberflag();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 19: _t->backcodetoServer((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 20: _t->autoCloseTimer(); break;
        case 21: _t->autoCloseTimer((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->getReadKeyStatues((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 23: _t->updateKeyStatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 24: _t->updateDrawStatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 25: _t->updatecodeStatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 26: _t->updateWarningStatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 27: { bool _r = _t->packToServer();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 28: _t->updateManagerChange((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 29: _t->readMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 30: _t->getNetworkFlag((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 31: _t->runNetworkcmd(); break;
        case 32: _t->threadRunset((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 33: { QString _r = _t->getnetworkTime();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 34: _t->senddatasignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData receiveDataFromServer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject receiveDataFromServer::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_receiveDataFromServer,
      qt_meta_data_receiveDataFromServer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &receiveDataFromServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *receiveDataFromServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *receiveDataFromServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_receiveDataFromServer))
        return static_cast<void*>(const_cast< receiveDataFromServer*>(this));
    return QThread::qt_metacast(_clname);
}

int receiveDataFromServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    }
    return _id;
}

// SIGNAL 0
void receiveDataFromServer::sigalOpendoor(char * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void receiveDataFromServer::sigalRelink()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void receiveDataFromServer::sigalhasdata()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void receiveDataFromServer::sendcodestatus(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void receiveDataFromServer::sendDrawNumber(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void receiveDataFromServer::sendTimeTipUser(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void receiveDataFromServer::sendCmdStatus(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void receiveDataFromServer::sendReadKeyStatues()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void receiveDataFromServer::sendSoundPlay(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void receiveDataFromServer::sendUpdateFileName(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void receiveDataFromServer::sendDatatoServer(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void receiveDataFromServer::signalautoCloseTimer(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void receiveDataFromServer::signalthread(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void receiveDataFromServer::signaldata(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}
QT_END_MOC_NAMESPACE
