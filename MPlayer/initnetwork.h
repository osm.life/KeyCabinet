#ifndef INITNETWORK_H
#define INITNETWORK_H

#include <QObject>
#include<QVector>
#include"config.h"
#include"networktest.h"
class initNetwork : public QObject
{
    Q_OBJECT
public:
    explicit initNetwork(QObject *parent);
    void init();//初始化网络
    int getNetSignal();//获取4G信号
    bool getnetstatus();//获取联网状态
    void rebootdev();


signals:

public slots:
    void opendev();
private:
    config conf;
    networkTest gps;
    QTimer opentimer;
    bool switchflag;


};

#endif // INITNETWORK_H
