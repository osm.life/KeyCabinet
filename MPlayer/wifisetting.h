#ifndef WIFISETTING_H
#define WIFISETTING_H
#include<QModelIndex>
#include <QDialog>
#include"settingwifi.h"
#include<QRadioButton>
namespace Ui {
    class wifisetting;
}

class wifisetting : public QDialog
{
    Q_OBJECT

public:
    explicit wifisetting(QWidget *parent = 0);
    ~wifisetting();
    void scanwifi();

private:
    Ui::wifisetting *ui;
    QStringList wifiNamelist;
    settingWifi wifi;


   private slots:
       void on_cancelBtn_clicked();
       void on_connectBtn_clicked();
       void on_closeBtn_clicked();
       //void updatewifi();
       void on_updateBtn_clicked();
       void on_listWidget_clicked(const QModelIndex &index);
   signals:
       void sendEssid(QString);
};

#endif // WIFISETTING_H
