#include "mybutton.h"
#include<QFont>
myButton::myButton(QWidget *parent) :
    QToolButton(parent)
{

    QFont font;
    font.setPointSize(15);
    this->setFont(font);
    setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    connect(this,SIGNAL(clicked()),this,SLOT(senddata()));

}

void myButton::setWindowName(QString name)
{
    setWindowTitle(name);
    setObjectName(name);
}
void myButton::setMyPicture(QString pic,int size)
{
    setIcon(QIcon(QPixmap(pic)));
    setIconSize(QSize(size,size));
    setBaseSize(QSize(size+15,size+15));
}
void myButton::senddata()
{
    emit sendNum(this->objectName());
}
