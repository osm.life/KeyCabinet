#ifndef WATCHDOG_H
#define WATCHDOG_H

#include <QObject>
#include <unistd.h>
#include <sys/stat.h>
#include <syslog.h>
#include <errno.h>
class watchdog : public QObject
{
    Q_OBJECT
public:
    explicit watchdog(QObject *parent = 0);
    bool openwatchdog();
    bool feeddog();
    bool closedog();
private:
    int fd_watchdog;

signals:

public slots:

};

#endif // WATCHDOG_H
