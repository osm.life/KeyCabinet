/********************************************************************************
** Form generated from reading UI file 'advertisermenttext.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADVERTISERMENTTEXT_H
#define UI_ADVERTISERMENTTEXT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_advertisermentText
{
public:

    void setupUi(QWidget *advertisermentText)
    {
        if (advertisermentText->objectName().isEmpty())
            advertisermentText->setObjectName(QString::fromUtf8("advertisermentText"));
        advertisermentText->resize(602, 415);

        retranslateUi(advertisermentText);

        QMetaObject::connectSlotsByName(advertisermentText);
    } // setupUi

    void retranslateUi(QWidget *advertisermentText)
    {
        advertisermentText->setWindowTitle(QApplication::translate("advertisermentText", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class advertisermentText: public Ui_advertisermentText {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADVERTISERMENTTEXT_H
