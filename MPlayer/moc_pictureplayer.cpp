/****************************************************************************
** Meta object code from reading C++ file 'pictureplayer.h'
**
** Created: Fri Nov 13 09:19:25 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "pictureplayer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pictureplayer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_picturePlayer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   15,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   14,   14,   14, 0x09,
      60,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_picturePlayer[] = {
    "picturePlayer\0\0,\0siganalCode(int,QString)\0"
    "pictures(QString)\0changePicture()\0"
};

void picturePlayer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        picturePlayer *_t = static_cast<picturePlayer *>(_o);
        switch (_id) {
        case 0: _t->siganalCode((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->pictures((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->changePicture(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData picturePlayer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject picturePlayer::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_picturePlayer,
      qt_meta_data_picturePlayer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &picturePlayer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *picturePlayer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *picturePlayer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_picturePlayer))
        return static_cast<void*>(const_cast< picturePlayer*>(this));
    return QWidget::qt_metacast(_clname);
}

int picturePlayer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void picturePlayer::siganalCode(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
