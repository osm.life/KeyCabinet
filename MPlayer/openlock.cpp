#include "openlock.h"
#include<QDebug>
Openlock::Openlock(QObject *parent) :
    QThread(parent)
{ 
    timeoutflag=0;
    serial.openMycom(1,1);
    alllockNum=0;
    openTypeFlag=true;
    maxNumber=conf.read("Cabinet","cabinetNumber").toInt();
}
void Openlock::getlockNo(int no)
{
    openTypeFlag=false;
    openOneLock(no);

}
void Openlock::getcloseNo(int no)
{
  closeoneLock(no);
}

void Openlock::openOneLock(int index)//打开某号锁
{
  int trytime1=0;
   serial.WriteMyCom(comman.readstatus_command(index));
   QString getinfor=serial.ReadMyCom(200);
   QString backinfor;
   while(true)
   {
       trytime1++;
      if(backinfor.contains("b9"))
      {
        break;
      }
      else
      {
          if(trytime1>3)
              break;
       backinfor=serial.ReadMyCom(200);
       }
   }
   if(getinfor.size()>=18)
   {
   backinfor=getinfor.mid(getinfor.size()-18,18);
   }

   QString statusflag=backinfor.mid(14,2);
   if(statusflag=="02")//抽屉打开的状态，就发送关闭的命令
    {
       int trytime=0;
       serial.WriteMyCom(comman.close_command(index));
       QString backinfor=serial.ReadMyCom(200);
       while(true)
       {
           trytime++;
       if(backinfor.contains("b5"))
       {
           qDebug()<<"关闭抽屉获得数据："<<backinfor;
           break;
       }
       else
       {
           if(trytime>6)
               break;
       backinfor=serial.ReadMyCom(400);
       }
       }


       if(backinfor.size()>=18)
       {
       backinfor=backinfor.mid(backinfor.size()-18,18);
       }
       statusflag=backinfor.mid(14,2);
    }
    else //发送打开命令
    {
       com=comman.openover_command(index);
        serial.WriteMyCom(com);
        QString inforr;//=serial.ReadMyCom(2000);
        //QString statusflag="00";
        while(true)
        {
            timeoutflag++;
            inforr=serial.ReadMyCom(200);
            if(inforr.size()>=18)
            {
            qDebug()<<"打开抽屉返回数据："<<inforr;
            inforr=inforr.mid(inforr.size()-18,18); //一次读取发送和返回数据
            }
            if(inforr.mid(0,2)=="b6")
            {
            timeoutflag=0;
            statusflag=inforr.mid(14,2);
            break;
            }
            if(timeoutflag>40)
            {
            timeoutflag=0;
            break;
            }

        }
        if((statusflag=="01")||(statusflag=="02"))
        {
            sendManagerOp(QString::number(index),"","1");
        }
    }
    emit senddata(index,statusflag);


}

void Openlock::openOneLock(int index,int no)//全开
{
//  int trytime1=0;
//   serial.WriteMyCom(comman.readstatus_command(index));
//   QString getinfor=serial.ReadMyCom(200);
//   QString backinfor;
//   while(true)
//   {
//       trytime1++;
//      if(backinfor.contains("b9"))
//      {
//        break;
//      }
//      else
//      {
//          if(trytime1>3)
//              break;
//       backinfor=serial.ReadMyCom(200);
//       }
//   }
//   if(getinfor.size()>=18)
//   {
//   backinfor=getinfor.mid(getinfor.size()-18,18);
//   }

//   QString statusflag=backinfor.mid(14,2);
//   if(statusflag=="02")//抽屉打开的状态，就发送关闭的命令
//    {
//       int trytime=0;
//       serial.WriteMyCom(comman.close_command(index));
//       QString backinfor=serial.ReadMyCom(200);
//       while(true)
//       {
//           trytime++;
//       if(backinfor.contains("b5"))
//       {
//           qDebug()<<"关闭抽屉获得数据："<<backinfor;
//           break;
//       }
//       else
//       {
//           if(trytime>6)
//               break;
//       backinfor=serial.ReadMyCom(400);
//       }
//       }


//       if(backinfor.size()>=18)
//       {
//       backinfor=backinfor.mid(backinfor.size()-18,18);
//       }
//       statusflag=backinfor.mid(14,2);
//    }
//    else //发送打开命令
//    {
       QString statusflag;
       com=comman.openover_command(index);
        serial.WriteMyCom(com);
        QString inforr;//=serial.ReadMyCom(2000);
        //QString statusflag="00";
        while(true)
        {
            timeoutflag++;
            inforr=serial.ReadMyCom(200);
            if(inforr.size()>=18)
            {
            qDebug()<<"打开抽屉返回数据："<<inforr;
            inforr=inforr.mid(inforr.size()-18,18); //一次读取发送和返回数据
            }
            if(inforr.mid(0,2)=="b6")
            {
            timeoutflag=0;
            statusflag=inforr.mid(14,2);
            break;
            }
            if(timeoutflag>40)
            {
            timeoutflag=0;
            break;
            }

        }
        if((statusflag=="01")||(statusflag=="02"))
        {
            sendManagerOp(QString::number(index),"","1");
        }
  //  }
    emit senddata(index,statusflag);


}

void Openlock::startOpenAll(bool ok)
{
    openTypeFlag=ok;
    for(int i=1;i<=maxNumber;i++)
    {
        if(openTypeFlag)//开
        {
        emit senddata(i,"99");
        openOneLock(i,0);
        }
        else
        {
        emit sendcloseData(i,"88");
        closeoneLock(i,0);//关
        }
    }
}
void Openlock::startSingle()
{

}
void Openlock::closeoneLock(int index)
{
    int trytime1=0;
     serial.WriteMyCom(comman.readstatus_command(index));
     QString getinfor=serial.ReadMyCom(200);
     QString backinfor;
     while(true)
     {
         trytime1++;
        if(backinfor.contains("b9"))
        {
          break;
        }
        else
        {
            if(trytime1>3)
                break;
         backinfor=serial.ReadMyCom(200);
         }
     }
     if(getinfor.size()>=18)
     {
     backinfor=getinfor.mid(getinfor.size()-18,18);
     }

     QString statusflag=backinfor.mid(14,2);
     if(statusflag=="02")//抽屉打开的状态，就发送关闭的命令
      {
        com=comman.close_command(index);
         serial.WriteMyCom(com);
         QString inforr;//=serial.ReadMyCom(2000);
             QString statusflag="00";
             while(true)
              {
               timeoutflag++;
              inforr=serial.ReadMyCom(200);
              if(inforr.size()>=18)
                  {
               inforr=inforr.mid(inforr.size()-18,inforr.size()); //一次读取发送和返回数据
              }
              if(inforr.mid(0,2)=="b5")
                 {
                   timeoutflag=0;
               statusflag=inforr.mid(14,2);
               break;
               }
         if(timeoutflag>40)//超时3秒返回
         {

        timeoutflag=0;
             break;
          }
             }
     }
     else
     {
         statusflag="01";
     }
   emit sendcloseData(index,statusflag);
}
void Openlock::closeoneLock(int index,int no)
{
//    int trytime1=0;
//     serial.WriteMyCom(comman.readstatus_command(index));
//     QString getinfor=serial.ReadMyCom(200);
//     QString backinfor;
//     while(true)
//     {
//         trytime1++;
//        if(backinfor.contains("b9"))
//        {
//          break;
//        }
//        else
//        {
//            if(trytime1>3)
//                break;
//         backinfor=serial.ReadMyCom(200);
//         }
//     }
//     if(getinfor.size()>=18)
//     {
//     backinfor=getinfor.mid(getinfor.size()-18,18);
//     }

//     QString statusflag=backinfor.mid(14,2);
//     if(statusflag=="02")//抽屉打开的状态，就发送关闭的命令
//      {
        com=comman.close_command(index);
         serial.WriteMyCom(com);
         QString inforr;//=serial.ReadMyCom(2000);
             QString statusflag="00";
             while(true)
              {
               timeoutflag++;
              inforr=serial.ReadMyCom(200);
              if(inforr.size()>=18)
                  {
               inforr=inforr.mid(inforr.size()-18,inforr.size()); //一次读取发送和返回数据
              }
              if(inforr.mid(0,2)=="b5")
                 {
                   timeoutflag=0;
               statusflag=inforr.mid(14,2);
               break;
               }
         if(timeoutflag>40)//超时3秒返回
         {

        timeoutflag=0;
             break;
          }
             }
     //}
//     else
//     {
//         statusflag="01";
//     }
   emit sendcloseData(index,statusflag);
}

void Openlock::getWarnCmd(int index)
{
    qDebug()<<"releasewarn_command";
     serial.WriteMyCom(comman.releasewarn_command(index));
     QString infor=serial.ReadMyCom();
     qDebug()<<infor;
}
