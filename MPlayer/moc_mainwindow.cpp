/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Fri Nov 13 10:14:32 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      32,   30,   11,   11, 0x05,
      54,   11,   11,   11, 0x05,
      77,   11,   11,   11, 0x05,
      99,   11,   11,   11, 0x05,
     129,   11,   11,   11, 0x05,
     147,   11,   11,   11, 0x05,
     166,   11,   11,   11, 0x05,
     194,   30,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
     221,   30,   11,   11, 0x08,
     256,   11,  248,   11, 0x08,
     273,   11,   11,   11, 0x08,
     287,   11,   11,   11, 0x08,
     313,   30,   11,   11, 0x08,
     337,   11,   11,   11, 0x08,
     374,  363,   11,   11, 0x08,
     402,   11,   11,   11, 0x08,
     420,   11,   11,   11, 0x08,
     438,   11,   11,   11, 0x08,
     461,   11,   11,   11, 0x08,
     473,   11,   11,   11, 0x08,
     493,  488,   11,   11, 0x08,
     514,   30,   11,   11, 0x08,
     540,   11,   11,   11, 0x08,
     564,  558,   11,   11, 0x08,
     583,   30,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0senddata(QString)\0,\0"
    "sendcode(int,QString)\0senduserinfor(QString)\0"
    "sendTipclose(QString)\0"
    "sendTipUserCloseDrawtime(int)\0"
    "sendremoteLogin()\0sendPlaySound(int)\0"
    "sendUpdateFileName(QString)\0"
    "sendManagerOp(int,QString)\0"
    "getcodestatus(int,QString)\0QString\0"
    "keyToNumber(int)\0getCodeUser()\0"
    "getHandwriteCode(QString)\0"
    "setShowPicture(int,int)\0"
    "getDrawAutocloseTime(int)\0cmd,status\0"
    "updateShow(QString,QString)\0"
    "getSoundPlay(int)\0getdownfinished()\0"
    "getupdateFile(QString)\0startplay()\0"
    "setUserlogin()\0host\0lookedUp(QHostInfo&)\0"
    "getManagerOp(int,QString)\0setnetsignal(int)\0"
    "index\0setdatasignal(int)\0"
    "getcodefromPicture(int,QString)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->senddata((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->sendcode((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->senduserinfor((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->sendTipclose((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->sendTipUserCloseDrawtime((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->sendremoteLogin(); break;
        case 6: _t->sendPlaySound((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->sendUpdateFileName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->sendManagerOp((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 9: _t->getcodestatus((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 10: { QString _r = _t->keyToNumber((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 11: _t->getCodeUser(); break;
        case 12: _t->getHandwriteCode((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: _t->setShowPicture((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 14: _t->getDrawAutocloseTime((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->updateShow((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 16: _t->getSoundPlay((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->getdownfinished(); break;
        case 18: _t->getupdateFile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 19: _t->startplay(); break;
        case 20: _t->setUserlogin(); break;
        case 21: _t->lookedUp((*reinterpret_cast< QHostInfo(*)>(_a[1]))); break;
        case 22: _t->getManagerOp((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 23: _t->setnetsignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->setdatasignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->getcodefromPicture((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::senddata(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::sendcode(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::senduserinfor(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::sendTipclose(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWindow::sendTipUserCloseDrawtime(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWindow::sendremoteLogin()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void MainWindow::sendPlaySound(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void MainWindow::sendUpdateFileName(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void MainWindow::sendManagerOp(int _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_END_MOC_NAMESPACE
