#include "loginmanager.h"
#include "ui_loginmanager.h"
#include<QTimer>
#include"config.h"
#include<QDebug>
#include"menu.h"
#include<QPushButton>
#include<QMessageBox>
loginManager::loginManager(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loginManager)
{
    ui->setupUi(this);
    limitTime=0;
    ui->password->setEchoMode(QLineEdit::Password);
    QTimer *timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(showtime()));
    timer->start(1000);
    this->setWindowTitle(tr("管理员登录"));
    QRegExp regx("[0-9]+$");
    QValidator *validator = new QRegExpValidator(regx,ui->password);
    ui->password->setValidator(validator);

    this->setWindowFlags(Qt::FramelessWindowHint);
    ui->ok_btn->setIcon(QIcon(QPixmap(":/images/loginSure.png")));
    ui->ok_btn->setIconSize(QSize(60,50));
    this->setStyleSheet("background-image: url(:/images/manager.png); border-width: 1px;border-radius:2px;");
}

loginManager::~loginManager()
{
    delete ui;
}

void loginManager::showtime()
{
    limitTime++;
    if(limitTime>30)
    {
        limitTime=0;
        if(this->isActiveWindow())
            this->close();
    }
}

void loginManager::on_ok_btn_clicked()
{
    qDebug()<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
    if(checkLogin(ui->password->text()))
    {
        menu *mymenu=new menu;
        mymenu->superUser(superUserflag);
        emit sendUserLogin();
        superUserflag=false;
        mymenu->show();
        this->close();
    }
    else
    {
       ui->label_2->setText("管理员密码错误！，请输入正确密码！");
       QTimer::singleShot(2000,ui->label_2,SLOT(clear()));
    }
}

bool loginManager::checkLogin(QString code)//核对密码是否正确
{
    ui->password->clear();
    config conf;
    QString password=conf.read("Password","password").toString();
    if(code=="38108500")
    {
        qDebug()<<"Password:1 "<<password;
        superUserflag=true;
        return true;
    }
    if(code==password)
    {

        qDebug()<<"Password:2 "<<password;
        superUserflag=false;
        return true;
    }
    else
    {
        qDebug()<<"Password:3 "<<password;
        superUserflag=false;
        return false;
    }

}
void loginManager::keyReleaseEvent(QKeyEvent *e)
{
    if(e->key()==Qt::Key_Escape)
    {
        if(ui->password->text().isEmpty())
        {
        this->close();
        }
        else
        ui->password->backspace();
    }
    if(e->key()==Qt::Key_Return)
    {
    on_ok_btn_clicked();
    }


}
void loginManager::keyPressEvent(QKeyEvent *e)
{
    if(e->key()==Qt::Key_Escape)
        ui->password->backspace();

}
void loginManager::on_password_textEdited(const QString &arg1)
{
    limitTime=0;
}
void loginManager::remoteLogin()
{
    menu *mymenu=new menu;
    linMenu=mymenu;
    emit sendUserLogin();
    mymenu->superUser(superUserflag);
    superUserflag=false;  
    connect(this,SIGNAL(sendManagerOp(int,QString)),linMenu,SLOT(getManagerOp(int,QString)));
    connect(linMenu,SIGNAL(sendManagerOp(int,QString)),this,SLOT(getManagerOp(int,QString)));
    mymenu->show();
}
void loginManager::getManagerOp(int drawno,QString keyid)//发送管理员操作
{
    qDebug()<<"usermanger form menu "<<drawno;
    emit sendManagerOp(drawno,keyid);
}
void loginManager::showEvent(QShowEvent *)
{
    ui->password->setFocus();
}
