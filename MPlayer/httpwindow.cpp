#include "httpwindow.h"
#include <QtGui>
#include<QtNetwork/QtNetwork>
 HttpWindow::HttpWindow(QWidget *parent):
     QWidget(parent)
 {
     #ifndef QT_NO_OPENSSL
     connect(&qnam, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
             this, SLOT(sslErrors(QNetworkReply*,QList<QSslError>)));
    #endif
    // downloadFile("http://d.adzop.com/adtopic/oumei/37/lxlJohnnieWalkerznhjlpwsjgg.rar");

 }

 HttpWindow::~HttpWindow()
 {

 }
 void HttpWindow::onConvert()
 {

 }



void HttpWindow::startRequest(QUrl url)   //链接请求
 {
     qDebug()<<"start down file:"<<url;

     reply = qnam.get(QNetworkRequest(url));//下载
     connect(reply, SIGNAL(finished()),this, SLOT(httpFinished()));
     connect(reply, SIGNAL(readyRead()),this, SLOT(httpReadyRead()));


 }
 void HttpWindow::downloadFile(QString addres)
 {


     url = addres;
     QFileInfo fileInfo(url.path());
     QString fileName = fileInfo.fileName();
     if(fileName.contains("down"))
     {
         qDebug()<<"     "<<addres.lastIndexOf("%")<<"   "<<addres.size();
         int sum=addres.size()-addres.lastIndexOf("%");
         fileName=addres.mid(addres.lastIndexOf("%")+1,sum);
         qDebug()<<"file of network file:"<<fileName;
     }
        filenameg=fileName;
      qDebug()<<"################start downloadFile#############################"<<fileName;
      if(fileName.contains(".txt"))
      {
          rebootfile=".txt";
      }
      else if(fileName.contains(".jpg")||fileName.contains(".png"))
      {
          rebootfile="jpgpng";
      }
      else if(fileName.contains(".mp4"))
      {
          rebootfile=".mp4";
      }
      else if(fileName.contains(".avi"))
      {
          rebootfile=".avi";
      }
      else if(fileName.contains("MPlayer"))
      {
     QProcess::execute("cp MPlayer MPlayer_s");
     rebootfile="MPlayer";
        if(fileName.contains("MPlayer.tar.gz"))
        {
        rebootfile="MPlayer.tar.gz";
        }
     }
     else if(fileName.contains(".wav"))
     {
            rebootfile=".wav";
     }
     else if(fileName.contains("qt4"))//开机启动文件
     {
           rebootfile="qt4";
     }
      else if(fileName.contains("voice.tar.gz"))//
      {
          rebootfile="voice.tar.gz";
      }
      else if(fileName.contains("picture.tar.gz"))//
      {
        rebootfile="picture.tar.gz";
      }
     else
     {
        fileName;
     }
     if (QFile::exists(fileName)) {
         QFile::remove(fileName);
     }

       qDebug()<<"filename path:"<<fileName;
     /******************************************/
     file = new QFile(fileName);
     if (!file->open(QIODevice::WriteOnly)) {

         delete file;
         file = 0;
         emit downloadfail();
         return;
     }
     httpRequestAborted = false;
     flag = 0;
     startRequest(url);//开始请求
 }

 void HttpWindow::cancelDownload()
 {
     reply->abort();
 }
 void HttpWindow::httpFinished()
 {

     connect(this, SIGNAL(finishedownload()),this, SLOT(httpFinishedownload()));//下载完成信号
     connect(this, SIGNAL(downloadfail()),this, SLOT(httpDownloadfail()));//下载失败
     if (httpRequestAborted) {
         if (file) {
             file->close();
             file->remove();
             delete file;
             file = 0;
             flag = 1;
             emit finishedownload();
         }
         reply->deleteLater();
         return;
     }
     file->flush();
     file->close();

     QVariant redirectionTarget = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
     if (reply->error()) {
         file->remove();
         emit downloadfail();
         qDebug()<<"error\n";
         reply->close();
        return;

     } else if (!redirectionTarget.isNull()) {
         QUrl newUrl = url.resolved(redirectionTarget.toUrl());

             url = newUrl;
             reply->deleteLater();
             file->open(QIODevice::WriteOnly);
             file->resize(0);
              qDebug()<<"startRequest\n";
             startRequest(url);
             return;

     }

     reply->deleteLater();
     reply = 0;
     delete file;
     file = 0;
     flag = 1;
     qDebug()<<"in hear\n";
     emit finishedownload();
 }

 void HttpWindow::httpFinishedownload()
 {
    qDebug()<<"yes! download success\n";
     emit downloadok(1);
     if(rebootfile=="MPlayer")
     {
      QProcess::execute("chmod 777 MPlayer");
      QProcess::execute("reboot");
     }
      else if(rebootfile=="MPlayer.tar.gz")
     {
      QProcess::execute("tar zxvf MPlayer.tar.gz");
      QProcess::execute("chmod 777 MPlayer");
      QProcess::execute("reboot");
     }
     else if(rebootfile.contains(".txt"))
     {
         QString strcmd="mv "+filenameg+ " text/";
         QProcess::execute(strcmd);
     }
     else if(rebootfile.contains("jpgpng")||rebootfile.contains(".png"))
     {
      QString strcmd="mv "+filenameg+ " /usr/local/mystart/picture/";
      QProcess::execute(strcmd);
     }
     else if(rebootfile.contains(".mp4"))
     {
         QString strcmd="mv "+filenameg+ " movie/";
         QProcess::execute(strcmd);
     }
     else if(rebootfile.contains(".avi"))
     {
         QString strcmd="mv "+filenameg+ " movie/";
         QProcess::execute(strcmd);
     }
     else if(rebootfile.contains(".wav"))
     {
         QString strcmd="mv "+filenameg+ " voice/";
         QProcess::execute(strcmd);
     }
     else if(rebootfile.contains("qt4"))
     {
      QProcess::execute("chmod 777 qt4");
      QProcess::execute("mv qt4 /bin/");
      QProcess::execute("reboot");
     }
     else if(rebootfile=="voice.tar.gz")
     {
     QProcess::execute("tar zxvf voice.tar.gz");
     }
     else if(rebootfile=="picture.tar.gz")
     {
     QProcess::execute("tar zxvf picture.tar.gz");
     QProcess::execute("mv /usr/local/mystart/*.png /usr/local/mystart/picture/");
     QProcess::execute("mv /usr/local/mystart/*.jpg /usr/local/mystart/picture/");
     }
     else
     {

     }



 }


 void HttpWindow::httpDownloadfail()
 {
      emit downloadok(-1);
     qDebug()<<" download faild\n";
     QProcess::execute("cp MPlayer_s MPlayer");
 }

 void HttpWindow::httpReadyRead()//接收到数据时的处理
 {
      qDebug()<<"ready to save file\n";
     if (file)
         file->write(reply->readAll());
 }

 void HttpWindow::updateDataReadProgress(qint64 bytesRead, qint64 totalBytes)
 {
     qDebug()<<"updateDataReadProgress:"<<bytesRead<<" totalbytes:"<<totalBytes;
     emit sendFlieRead(bytesRead,totalBytes);
     if (httpRequestAborted)
         return;
 }
 void HttpWindow::sslErrors(QNetworkReply*,const QList<QSslError> &errors)
 {
     QString errorString;
     foreach (const QSslError &error, errors) {
         if (!errorString.isEmpty())
             errorString += ", ";
         errorString += error.errorString();
     }
         reply->ignoreSslErrors();

 }

