#include "changekey.h"
#include "ui_changekey.h"
#include<QDebug>

changeKey::changeKey(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::changeKey)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    lockNumber=0;
    serial.openMycom(1,1);
    ui->closeBtn->setIcon(QPixmap(":/images/closepng.png"));

}

changeKey::~changeKey()
{
    delete ui;
}
void changeKey::getLockNo(int num)
{
    ui->okBtn->setEnabled(false);
    ui->cancelBtn->setText("取消");
    lockNumber=num;
    ui->tipUser->setText(QString::number(num)+"号抽屉更换钥匙\n正在打开抽屉***");
    ui->okBtn->setVisible(true);
    openCabinet(num);


}

void changeKey::on_cancelBtn_clicked()
{
    closeCabinet(lockNumber);
    ui->okBtn->setEnabled(true);
    this->close();
}
void changeKey::openCabinet(int num)
{

    int timeoutflag=0;

    serial.WriteMyCom(comman.open_command(num));
    QString drawstr=QString::number(num,16);
    if(drawstr.size()==1)
    {
        drawstr="0"+drawstr;
    }
    QString backinfor;
    while(true)
    { 
         qDebug()<<"*******************************";
         backinfor=serial.ReadMyCom(300);
         if(backinfor.contains("b3"+drawstr))
         {
             backinfor=backinfor.mid(backinfor.indexOf("b3"+drawstr),18); //一次读取发送和返回数据
         break;
         }
         timeoutflag++;
         if(timeoutflag>20)
         {
         break;
         }
    }
    QString infor=getinfor(backinfor);
    ui->tipUser->setText(infor);

}
QString changeKey::closeCabinet(int num)
{
       int trytime=0;
       serial.WriteMyCom(comman.close_command(num));
       QString backinfor=serial.ReadMyCom(200);
       QString drawstr=QString::number(num,16);
       if(drawstr.size()==1)
       {
           drawstr="0"+drawstr;
       }
       while(true)
       {
           trytime++;
       if(backinfor.contains("b5"+drawstr))
       {
           backinfor=backinfor.mid(backinfor.indexOf("b5"+drawstr),18); //一次读取发送和返回数据
           break;
       }
       else
       {
           if(trytime>20)
               break;
           backinfor=serial.ReadMyCom(200);
       }
       }

       if(backinfor.size()>=18)
       {
       backinfor=backinfor.mid(backinfor.size()-18,18);
       }
       QString statusflag=backinfor.mid(14,2);
       return statusflag;
}
QString changeKey::getinfor(QString inforr)
{
   QString reback=inforr; //一次读取发送和返回数据
    qDebug()<<"getinfor:"<<reback;
   QString reselt;
   if(reback.size()>18)
   {
   reback=reback.mid(reback.size()-18,18);
   }
  if(reback.mid(0,2)=="b3")
  {
      QString statusflag=reback.mid(14,2);
      if((statusflag=="01")||(statusflag=="02"))
      {
           ui->cancelBtn->setText("关闭");
          //          
           emit sendManagerOp(QString::number(lockNumber),"","1");
           reselt=QString::number(lockNumber)+"号抽屉打开成功！\n放入新钥匙后,点击【更换】按钮更换";
           ui->okBtn->setEnabled(true);

      }
      else
      {
           reselt=lockNumber+"号抽屉打开失败！";
           ui->okBtn->setEnabled(false);
           ui->cancelBtn->setText("关闭");
      }
      return reselt;
  }
  else if(reback.mid(0,2)=="b4")
  {

      QString statusflag=reback.mid(14,2);
      if(statusflag=="01")
      {
          reselt="钥匙更换成功！";
          ui->okBtn->setEnabled(false);
      }
      if(statusflag=="02")
      {
          reselt="钥匙更换失败！";
      }
      if(statusflag=="03")
      {
          reselt="未感应到新钥匙，请放入新钥匙";

      }
      return reselt;
  }
  else
  {
      reselt="命令执行完毕";
  }

}

void changeKey::on_okBtn_clicked()
{
    ui->tipUser->setText("正在发送更换钥匙命令，请等待***");
    serial.ReadMyCom();
    serial.WriteMyCom(comman.catCardID(lockNumber+100));
    QString drawstr=QString::number(lockNumber+100,16);
    if(drawstr.size()==1)
    {
        drawstr="0"+drawstr;
    }
    int timeoutflag=0;
    QString backinfor;
    while(true)
    {
        backinfor=serial.ReadMyCom(200);
        if(backinfor.size()>=18)
        {
            if(backinfor.contains("b4"+drawstr))
            {
            backinfor=backinfor.mid(backinfor.indexOf("b4"+drawstr),18); //一次读取发送和返回数据
            break;
            }
        }
        timeoutflag++;
        if(timeoutflag==20)
        {
        serial.WriteMyCom(comman.catCardID(lockNumber+100));
        }
        if(timeoutflag>40)
        {
        break;
        }
    }

    if(backinfor.size()>=18)
    {
    backinfor=backinfor.mid(backinfor.size()-18,18);
    }
    qDebug()<<"backinfor:"<<backinfor;
    QString statusflag=backinfor.mid(14,2);
    QString cardid=backinfor.mid(4,10);
    qDebug()<<"更换钥匙的id："<<cardid;
    if(statusflag=="01")//写成功
    {
        emit sendManagerOp(QString::number(lockNumber),cardid,"2");
        ui->tipUser->setText("更换钥匙成功！");
        QString closeflag=closeCabinet(lockNumber);
        qDebug()<<"抽屉关闭状态："<<closeflag;
        if(closeflag=="01")//抽屉关闭成功
        {
            ui->tipUser->setText("抽屉已经关闭");
            QString datar=readCardId(lockNumber+100);
            QString statusflag=datar.mid(10,2);
            QString cardid=datar.mid(0,10);

            qDebug()<<"关闭ID："<<cardid <<"status:"<<statusflag;
            if(statusflag=="01")
            {
               ui->okBtn->setEnabled(false);
               ui->tipUser->setText("更换钥匙成功！");
               ui->cancelBtn->setText("关闭");
               this->close();

            }
            else
            {
              ui->tipUser->setText("更换钥匙失败！");
            }

        }
        else
        {
            qDebug()<<"关闭err"<<closeflag;
        }
    }
    //QString infor=getinfor(backinfor);
   // ui->tipUser->setText(infor);

}

QString changeKey::readCardId(int lockid)
{
   int timeofGetData=0;
    QString inforr;
    serial.WriteMyCom(comman.readCardId(lockid));
    QString drawstr=QString::number(lockid,16);
    if(drawstr.size()==1)
    {
        drawstr="0"+drawstr;
    }
    while(true)
    {
    inforr=serial.ReadMyCom(300);
    if(inforr.contains("bb"+drawstr))
    {
         inforr=inforr.mid(inforr.indexOf("bb"+drawstr),18); //一次读取发送和返回数据
        break;
    }
    else
    {
        serial.WriteMyCom(comman.readCardId(lockid));
        timeofGetData++;
        if(timeofGetData>6)break;
    }
    }

    if(inforr.size()>=18)
    {
        inforr=inforr.mid(inforr.indexOf("bb"+drawstr),18); //一次读取发送和返回数据
    //inforr=inforr.mid(inforr.size()-18,18);
    }
    QString handcmd=inforr.mid(0,2);
    QString statuscmd=inforr.mid(14,2);
    if(handcmd=="bb")
    {
      if((statuscmd=="01")||(statuscmd=="02"))//
      {
      QString code=inforr.mid(4,10);
      return code+statuscmd;
      }
       else
           return "03";
    }
    return "00";
}

void changeKey::on_closeBtn_clicked()
{
    ui->cancelBtn->setFocus();
    this->close();
}
