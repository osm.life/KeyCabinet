#ifndef SETTINGWIFI_H
#define SETTINGWIFI_H

#include <QDialog>

namespace Ui {
    class settingWifi;
}

class settingWifi : public QDialog
{
    Q_OBJECT

public:
    explicit settingWifi(QWidget *parent = 0);
    ~settingWifi();

private slots:
    void on_linkBtn_clicked();
    void getEssidName(QString);

    void on_cancelBtn_clicked();

    void on_checkBox_clicked(bool checked);

    void on_forgetBtn_clicked();

private:
    Ui::settingWifi *ui;
    QString essId;
};

#endif // SETTINGWIFI_H
