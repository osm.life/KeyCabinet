#ifndef OPENLOCK_H
#define OPENLOCK_H

#include <QThread>
#include"config.h"
#include"gunconman.h"
#include"newqserialport.h"

class Openlock : public QThread
{
    Q_OBJECT
public:
    explicit Openlock(QObject *parent = 0);
    void startOpenAll(bool);
    void startSingle();
signals:
    void senddata(int index,QString flaginfor);//锁号与对应的开锁信息
    void sendcloseData(int index,QString flaginfor);//锁号与对应的开锁信息

    void sendManagerOp(QString,QString,QString);//管理员操作记录

public slots:
    void getlockNo(int );
    void getcloseNo(int );
    void openOneLock(int);
    void openOneLock(int,int);
    void closeoneLock(int);
    void closeoneLock(int,int);
    void getWarnCmd(int);
private:
    int numberlock;
    int timeoutflag;
    newqserialport serial;
    gunconman comman;
    char *com;
    int alllockNum;
    bool openTypeFlag;//打开或者关闭类型
    config conf;

    int maxNumber;

};

#endif // OPENLOCK_H
