#include "watchdog.h"
#include<QDebug>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h> // open() close()
#include <unistd.h> // read() write()
#include <termios.h> // set baud rate

watchdog::watchdog(QObject *parent) :
    QObject(parent)
{
   fd_watchdog=-1;
}
bool watchdog::openwatchdog()
{
    fd_watchdog = open("/dev/watchdog",O_WRONLY);
    if(fd_watchdog == -1) {
            int err = errno;
          qDebug()<<"\n!!! FAILED to open /dev/watchdog, errno:"<<err;
    return false;
    }
    else
        return true;
}
bool watchdog::feeddog()
{
    if(fd_watchdog >= 0) {
         static unsigned char food = 0;
            ssize_t eaten = write(fd_watchdog,&food, 1);
            if(eaten != 1) {
                   qDebug()<<"FAILED feeding watchdog";
            }
            else
            {
            //   qDebug()<<"ok feeding watchdog";
            return true;
            }
    }
    else
    {
        return false;
    }
}
bool watchdog::closedog()
{
        return true;
}
