#include "loginuser.h"
#include "ui_loginuser.h"
#include<QTimer>
#include<QDebug>
#include<QValidator>
loginUser::loginUser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loginUser)
{
    ui->setupUi(this);
    limitTime=0;

    QTimer *timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(showtime()));
    timer->start(1000);
    this->setWindowTitle(tr("用户条码输入"));
//    QRegExp regx("[0-9]+$");
//    QValidator *validator = new QRegExpValidator(regx,ui->password);
//    ui->password->setValidator(validator);
//    ui->password->installEventFilter(this);

    this->setStyleSheet("background-image: url(:/images/userlogin.png);");
    ui->label->setStyleSheet("background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,"
                               "stop: 0 #FFFFFF, stop: 0.5 #FFFFFF,"
                               "stop: 1.0 #FFFFFF);"
                               "border-color: #FFFFFF;font-size:25px;color:#000000");
    ui->password->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:30px;font-size:20px;");
    this->setWindowFlags(Qt::FramelessWindowHint);
    ui->ok_btn->setStyleSheet("background-image: url(:/images/button.png);border-radius: 5px;height:50px;width:100px;font-size:25px;");
    ui->cancel_btn->setStyleSheet("background-image: url(:/images/button.png);border-radius:5px;height:50px;width:100px;font-size:25px;");
  //  ui->ok_btn->setStyleSheet("background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,"
   //                         "stop: 0 #758385, stop: 0.5 #122C39,"
   //                         "stop: 1.0 #0E7788);"
   //                         "border-color: #11505C;font-size:20px; width:90px; height: 60px;");
//    ui->cancel_btn->setStyleSheet("background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,"
//                            "stop: 0 #758385, stop: 0.5 #122C39,"
//                            "stop: 1.0 #0E7788);"
//                            "border-color: #11505C;font-size:20px; width:90px; height: 60px; ");

    ui->num0->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
     ui->num1->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
      ui->num2->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
       ui->num3->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
        ui->num4->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
         ui->num5->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
          ui->num6->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
           ui->num7->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
            ui->num8->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");
             ui->num9->setStyleSheet("background-image: url(:/images/button.png);border-radius: 3px;height:40px;width:80px;font-size:20px;");


}

loginUser::~loginUser()
{
    delete ui;
}

void loginUser::showtime()
{
    limitTime++;
    if(limitTime>30)
    {
        limitTime=0;
        if(this->isActiveWindow())
            this->close();
    }
}

void loginUser::on_password_textEdited(const QString &arg1)
{
    limitTime=0;
}
void loginUser::keyReleaseEvent(QKeyEvent *e)
{
    if(e->key()==Qt::Key_Escape)
    {
         if(ui->password->text().isEmpty())
         {
         this->close();
         }
        ui->password->backspace();

    }
    else
    {
    qDebug()<<"keyinfor:"<<e->key();
    }
}
void loginUser::keyPressEvent(QKeyEvent *e)
{
    if(Qt::Key_Return==e->key())//确认键按下
    {

        if(ui->password->text().isEmpty())
        {
        this->close();
        }
        else
        {
        on_ok_btn_clicked();
        }
    }
}

void loginUser::getinfor(QString infor)
{
   // ui->tipUser->setText(ui->tipUser->text()+"\n"+infor);//提示消息
}

void loginUser::on_ok_btn_clicked()//手动发送条码
{
    if(!ui->password->text().isEmpty())
    {
    QString text=ui->password->text();
    ui->password->clear();
    this->hide();
    emit sendCode(text);
    this->close();
    }
    else
    {
    this->close();
    }

}

void loginUser::on_cancel_btn_clicked()
{
    if(!ui->password->text().isEmpty())
        ui->password->backspace();
    else
    this->close();
}

void loginUser::on_num0_clicked()
{
    ui->password->setText(ui->password->text()+"0");
}
void loginUser::on_num1_clicked()
{
    ui->password->setText(ui->password->text()+"1");
}
void loginUser::on_num2_clicked()
{
    ui->password->setText(ui->password->text()+"2");
}
void loginUser::on_num3_clicked()
{
    ui->password->setText(ui->password->text()+"3");
}
void loginUser::on_num4_clicked()
{
    ui->password->setText(ui->password->text()+"4");
}
void loginUser::on_num5_clicked()
{
    ui->password->setText(ui->password->text()+"5");
}
void loginUser::on_num6_clicked()
{
    ui->password->setText(ui->password->text()+"6");
}
void loginUser::on_num7_clicked()
{
    ui->password->setText(ui->password->text()+"7");
}
void loginUser::on_num8_clicked()
{
    ui->password->setText(ui->password->text()+"8");
}
void loginUser::on_num9_clicked()
{
    ui->password->setText(ui->password->text()+"9");
}
bool loginUser::eventFilter(QObject *obj, QEvent *event)
{

     if (obj ==ui->password) {
         if (event->type() == QEvent::RequestSoftwareInputPanel) {
             return false;
         } else {
             return false;
         }
     } else {
         return loginUser::eventFilter(obj, event);
     }
}
void loginUser::showEvent(QShowEvent *e)
{
    limitTime=0;
    ui->password->setFocus();
}


