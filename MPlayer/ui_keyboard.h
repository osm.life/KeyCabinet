/********************************************************************************
** Form generated from reading UI file 'keyboard.ui'
**
** Created: Fri Jun 19 10:57:49 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYBOARD_H
#define UI_KEYBOARD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_KeyBoard
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QPushButton *num_1;
    QPushButton *num_2;
    QPushButton *num_3;
    QPushButton *num_4;
    QPushButton *num_5;
    QPushButton *num_6;
    QPushButton *num_7;
    QPushButton *num_8;
    QPushButton *num_9;
    QPushButton *clear;
    QPushButton *ok;
    QPushButton *num_0;
    QPushButton *close;
    QLineEdit *showdata;

    void setupUi(QDialog *KeyBoard)
    {
        if (KeyBoard->objectName().isEmpty())
            KeyBoard->setObjectName(QString::fromUtf8("KeyBoard"));
        KeyBoard->resize(565, 387);
        KeyBoard->setMinimumSize(QSize(0, 0));
        QFont font;
        font.setPointSize(33);
        KeyBoard->setFont(font);
        verticalLayout = new QVBoxLayout(KeyBoard);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        num_1 = new QPushButton(KeyBoard);
        num_1->setObjectName(QString::fromUtf8("num_1"));

        gridLayout->addWidget(num_1, 1, 0, 1, 1);

        num_2 = new QPushButton(KeyBoard);
        num_2->setObjectName(QString::fromUtf8("num_2"));

        gridLayout->addWidget(num_2, 1, 1, 1, 1);

        num_3 = new QPushButton(KeyBoard);
        num_3->setObjectName(QString::fromUtf8("num_3"));
        num_3->setMinimumSize(QSize(180, 0));

        gridLayout->addWidget(num_3, 1, 2, 1, 1);

        num_4 = new QPushButton(KeyBoard);
        num_4->setObjectName(QString::fromUtf8("num_4"));

        gridLayout->addWidget(num_4, 2, 0, 1, 1);

        num_5 = new QPushButton(KeyBoard);
        num_5->setObjectName(QString::fromUtf8("num_5"));

        gridLayout->addWidget(num_5, 2, 1, 1, 1);

        num_6 = new QPushButton(KeyBoard);
        num_6->setObjectName(QString::fromUtf8("num_6"));

        gridLayout->addWidget(num_6, 2, 2, 1, 1);

        num_7 = new QPushButton(KeyBoard);
        num_7->setObjectName(QString::fromUtf8("num_7"));

        gridLayout->addWidget(num_7, 3, 0, 1, 1);

        num_8 = new QPushButton(KeyBoard);
        num_8->setObjectName(QString::fromUtf8("num_8"));

        gridLayout->addWidget(num_8, 3, 1, 1, 1);

        num_9 = new QPushButton(KeyBoard);
        num_9->setObjectName(QString::fromUtf8("num_9"));

        gridLayout->addWidget(num_9, 3, 2, 1, 1);

        clear = new QPushButton(KeyBoard);
        clear->setObjectName(QString::fromUtf8("clear"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(clear->sizePolicy().hasHeightForWidth());
        clear->setSizePolicy(sizePolicy);
        clear->setMinimumSize(QSize(0, 57));
        QFont font1;
        font1.setPointSize(30);
        clear->setFont(font1);

        gridLayout->addWidget(clear, 0, 2, 1, 1);

        ok = new QPushButton(KeyBoard);
        ok->setObjectName(QString::fromUtf8("ok"));

        gridLayout->addWidget(ok, 4, 2, 1, 1);

        num_0 = new QPushButton(KeyBoard);
        num_0->setObjectName(QString::fromUtf8("num_0"));

        gridLayout->addWidget(num_0, 4, 1, 1, 1);

        close = new QPushButton(KeyBoard);
        close->setObjectName(QString::fromUtf8("close"));
        sizePolicy.setHeightForWidth(close->sizePolicy().hasHeightForWidth());
        close->setSizePolicy(sizePolicy);
        close->setMinimumSize(QSize(0, 56));
        close->setFont(font);

        gridLayout->addWidget(close, 4, 0, 1, 1);

        showdata = new QLineEdit(KeyBoard);
        showdata->setObjectName(QString::fromUtf8("showdata"));
        QFont font2;
        font2.setPointSize(28);
        showdata->setFont(font2);

        gridLayout->addWidget(showdata, 0, 0, 1, 2);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(KeyBoard);

        QMetaObject::connectSlotsByName(KeyBoard);
    } // setupUi

    void retranslateUi(QDialog *KeyBoard)
    {
        KeyBoard->setWindowTitle(QApplication::translate("KeyBoard", "Dialog", 0, QApplication::UnicodeUTF8));
        num_1->setText(QApplication::translate("KeyBoard", "1", 0, QApplication::UnicodeUTF8));
        num_2->setText(QApplication::translate("KeyBoard", "2", 0, QApplication::UnicodeUTF8));
        num_3->setText(QApplication::translate("KeyBoard", "3", 0, QApplication::UnicodeUTF8));
        num_4->setText(QApplication::translate("KeyBoard", "4", 0, QApplication::UnicodeUTF8));
        num_5->setText(QApplication::translate("KeyBoard", "5", 0, QApplication::UnicodeUTF8));
        num_6->setText(QApplication::translate("KeyBoard", "6", 0, QApplication::UnicodeUTF8));
        num_7->setText(QApplication::translate("KeyBoard", "7", 0, QApplication::UnicodeUTF8));
        num_8->setText(QApplication::translate("KeyBoard", "8", 0, QApplication::UnicodeUTF8));
        num_9->setText(QApplication::translate("KeyBoard", "9", 0, QApplication::UnicodeUTF8));
        clear->setText(QApplication::translate("KeyBoard", "\346\270\205\351\231\244", 0, QApplication::UnicodeUTF8));
        ok->setText(QApplication::translate("KeyBoard", "\347\241\256\350\256\244", 0, QApplication::UnicodeUTF8));
        num_0->setText(QApplication::translate("KeyBoard", "0", 0, QApplication::UnicodeUTF8));
        close->setText(QApplication::translate("KeyBoard", "\345\205\263\351\227\255", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class KeyBoard: public Ui_KeyBoard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYBOARD_H
