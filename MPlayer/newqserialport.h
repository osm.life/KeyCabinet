#ifndef NEWQSERIALPORT_H
#define NEWQSERIALPORT_H

#include <QObject>
#include"myStaticClass.h"
#include"posix_qextserialport.h"
class newqserialport : public QObject
{
    Q_OBJECT
public:
    explicit newqserialport(QObject *parent = 0);

    QByteArray GetHexValue(QString str);//格式化16进制数据,参数为字符串
    char ConvertHexChar(char ch);

signals:
private:
    Posix_QextSerialPort *myCom;
    int number_dev;


public slots:
    QString ReadMyCom();//读取串口数据
    QString ReadMyCom(int);//读取串口数据
    void WriteMyCom(QString);//写串口数据
    void WriteMyCom(char *);//写串口数据
    void WriteMyCom(char *,int);//写串口数据
    void openMycom(int,int);
    bool openMycom(int);
    bool openMycom();

    void  WriteSmsCom(QString);//写串口数据
    void closeMycom();

    QString ReadSmsCom();//读取串口数据
    QString ReadSmsCom(int);//读取串口数据

    bool comisopen();//判断串口是否打开成功

};

#endif // NEWQSERIALPORT_H
