/****************************************************************************
** Meta object code from reading C++ file 'setting3gnet.h'
**
** Created: Wed Aug 5 16:45:46 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "setting3gnet.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'setting3gnet.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_setting3gnet[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x08,
      33,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_setting3gnet[] = {
    "setting3gnet\0\0on_okBtn_clicked()\0"
    "on_cancelBtn_clicked()\0"
};

void setting3gnet::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        setting3gnet *_t = static_cast<setting3gnet *>(_o);
        switch (_id) {
        case 0: _t->on_okBtn_clicked(); break;
        case 1: _t->on_cancelBtn_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData setting3gnet::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject setting3gnet::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_setting3gnet,
      qt_meta_data_setting3gnet, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &setting3gnet::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *setting3gnet::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *setting3gnet::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_setting3gnet))
        return static_cast<void*>(const_cast< setting3gnet*>(this));
    return QDialog::qt_metacast(_clname);
}

int setting3gnet::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
