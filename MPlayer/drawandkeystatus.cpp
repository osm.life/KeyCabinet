#include "drawandkeystatus.h"
DrawAndKeyStatus::DrawAndKeyStatus(QObject *parent) :
    QThread(parent)
{
    config conf;
    timeflag=conf.read("TimeCabinetClose","timecabinetclose").toInt();
    MaxKeyNo=conf.read("Cabinet","cabinetNumber").toInt();
    keystatus="0000000000";
    drawstatus="0000000000";
    codeNumber=101+MaxKeyNo;
    qDebug()<<"maxkeyNo:"<<MaxKeyNo;
    if(MaxKeyNo<=0)
    {
    MaxKeyNo=10;
    }
     qDebug()<<"maxkeyNo:"<<MaxKeyNo;
    port.openMycom(1,1);
    stopflag=false;
    numberStart=0;
}
void DrawAndKeyStatus::run()
{
    while(!stopflag)
       {
        msleep(100);
           for(int i=numberStart;i<=MaxKeyNo;i++)
           {
           msleep(10);
           if(!stopflag)
           {
               if(i==MaxKeyNo)
               {
               numberStart=1;
               }
               readstatus(i);
           }
           else
           {
               numberStart=i;
               break;
           }
           }
           emit sendDrawstatus(drawstatus);
           emit sendKeyStatus(keystatus);
       }
}
bool DrawAndKeyStatus::readstatus(int i)
{
   //#################################keystatus########################################
 //qDebug()<<"#####################keystatus#### ###########"<<i;
    int maxkey=i;
   port.WriteMyCom(cmd.readCardId(maxkey+100));
   QString getinfor=port.ReadMyCom(200);
   QString drawno1=QString::number((maxkey+100),16);
  // qDebug()<<"钥匙状态:"<<drawno1.toInt(0,16)<<"    "<<getinfor<<"  "<<"bb"+drawno1;
   if(getinfor.contains("bb"+drawno1))
   {
       getinfor=getinfor.mid(getinfor.indexOf("bb"+drawno1),18);
       if(checkCmd.check_cmdSun(getinfor))//校验和检查
       {
          // qDebug()<<"getinfor:"<<getinfor<<"校验和ok";
       int lock=getinfor.mid(2,2).toInt(0,16);
       QString status=getinfor.mid(14,2);
       int statusInt=1;

         if((status=="01")||(status=="02"))//01,02有钥匙，03无
             {
             statusInt=1;
             }
         else if(status=="03")
             {
            statusInt=0;
            }
         else
         {

         }
        updateStatus(0,lock-100,statusInt);
       }

   }
   else
   {
      // qDebug()<<"get key bad："<<getinfor;
   }

   if(stopflag)
   {
   return false;
   }
  //#################################抽屉状态########################################
  // qDebug()<<"#####################draw status#### ###########"<<maxkey;
   port.WriteMyCom(cmd.readstatus_command(maxkey));
    getinfor=port.ReadMyCom(200);
    drawno1=QString::number(maxkey,16);
    if(drawno1.size()==1)//抽屉号变成16进制
    {
        drawno1="0"+drawno1;
    }
    if(getinfor.contains("b9"+drawno1))
    {
        getinfor=getinfor.mid(getinfor.indexOf("b9"+drawno1),18);
       // qDebug()<<"getinfor:"<<getinfor;
        int lock=getinfor.mid(2,2).toInt(0,16);
        QString status=getinfor.mid(14,2);
        int statusInt=1;
          if(status=="01")//01,02有钥匙，03无
              {
              statusInt=0;
              }
          else if(status=="02")
              {
             statusInt=1;
             }
          else
          {

          }
          updateStatus(2,lock,statusInt);
    }
  // qDebug()<<"抽屉状态检测drawStatus:"<<drawstatus<<"    data:"<<getinfor;


    if(stopflag)
    {
    return false;
    }
//#################################刷卡########################################
//qDebug()<<"#####################cardid scanf#### ###########"<<codeNumber;

     QString drawstr=QString::number(codeNumber,16);
     port.WriteMyCom(cmd.readCardId(codeNumber));//codeNumber
     QString inforr;
     drawno1=QString::number(codeNumber,16);
     if(drawno1.size()==1)//抽屉号变成16进制
     {
         drawno1="0"+drawno1;
     }
     inforr=port.ReadMyCom(200);
     //qDebug()<<"      刷卡数据："<<inforr;
     QString backcmd,handcmd,statuscmd;//执行命令返回的数据，命令头，命令执行结果返回
     if(inforr.contains("bb"+drawno1))
     {
         qDebug()<<inforr;
        inforr=inforr.mid(inforr.indexOf("bb"+drawstr),18);
        if(checkCmd.check_cmdSun(inforr))//核对校验和是否正确
        {
            handcmd=inforr.mid(0,2);
            statuscmd=inforr.mid(14,2);
           // qDebug()<<"命令可以"<<handcmd<<"   "<<statuscmd;
            if((statuscmd=="01")||(statuscmd=="02"))//有钥匙在
            {
              qDebug()<<"命令可以,发送给服务器";
             code=inforr.mid(4,10);
             emit sendCode(code);
             msleep(1000);
            }
        }
        else
        {
            return false;
        }

   }


     if(stopflag)
     {
     return false;
     }
//#################################报警检测########################################

     port.WriteMyCom(cmd.readwarn_command(codeNumber+2));//大门状态检测命令
     inforr=port.ReadMyCom(300);
     backcmd,handcmd,statuscmd;//执行命令返回的数据，命令头，命令执行结果返回
     int warnNumber=0;
     if(inforr.size()>=18)
     {
     backcmd=inforr.mid(inforr.size()-18,18);
     handcmd=backcmd.mid(0,2);
     statuscmd=backcmd.mid(14,2);
     warnNumber=backcmd.mid(2,2).toInt(0,16);
     //qDebug()<<"status number:"<<warnNumber;
     if((handcmd=="ba")&&(statuscmd=="02")&&(warnNumber==(codeNumber+2)))
     {
     emit sendWarning("02");
     int locknum=codeNumber+2;
     msleep(100);
     port.WriteMyCom(cmd.open_command(locknum));
     QString infor=port.ReadMyCom();
     }
     }

     return true;
}

int DrawAndKeyStatus::rebackInfor(QString inforr)
{

    QString backcmd,handcmd,statuscmd,keyno;//执行命令返回的数据，命令头，命令执行结果返回
    if(inforr.size()>=18)
    {
    backcmd=inforr.mid(inforr.size()-18,18);
    handcmd=backcmd.mid(0,2);
    keyno=backcmd.mid(2,2);
    statuscmd=backcmd.mid(14,2);
   // qDebug()<<"返回命令执行结果flag:"<<backcmd<< "  handcmd:"<<handcmd<<"    statuscmd:"<<statuscmd;
    if((statuscmd=="01")||(statuscmd=="02"))
    {
    int ky=keyno.toInt(0,16);
    //qDebug()<<"返回的钥匙号："<<ky;
    return ky;
    }
    else
    {
        return 0;
    }
    }
    return 0;
}
QString DrawAndKeyStatus::checkData(QString inforr)
{
    QString backcmd,handcmd,statuscmd,keyno;//执行命令返回的数据，命令头，命令执行结果返回
    if(inforr.size()>=18)
    {
    backcmd=inforr.mid(inforr.size()-18,18);
    handcmd=backcmd.mid(0,2);
    keyno=backcmd.mid(2,2);
    statuscmd=backcmd.mid(14,2);
    }
    if(backcmd.contains("b9"))
    {
    return keyno+statuscmd;
    }

    if(backcmd.contains("bb"))
    {
    return keyno+statuscmd;
    }
    return "";

}

void DrawAndKeyStatus::pauseThread()
{
    stopflag=true;
}
void DrawAndKeyStatus::startThread()
{
       stopflag=false;
       port.ReadMyCom();
       this->start();
}
bool DrawAndKeyStatus::getStatus()
{
    return stopflag;
}

void DrawAndKeyStatus::updateStatus(int flag,int lockno, int status)//status 0关闭，1打开
{
    //qDebug()<<"flag:"<<flag<<"  lockno:"<<lockno<<"   status:"<<status;
    int status1,status2,status3,status4,status5;
        if(flag>1)
        {
        status1=drawstatus.mid(0,2).toInt(0,16);
        status2=drawstatus.mid(2,2).toInt(0,16);
        status3=drawstatus.mid(4,2).toInt(0,16);
        status4=drawstatus.mid(6,2).toInt(0,16);
        status5=drawstatus.mid(8,2).toInt(0,16);
        }
        else
        {
            status1=keystatus.mid(0,2).toInt(0,16);
            status2=keystatus.mid(2,2).toInt(0,16);
            status3=keystatus.mid(4,2).toInt(0,16);
            status4=keystatus.mid(6,2).toInt(0,16);
            status5=keystatus.mid(8,2).toInt(0,16);
        }
uchar b;
int numbera=lockno/8;//除数
int numberb=lockno%8;//余数
if(numberb==0)
{
    numbera=numbera-1;
    numberb=8;
}
switch(numberb)
{
case 1:
       b=0x01;break;
case 2:
       b=0x02;break;
case 3:
       b=0x04;break;
case 4:
       b=0x08; break;
case 5:
       b=0x10;break;
case 6:
       b=0x20;break;
case 7:
       b=0x40;break;
case 8:
       b=0x80;break;
}
int ch=0;
switch(numbera)
{
case 0:
    ch=status1&b; break;
case 1:
    ch=status2&b; break;
case 2:
    ch=status3&b; break;
case 3:
    ch=status4&b; break;
case 4:
    ch=status5&b;break;
}
int old=0;
//qDebug()<<"ch size:"<<ch;
if(ch>0)
{
  old=1;
}
if(old==status)
{
//位没有改变
}
else
{
    if(old==0)
    {
        switch(numbera)
        {
        case 0:
            status1=status1+b; break;
        case 1:
            status2=status2+b; break;
        case 2:
            status3=status3+b; break;
        case 3:
            status4=status4+b; break;
        case 4:
            status5=status5+b; break;
        }
    }
    else
    {

        switch(numbera)
        {
        case 0:
            status1=status1-b; break;
        case 1:
            status2=status2-b; break;
        case 2:
            status3=status3-b; break;
        case 3:
            status4=status4-b; break;
        case 4:
            status5=status5-b; break;
        }
    }
}

QString str1,str2,str3,str4,str5;
    str1=QString::number(status1,16);
    str2=QString::number(status2,16);
    str3=QString::number(status3,16);
    str4=QString::number(status4,16);
    str5=QString::number(status5,16);
    if(str1.size()<2)
        str1="0"+str1;
    if(str2.size()<2)
        str2="0"+str2;
    if(str3.size()<2)
        str3="0"+str3;
    if(str4.size()<2)
        str4="0"+str4;
    if(str5.size()<2)
        str5="0"+str5;

if(flag>1)
{

    drawstatus=str1+str2+str3+str4+str5;
   // qDebug()<<"抽屉状态更新 :"<<drawstatus;
}
else
{
    keystatus=str1+str2+str3+str4+str5;
   //  qDebug()<<"钥匙状态更新:"<<keystatus;
}

}
