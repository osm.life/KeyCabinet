#include "advertisermenttext.h"
#include "ui_advertisermenttext.h"

#include<QPainter>
#include<QFileSystemWatcher>
#include<QDebug>
#include<QDir>
#include<QFile>
#include<QTextStream>
advertisermentText::advertisermentText(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::advertisermentText)
{
    offset = 0;
    myTimerId = 0;
    QFileSystemWatcher *filewatcher=new QFileSystemWatcher(this);
    filewatcher->addPath(QDir::currentPath()+"/text/adv.txt");
    connect(filewatcher,SIGNAL(fileChanged(QString)),this,SLOT(updatetext(QString)));
    connect(filewatcher,SIGNAL(directoryChanged(QString)),this,SLOT(updatpath(QString)));
}

advertisermentText::~advertisermentText()
{
    delete ui;
}
void advertisermentText::setText(const QString &newText)
{
myText = newText;
update();
updateGeometry();
}
QSize advertisermentText::sizeHint() const
{
return fontMetrics().size(0, text());
}
void advertisermentText::paintEvent(QPaintEvent * )
{
QPainter painter(this);
int textWidth = fontMetrics().width(text());
if (textWidth < 1)
return;
int x = -offset;
while (x < width()) {
painter.drawText(x, 0, textWidth, height(),
Qt::AlignLeft | Qt::AlignVCenter, text());
x += textWidth;
}
}
void advertisermentText::showEvent(QShowEvent * )
{
myTimerId = startTimer(30);
}
void advertisermentText::timerEvent(QTimerEvent *event)
{
if (event->timerId() == myTimerId) {
++offset;
if (offset >= fontMetrics().width(text()))
offset = 0;
scroll(-1, 0);
} else {
QWidget::timerEvent(event);
}
}
void advertisermentText::hideEvent(QHideEvent * )
{
killTimer(myTimerId);
myTimerId = 0;
}

void advertisermentText::updatetext(QString  test)
{

    qDebug()<<"advertisement test:"<<test;
    QFile file(QDir::currentPath()+"/text/adv.txt");
    if(file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
       myText=file.readAll();
    }
}
void advertisermentText::updatpath(QString  test)
{
    qDebug()<<"advertisement path:"<<test;
}

