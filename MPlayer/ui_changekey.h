/********************************************************************************
** Form generated from reading UI file 'changekey.ui'
**
** Created: Fri Nov 13 09:18:29 2015
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHANGEKEY_H
#define UI_CHANGEKEY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_changeKey
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *tipUser;
    QPushButton *cancelBtn;
    QPushButton *okBtn;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *closeBtn;

    void setupUi(QDialog *changeKey)
    {
        if (changeKey->objectName().isEmpty())
            changeKey->setObjectName(QString::fromUtf8("changeKey"));
        changeKey->resize(580, 370);
        QFont font;
        font.setPointSize(15);
        changeKey->setFont(font);
        verticalLayout = new QVBoxLayout(changeKey);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tipUser = new QLabel(changeKey);
        tipUser->setObjectName(QString::fromUtf8("tipUser"));
        tipUser->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(tipUser, 1, 1, 1, 3);

        cancelBtn = new QPushButton(changeKey);
        cancelBtn->setObjectName(QString::fromUtf8("cancelBtn"));
        cancelBtn->setMinimumSize(QSize(200, 50));

        gridLayout->addWidget(cancelBtn, 2, 1, 1, 1);

        okBtn = new QPushButton(changeKey);
        okBtn->setObjectName(QString::fromUtf8("okBtn"));
        okBtn->setMinimumSize(QSize(200, 50));

        gridLayout->addWidget(okBtn, 2, 3, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 2, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 2, 4, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 2, 0, 1, 1);

        closeBtn = new QPushButton(changeKey);
        closeBtn->setObjectName(QString::fromUtf8("closeBtn"));
        closeBtn->setMaximumSize(QSize(60, 60));
        closeBtn->setIconSize(QSize(50, 50));
        closeBtn->setFlat(true);

        gridLayout->addWidget(closeBtn, 0, 4, 1, 1);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(changeKey);

        QMetaObject::connectSlotsByName(changeKey);
    } // setupUi

    void retranslateUi(QDialog *changeKey)
    {
        changeKey->setWindowTitle(QApplication::translate("changeKey", "Dialog", 0, QApplication::UnicodeUTF8));
        tipUser->setText(QString());
        cancelBtn->setText(QApplication::translate("changeKey", "\345\217\226\346\266\210", 0, QApplication::UnicodeUTF8));
        okBtn->setText(QApplication::translate("changeKey", "\346\233\264\346\215\242", 0, QApplication::UnicodeUTF8));
        closeBtn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class changeKey: public Ui_changeKey {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGEKEY_H
